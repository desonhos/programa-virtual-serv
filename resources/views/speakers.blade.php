@extends('layouts/app')

@section('page-class', 'page-speakers-index')

@section('js')
    <script src="{{ mix('js/pages/speakersView.js') }}"></script>
@endsection

@php
// Indiza los ponentes por el apellido
$speakersMap = [];
foreach ($speakers as $speaker) {
    $firstLetter = mb_substr($speaker->getSurname(), 0, 1);
    $firstLetter = mb_strtoupper($firstLetter);
    if ($firstLetter == "Á") { $firstLetter = "A"; }
    if ($firstLetter == "É") { $firstLetter = "E"; }
    if ($firstLetter == "Í") { $firstLetter = "I"; }
    if ($firstLetter == "Ó") { $firstLetter = "O"; }
    if ($firstLetter == "Ú") { $firstLetter = "U"; }
    if ($firstLetter == "Ñ") { $firstLetter = "N"; }
    if (!array_key_exists($firstLetter, $speakersMap)) { $speakersMap[$firstLetter] = []; }
    array_push($speakersMap[$firstLetter], $speaker);
}
@endphp

@section('content')
    <article>
        <section class="section speaker-list">
            <div class="container">
                {{-- Buscador --}}
                <div class="columns">
                    <div class="column is-half">
                        <form method="POST" action="">
                            @csrf
                            <div class="field has-addons">
                                <div class="control">
                                    <input name="searchText" class="input" type="text" placeholder="{{ __('Buscar ponente') }}" @if (!empty($searchText)) value="{{ $searchText }}" @endif>
                                </div>
                                <div class="control">
                                    <button id="searchBtn" class="button">
                                        @svg('ico-search')
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

                {{-- Filtrador de letras --}}
                <nav class="pagination has-paginated-content" role="navigation" aria-label="pagination">
                    <ul class="pagination-list">
                        @foreach( range('A', 'Z') as $letter)
                            @if (array_key_exists($letter, $speakersMap))
                                <li>
                                    <a href="#letra-{{$letter}}" class="pagination-link" aria-label="{{ __('Ir a la página') }} {{ $letter }}">
                                        {{ $letter }}
                                    </a>
                                </li>
                            @endif
                        @endforeach
                    </ul>
                </nav>

                {{-- Resultados --}}
                @php $noResults = true; @endphp
                @foreach( range('A', 'Z') as $letter)
                    <div class="alphaTab paginated-content is-hidden" data-id="#letra-{{$letter}}">
                        @if (array_key_exists($letter, $speakersMap))
                            @foreach ($speakersMap[$letter] as $speaker)
                                @php $noResults = false; @endphp
                                <a class="speaker" href="{{ route('speakers.view', ['speaker_id' => $speaker->getSpeakerId() ]) }}">
                                    @php $img = $speaker->getImageUrl(); @endphp
                                    @if ($img)
                                        <img src="{{ $img }}" onerror="$(this).hide()"/>
                                    @endif
                                    <span>{{ $speaker->getSurname() }}, {{ $speaker->getName() }}</span>
                                </a>
                            @endforeach
                        @endif
                    </div>
                @endforeach
                @if ($noResults)
                    <p>{{ __("No hay resultados") }}</p>
                @endif
            </div>
        </section>
    </article>
@endsection
