@if ($paper->isPoster())
    @php
    $average = App\Models\PosterRating::_getAverageRatingOfPaper($paper->getId());
    $currentUserRating = App\Models\PosterRating::_getUserRatingOfPaper($paper->getId());
    @endphp
    {{-- @if ($currentUserRating)
        <p>Valoración del usuario actual: {{ $currentUserRating }}</p>
    @endif
    @if ($average)
        <p>Valoración media: {{ $average[0] }}</p>
        <p>Número de valoraciones: {{ $average[1] }}</p>
    @endif --}}
    <div class="mt-2 is-flex is-align-items-center ratings">

        {{-- Media --}}
        @if ($average)
            <span class="has-text-weight-bold ratings-average-number">
                {{ number_format($average[0], 1, ",", ".") }}/{{ config('web.ratings.maxScore') }}
            </span>
            <span class="is-hidden averageHidden">{{ $average[0] }}</span>
        @endif

        {{-- Estrellas con la valoración media --}}
        @php $i = 0; @endphp
        <div class="is-flex is-align-items-center ratings-average-stars">
            @if ($average)
                @for ($i; (($i < config('web.ratings.maxScore')) && ($i < floor($average[0]))); $i++)
                    @svg('ico-estrella-rellena')
                @endfor
            @endif
            @for ($i; $i < config('web.ratings.maxScore'); $i++)
                @svg('ico-estrella-hueca')
            @endfor
        </div>

        {{-- Número de valoraciones --}}
        <span class="has-text-14 has-text-14-mobile ratings-total">
            @if ($average)
                {{ $average[1] }} @php echo ($average[1] > 1) ? __("valoraciones") : __("valoración"); @endphp
            @else
                0 {{__("valoraciones")}}
            @endif
        </span>
    </div>
    @if (!$currentUserRating)
        <input class="button is-secondary is-outline mt-2 ratePoster" type="submit" value="{{__("Valorar póster")}}" data-paper-id="{{ $paper->getId() }}">
    @else
        <input class="button is-outline mt-2 has-text-grey is-uppercase letter-spacing-0 pointer-events-none" type="submit" value="{{__("Ya has valorado este póster")}}">
    @endif

    @include('partials._modal-rate', compact('paper'))
@endif
