<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
*/

// Webapp
Route::get("/", 'WebappController@showHome')->name('inicio-webapp');
// Route::get("/", function(){ return redirect()->route('program.view'); })->name('inicio-webapp');
Route::get('info', 'InfoController@view')->name('webapp.info');
Route::get('patrocinadores', 'SponsorController@index')->name('webapp.sponsors');
Route::get("notas", 'NotebookController@view')->name('notas');
Route::get("agenda", 'ScheduleController@view')->name('agenda');
Route::get('ponentes', 'SpeakerController@index')->name('speakers.index');
Route::post('ponentes', 'SpeakerController@search')->name('speakers.search');
Route::get('ponentes/{speaker_id}', 'SpeakerController@view')->name('speakers.view');
Route::get('instalar-webapp', 'WebappController@showAddToHomescreen')->name('webapp.add-to-homescreen');
Route::view('politica-de-privacidad', 'privacy-policy')->name('privacy-policy');

// Modo videojuego
Route::get('exposicion', function() { return redirect()->route('virtual.vroom.view', ['slug' => 'exposicion']); })->name('virtual.exhibition');
Route::get('exposicion/{exhibitor_slug}', function($exhibitorSlug) { return redirect()->route('virtual.vroom.view', ['slug' => $exhibitorSlug]); })->name('virtual.exhibitor');
Route::get('sala/{slug}', 'VRoomController@show')->name('virtual.vroom.view');
Route::get('sala/{slug}/siguiente', 'VRoomController@showNext')->name('virtual.vroom.view-next');
Route::get('sala/{slug}/previa', 'VRoomController@showPrev')->name('virtual.vroom.view-prev');
Route::get('sala/view-id/{id}', 'VRoomController@showById')->name('virtual.vroom.view-by-id');

// PWA
Route::get('/offline', function(){return view('vendor.laravelpwa.offline');});

// AUTH
Route::group(['middleware' => 'check.closing.date'], function(){
    Route::get('/programa', 'ProgramController@view')->name('program.view');
    Route::get('/busca', 'ProgramController@search')->name('program.search');
    Route::get('/sesiones/{session}', 'SessionController@view')->name('sessions.view');
    Route::post('/ponencias/{paper_id}/pregunta', 'PaperController@ask')->name('papers.ask');
    Route::post('/sesiones/{session_id}/contabiliza-visualizacion', 'SessionController@storeViewing')->name('sessions.viewing');
    Route::post('/valora-ponencia', 'PaperController@rate')->name('papers.rate');
});
Route::group(['middleware' => 'auth:multi'], function () {
    Route::get('/descargar-certificado-asistencia', 'CertificateController@generate')->name('certificate.generate');
    Route::get('mi-perfil', function() { return view('user.profile'); })->name('user.profile');
    Route::post('mi-perfil', 'UserController@updateProfile')->name('user.update-profile');
    Route::get('salir', 'UserController@logout')->name('user.exit');
    Route::get('/virtual/sesiones/{session}', 'SessionController@viewVirtual')->name('virtual.sessions.view');
});

// NO AUTH
Route::get('/genera-url-videos-ponencias', 'ProgramController@generatePapersVimeoUrls');
Route::get('/sesion-abierta-en-otro-dispositivo', 'UserController@informAnotherSessionOpened')->name('users.anotherSessionOpened');
Route::get('/calcula-porcentajes-visionado', 'SessionController@calculateNViews')->name('sessions.calculateNViews');

// Llamadas por AJAX
Route::get('sesiones/agendar/{id}', 'SessionController@addToSchedule')->name('sessions.addToSchedule');
Route::get('sesiones/desagendar/{id}', 'SessionController@removeFromSchedule')->name('sessions.removeFromSchedule');
Route::get('reevaluar-sesiones-solapadas', 'SessionController@reevaluateOverlappingSessions')->name('sessions.reevaluateOverlapping');
Route::get('sesiones/obtener-nota/{id}', 'SessionController@getNote')->name('session.getNote');
Route::get('sesiones/modificar-nota/{id}', 'SessionController@modifyNote')->name('sessions.modifyNote');
Route::get('sesiones/obtener-urls-calendar/{sessionId}', 'SessionController@getCalendarUrls')->name('sessions.getCalendarUrls');
Route::get('ver-notas', function() {
    $notes = new App\Models\Notebook();
    echo "Las notas son: " . dd($notes->get());
});
Route::get('ver-imagen/{urlImg}', function($urlArchivo) {
    $info = "<img src='{$urlArchivo}' style='width: 100%; max-width: 100%;'>";
    return view('info', compact('info'));
})->where('urlImg', '(.*)');
Route::get('ver-web/{url}', function($url) {
    $info = "<iframe src='{$url}' class='visor'></iframe>";
    $isVisor = true;
    return view('visor', compact('info', 'isVisor'));
})->where('url', '(.*)');

/*
|--------------------------------------------------------------------------
| Login de usuarios al programa virtual
|--------------------------------------------------------------------------
*/
require __DIR__.'/auth.php';


/*
|--------------------------------------------------------------------------
| Panel de administración (Laravel Voyager)
|--------------------------------------------------------------------------
*/
// Route::get('vrooms', 'VRoomController@showById');
Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
    Route::get('/vrooms/view-by-id/{id}', 'VRoomController@showById')->name('admin.vrooms.view-by-id');
});


/*
|--------------------------------------------------------------------------
| Útiles
|--------------------------------------------------------------------------
*/
Route::get('limpia-cache', function() {
    Artisan::call('optimize:clear');
    Artisan::call('cache:clear');
    Artisan::call('route:clear');
    Artisan::call('config:clear');
    Artisan::call('view:clear');
    return redirect('/');
});
