@foreach ($papers as $paper)
    @if ($paper->isPoster())
        <div class="mb-5 is-flex">
            {{__("Ordenar pósteres por puntuación")}}: <fieldset class="is-flex is-align-items-center">
                <input type="radio" class="radio ml-4" name="orden_posteres" value="1" id="orden_mas_alta" />
                <label for="orden_mas_alta" class="mx-2">{{__("Más alta")}}</label>
                <input type="radio" class="radio ml-2" name="orden_posteres" value="0" id="orden_mas_baja" />
                <label for="orden_mas_baja" class="mx-2">{{__("Más baja")}}</label>
        </div>
        @break
    @endif
@endforeach
