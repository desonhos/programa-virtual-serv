<script>

    /* Tras X tiempo, muestra el visor del vídeo */
    var sec = parseInt("{{ $secondsRemainingToShowVideo }}");
    var iframe = '{!! $iframe !!}';
    if ((sec*1000) <= 2147483647) {    // máximo retardo (https://stackoverflow.com/questions/3468607/why-does-settimeout-break-for-large-millisecond-delay-values)
        setTimeout(function() {
            console.log(sec);
            showVideoPlayer();
        }, sec * 1000);
    }

    /* Actualiza la cuenta atrás */
    var secToStartSession = "{{ $secondsRemainingToStart }}";
    updateCountdown();
    var intervalId = window.setInterval(function(){
        //console.log("Tiempo restante para mostrar el visor: " + seconds2human(sec));
        updateCountdown();
    }, 1000);

    function updateCountdown() {
        secToStartSession = secToStartSession - 1;
        $("#timeRemainingToStart").html(seconds2human(secToStartSession));
    }
    function showVideoPlayer() {
        console.log("show video player");
        $("#prevVideo").fadeOut(1000, function(){
            $("#sessionVideoContainer").html("").append(iframe);
            initVimeoIframe();
            $("#sessionVideoContainer").fadeIn(1000);
        });
        @if (!\SettingHelper::isApp())
            $("h1.title").html($("h1.title").html() + "<span class='is-live mt-2'>{{ __('¡En directo!') }}</span>");
        @else
            $("h1.title").html($("h1.title").html() + "<span class='is-live mt-2'>{{ __('¡Ahora!') }}</span>");
        @endif
    }
    function seconds2human(ss) {
        var s = ss%60;
        var m = Math.floor((ss%3600)/60);
        var h = Math.floor((ss%86400)/3600);
        var d = Math.floor((ss%2592000)/86400);
        var month = Math.floor(ss/2592000);

        // var res = "";
        // if (month > 0) { res = month + " meses, "; }
        // if (d > 0) { res += d + " días, "; }
        // if (h > 0) { res += h + " horas, "; }
        // if (m > 0) { res += m + " minutos, "; }
        // if (s > 0) { res += s + " segundos, "; }
        //return res.substring(0, res.length - 2);

        var res = "<ul class='countdown'>";
        var entro = false;
        if (entro || (month > 0)) {
            var unit = (month == 1) ? "mes" : "meses";
            res += "<li><span class='months'>" + month + "</span>" + unit + "</li>";
            entro = true;
        }
        if (entro || (d > 0)) {
            var unit = (d == 1) ? "día" : "días";
            res += "<li><span class='days'>" + d + "</span>" + unit + "</li>";
            entro = true;
        }
        if (entro || (h > 0)) {
            var unit = (h == 1) ? "hora" : "horas";
            res += "<li><span class='hours'>" + h + "</span>" + unit + "</li>";
            entro = true;
        }
        if (entro || (m > 0)) {
            var unit = (m == 1) ? "minuto" : "minutos";
            res += "<li><span class='minutes'>" + m + "</span>" + unit + "</li>";
            entro = true;
        }
        if (entro || (s > 0)) {
            var unit = (s == 1) ? "segundo" : "segundos";
            res += "<li><span class='seconds'>" + s + "</span>" + unit + "</li>";
            entro = true;
        }
        res += "</ul>";
        return res;
    }
</script>
