<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class RememberPass extends Mailable
{
    use Queueable, SerializesModels;
    public $defaultPassword;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($defaultPassword) {
        $this->defaultPassword = $defaultPassword;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build() {
        if(!empty($this->senderEmail)) {
            $this->replyTo($this->senderEmail);
        }
        return $this->markdown('emails.remember-pass')
                    ->subject('Recordar acceso a ' . config('app.name'));
    }
}
