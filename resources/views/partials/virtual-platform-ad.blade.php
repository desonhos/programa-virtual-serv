@if (
        ($session->getViewerVisibility() !== $session::PV_VIDEO_HIDDEN) &&
        SettingHelper::getFechaInicioAccesoVirtual()
)

    @php
    $promoVideo = $session->getPromoVideoVimeoUrl();
    $adImg = setting('web.webapp_ad_img');
    $adImg = (!empty($adImg)) ? \VoyagerHelper::getImageUrl($adImg) : config('web.event.defaultImg');
    @endphp

    @if (!empty($promoVideo))
        @include('partials._promo-video', ['showOnEnd' => '#virtualPlatformAd', 'video' => $promoVideo])
    @endif

    <div id="virtualPlatformAd" class="position-relative mb-4 @if (!empty($promoVideo)) is-hidden @endif">
        <div class="position-absolute w-100 h-100 has-background-black-semi is-flex
                    is-flex-direction-column is-justify-content-center is-align-items-center
                    has-text-white p-6">

            {{-- Si ya se ha iniciado el congreso virtual, invita al logueo --}}
            @if (!\SettingHelper::isAntesInicioVirtual())
                <p class="has-text-centered	has-text-weight-bold">
                    {{__("Sigue")}} {{ config('web.event.shortName') }} {{__("en directo o bajo demanda desde la Plataforma Virtual") }}
                </p>
                <a href="{{ route('virtual.sessions.view', compact('session')) }}" class="button is-outlined is-white mt-3 is-uppercase">{{ __("Accede") }}</a>
            @else
                {{-- Modo WebApp: dice a partir de cuándo se podrá acceder --}}
                <p class="has-text-centered	has-text-weight-bold">
                    {!! \SettingHelper::getTextAccessFrom(Lang::locale()) !!}
                </p>
            @endif
        </div>
        <img class="" src="{{ $adImg }}">
    </div>

@endif
