<div id="sessionPromoVideoContainer" style="padding:56.25% 0 0 0;position:relative;">
    <iframe src="{{ $video }}" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen style="position:absolute;top:0;left:0;width:100%;height:100%;"></iframe>
</div>

<script>
    $(document).ready(function() {
        initPromoVimeoIframe();
    });
    function initPromoVimeoIframe() {
        console.log("init promo video iframe");
        iframe = document.querySelector('#sessionPromoVideoContainer');
        if (iframe == null) { return; }
        playerPromo = new Vimeo.Player(iframe);

        playerPromo.on('ended', function() {
            jQuery("#sessionPromoVideoContainer").hide();
            jQuery('{{$showOnEnd}}').removeClass('is-hidden');
            player.play();
        });
    }
</script>