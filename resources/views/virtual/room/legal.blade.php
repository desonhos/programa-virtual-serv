{{-- Aviso legal (si coresponde) --}}
@if ($vroom->isExhibitorOrExhibition())
    <div id="exhibitorInfo" class="is-hidden"
        data-alert-title="Aviso Legal"
        data-alert-body="{{ $vroom->getLegal() }}"
        data-alert-confirm="{{ $vroom->getLegalYes() }}"
        data-alert-cancel='{{ $vroom->getLegalNo() }}'
        data-alert-redirect='{{ \SettingHelper::getUrlNoSanitario() }}'>
    </div>
@endif
