<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cookie;

/** La tabla speakers tiene todos los ponentes (y coordinadores de sesión) que
 * participan en cada evento. Para no tener que hacer una tabla que relacione
 * ponentes y ponencias, desde CYEX suben tantas veces los ponentes como ponencias
 * tengan en las que participan.
 * El campo id identifica a un ponente en una ponencia determinada. Es speaker_id
 * el que identifica a una persona (solo que aparecerá duplicado en la tabla pero
 * desde CYEX garantizan que los datos personales siempre serán iguales, así que
 * bastará con coger el primer registro que cumpla la condición de speaker_id).
 * En la tabla speakers, dentro del evento el campo id sí que es único
*/
class Speaker extends Model {

    protected $connection= 'cyex_online';

    public static function _get($speakerId) {
        return Speaker::where('speaker_id', $speakerId)
                            ->where('event_id', config('web.eventId'))
                            ->first();
    }

    /** Obtiene todos los ponentes del evento actual */
    public static function _getAll() {
        $speakers = Speaker::where('event_id', config('web.eventId'))->get();
        $s = [];
        // me quedo con el id de la tabla, que sí es único dentro del evento
        foreach ($speakers as $speaker) {
            $s[$speaker->getSpeakerId()] = $speaker->id;
        }
        $ids = array_values($s);
        
        $len = (int) count($ids);
        $firsthalf = array_slice($ids, 0, $len / 2);
        $secondhalf = array_slice($ids, $len / 2);

        $s1 = Speaker::where('event_id', config('web.eventId'))
                        ->whereIn('id', $firsthalf)
                        ->orderBy('surname')
                        ->orderBy('name')
                        ->get();

        $s2 = Speaker::where('event_id', config('web.eventId'))
                        ->whereIn('id', $secondhalf)
                        ->orderBy('surname')
                        ->orderBy('name')
                        ->get();
        
        $res = $s1->merge($s2);
        return $res->sort(
            function ($a, $b) {
                $aSurname = \TextHelper::removeAccents($a->surname);
                $bSurname = \TextHelper::removeAccents($b->surname);
                $aName = \TextHelper::removeAccents($a->name);
                $bName = \TextHelper::removeAccents($b->name);
                return strcmp($aSurname, $bSurname)
                    ?: strcmp($aName, $bName);
            }
        );
    }

    /** Busca el speaker por su id dentro del listado pasado por parámetro */
    public static function _getFromList($speakerId, $speakers) {
        foreach ($speakers as $speaker) {
            if (($speaker->getSpeakerId() == $speakerId) && ($speaker->getEventId() == config('web.eventId'))) {
                return $speaker;
            }
        }
        return null;
    }

    /** Obtiene las sesiones en las que participa el ponente de speaker_id pasado */
    public static function _getSessions($speakerId) {
        $paperIds = Speaker::_getPaperIds($speakerId);
        return Session::_getWhereHasPaperIn($paperIds);
    }

    /** Obtiene las ponencias en las que participa el ponente de speaker_id pasado */
    public static function _getPapers($speakerId) {
        $paperIds = Speaker::_getPaperIds($speakerId);
        return Paper::_getWhereIn($paperIds);
    }

    public static function _getPaperIds($speakerId) {
        return Speaker::where('speaker_id', $speakerId)
                            ->where('event_id', config('web.eventId'))
                            ->pluck('paper_id')
                            ->toArray();
    }

    public static function search($text) {
        $searchedWords = explode(" ", $text);
        return Speaker::when(!empty($searchedWords), function ($query) use ($searchedWords) {
            $query->where(function ($query) use ($searchedWords) {
                foreach ($searchedWords as $word) {
                    $query->where(function ($query) use ($word) {
                        $query->orWhere('name', 'like', "%{$word}%");
                        $query->orWhere('surname', 'like', "%{$word}%");
                        // $query->orWhere('center', 'like', "%{$word}%");
                    });
                }
            });
        })
        ->where('event_id', config('web.eventId'))
        ->get()
        ->unique('speaker_id');
    }

    /** Obtiene las sesiones en las que partica el ponente actual */
    public function getSessions() {
        return Speaker::_getSessions($this->getSpeakerId());
    }

    /** Obtiene las ponencias en las que participa el ponente actual */
    public function getPapers() {
        return Speaker::_getPapers($this->getSpeakerId());
    }

    /************************** GETTERS ***************************************/

    public function getId() { return $this->id; }

    public function getEventId() { return $this->event_id; }

    public function getSpeakerId() { return $this->speaker_id; }

    public function getName() { return trim($this->name); }

    public function getSurname() { return trim($this->surname); }

    public function getCenter() { return $this->center; }

    public function getImageUrl() {
        $url = "https://365.dataeventservices.net/recursos/" . config('web.eventId') . "/speakers/" . $this->getSpeakerId() . ".png";
        return $url;
        // $ch = curl_init();
        // curl_setopt($ch, CURLOPT_URL,$url);
        // // don't download content
        // curl_setopt($ch, CURLOPT_NOBODY, 1);
        // curl_setopt($ch, CURLOPT_FAILONERROR, 1);
        // curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        // $result = curl_exec($ch);
        // curl_close($ch);
        // if ($result !== FALSE) {
        //     return $url;
        // }

        // return false;
    }

    /************************** ISERS ***************************************/


}
