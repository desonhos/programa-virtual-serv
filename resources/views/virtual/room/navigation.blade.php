@if ($vroom->isExhibitor() && ($vroom->getSlug() !== "area-mype") )
    <a id="prevStandLink" href="{{ route('virtual.vroom.view-prev', ['slug' => $vroom->getSlug()]) }}">
        @svg('ico-back', 'ico-back')
    </a>
    <a id="nextStandLink" href="{{ route('virtual.vroom.view-next', ['slug' => $vroom->getSlug()]) }}">
        @svg('ico-back', 'ico-back')
    </a>
    <a id="backToExhibitionButton" href="{{ route('virtual.exhibition') }}">
        @svg('ico-volver-trans')
    </a>
@endif
