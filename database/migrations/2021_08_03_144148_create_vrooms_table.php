<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVroomsTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'vrooms';

    /**
     * Run the migrations.
     * @table vrooms
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 64);
            $table->string('slug', 64)->unique();
            $table->string('background')->nullable();
            $table->json('mapareas')->nullable();
            $table->longText('legal')->nullable();
            $table->string('legal_yes', 64)->nullable();
            $table->string('legal_no', 64)->nullable();
            $table->integer('is_exhibitor')->default(0);
            $table->integer('pos')->default(99);
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
