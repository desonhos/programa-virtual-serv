<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class AskPaper extends Mailable
{
    use Queueable, SerializesModels;
    public $senderName;
    public $senderEmail;
    public $message;
    public $paper;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($requestParams, $paper) {
        $this->senderName = !empty($requestParams['name']) ? $requestParams['name'] : null;
        $this->senderEmail = !empty($requestParams['email']) ? $requestParams['email'] : null;
        $this->message = !empty($requestParams['message']) ? $requestParams['message'] : null;
        $this->paper = $paper;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build() {
        if(!empty($this->senderEmail)) {
            $this->replyTo($this->senderEmail);
        }
        // return $this->view('view.name');
        return $this->markdown('emails.ask-paper')
                    ->subject('Consulta enviada desde ' . config('app.name'));
    }
}
