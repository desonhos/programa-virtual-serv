<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class WebappController extends Controller {
    public function showHome(Request $request) {
        if (isset($request->addToHomescreen)) {
            $request->session()->get('key');
            if (!$request->session()->has('showAddToHomescreen')) {
                return redirect(route('inicio-webapp'));
            }
            $request->session()->forget('showAddToHomescreen');
        }
        return view('inicio-webapp');
    }
    public function showAddToHomescreen() {
        session(['showAddToHomescreen' => '1']);
        return redirect(route('inicio-webapp', ['addToHomescreen' => 1]));
    }
}
