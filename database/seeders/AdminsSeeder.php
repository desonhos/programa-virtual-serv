<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
use TCG\Voyager\Models\Role;
use TCG\Voyager\Models\User;

class AdminsSeeder extends Seeder {

     public function run() {
         echo "\e[0;32;40mCreando:\e[0m administradores\n";
         $this->importAdmins();
         echo "\e[0;33;40mCreados:\e[0m administradores\n";
     }

     protected function importAdmins() {
         $role = Role::where('name', 'admin')->firstOrFail();
         $user = User::firstOrCreate(['email' => 'pablo@desonhos.net'],
             [
                 'name'           => 'Pablo Desoños',
                 'email'          => 'pablo@desonhos.net',
                 'avatar'         => 'users/default.png',
                 'password'       => bcrypt('pablo@desonhos.net'),
                 'remember_token' => Str::random(60),
                 'role_id'        => $role->id,
             ]
         );
         $user = User::firstOrCreate(['email' => 'apregue@cyex.es'],
             [
                 'name'           => 'Aldara Pregue',
                 'email'          => 'apregue@cyex.es',
                 'avatar'         => 'users/default.png',
                 'password'       => bcrypt('cyex'),
                 'remember_token' => Str::random(60),
                 'role_id'        => $role->id,
             ]
         );
         $user = User::firstOrCreate(['email' => 'sbalsa@cyex.es'],
             [
                 'name'           => 'Sandra Balsa',
                 'email'          => 'sbalsa@cyex.es',
                 'avatar'         => 'users/default.png',
                 'password'       => bcrypt('cyex'),
                 'remember_token' => Str::random(60),
                 'role_id'        => $role->id,
             ]
         );
         $user = User::firstOrCreate(['email' => 'mrey@cyex.es'],
             [
                 'name'           => 'María Rey',
                 'email'          => 'mrey@cyex.es',
                 'avatar'         => 'users/default.png',
                 'password'       => bcrypt('cyex'),
                 'remember_token' => Str::random(60),
                 'role_id'        => $role->id,
             ]
         );
     }
}
