<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use TCG\Voyager\Models\DataRow;
use TCG\Voyager\Models\DataType;
use TCG\Voyager\Models\Menu;
use TCG\Voyager\Models\MenuItem;
use TCG\Voyager\Models\Permission;
use TCG\Voyager\Models\Role;
use \App\Models\Anuncio;

class AnunciosTableSeeder extends Seeder {
    private $tableName = 'anuncios';
    private $dataTypeSlug = 'anuncios';

    public function run() {
        $dataType = $this->createDataType();
        $this->createDataRows($dataType);
        $this->createAdminMenuItem();
        $this->generatePermissions();
    }

    public function createDataType() {
        $dataType = $this->dataType('slug', $this->dataTypeSlug);
        if (!$dataType->exists) {
            $dataType->fill([
                'name'                  => $this->tableName,
                'display_name_singular' => 'Anuncio',
                'display_name_plural'   => 'Anuncios',
                'icon'                  => 'voyager-dollar',
                'model_name'            => 'App\\Models\\Anuncio',
                // 'controller'            => 'App\\Http\\Controllers\\Voyager\\VoyagerImageController',
                'generate_permissions'  => 1,
                'server_side'           => 0,
                'details'               => [
                    "order_column" => null,
                    "order_display_column" => null,
                    "order_direction" => "asc",
                    "default_search_key" => null,
                    "scope" => null
                ]
            ])->save();
        }
        return $dataType;
    }

    public function createDataRows($dataType) {
        $dataRow = $this->dataRow($dataType, 'id');
        if (!$dataRow->exists) {
            $dataRow->fill([
                'type'         => 'number',
                'display_name' => 'ID',
                'required'     => 1,
                'browse'       => 0,
                'read'         => 0,
                'edit'         => 0,
                'add'          => 0,
                'delete'       => 0,
                'order'        => 1,
            ])->save();
        }
        $dataRow = $this->dataRow($dataType, 'imagen');
        if (!$dataRow->exists) {
            $dataRow->fill([
                'type'         => 'image',
                'display_name' => 'Imagen',
                'required'     => 0,
                'browse'       => 1,
                'read'         => 1,
                'edit'         => 1,
                'add'          => 1,
                'delete'       => 1,
                'details' => [
                    'resize' => [
                        'width' => "1200",
                        'height' => null
                    ],
                    'quality' => "100%",
                    'upsize' => false,
                ],
                'order'        => 2,
            ])->save();
        }
        $dataRow = $this->dataRow($dataType, 'imagen_vertical');
        if (!$dataRow->exists) {
            $dataRow->fill([
                'type'         => 'image',
                'display_name' => 'Imagen vertical',
                'required'     => 0,
                'browse'       => 1,
                'read'         => 1,
                'edit'         => 1,
                'add'          => 1,
                'delete'       => 1,
                'details' => [
                    'resize' => [
                        'width' => "1200",
                        'height' => null
                    ],
                    'quality' => "100%",
                    'upsize' => false,
                ],
                'order'        => 3,
            ])->save();
        }
        $dataRow = $this->dataRow($dataType, 'video');
        if (!$dataRow->exists) {
            $dataRow->fill([
                'type'         => 'media_picker',
                'display_name' => 'Vídeo',
                'required'     => 0,
                'browse'       => 1,
                'read'         => 1,
                'edit'         => 1,
                'add'          => 1,
                'delete'       => 1,
                'order' => 4,
            ])->save();
        }
        $dataRow = $this->dataRow($dataType, 'video_vertical');
        if (!$dataRow->exists) {
            $dataRow->fill([
                'type'         => 'media_picker',
                'display_name' => 'Vídeo vertical',
                'required'     => 0,
                'browse'       => 1,
                'read'         => 1,
                'edit'         => 1,
                'add'          => 1,
                'delete'       => 1,
                'order' => 5,
            ])->save();
        }
        $dataRow = $this->dataRow($dataType, 't_min_cierre');
        if (!$dataRow->exists) {
            $dataRow->fill([
                'type'         => 'number',
                'display_name' => 'Tiempo (s) mínimo antes de poder cerrar',
                'required'     => 0,
                'browse'       => 1,
                'read'         => 1,
                'edit'         => 1,
                'add'          => 1,
                'delete'       => 1,
                'order' => 6,
            ])->save();
        }
        $dataRow = $this->dataRow($dataType, 't_cierre_auto');
        if (!$dataRow->exists) {
            $dataRow->fill([
                'type'         => 'number',
                'display_name' => 'Tiempo (s) tras el que se cierra automáticamente',
                'required'     => 0,
                'browse'       => 1,
                'read'         => 1,
                'edit'         => 1,
                'add'          => 1,
                'delete'       => 1,
                'order'        => 7,
            ])->save();
        }
        $dataRow = $this->dataRow($dataType, 'probabilidad_mostrar');
        if (!$dataRow->exists) {
            $dataRow->fill([
                'type'         => 'number',
                'display_name' => 'Probabilidad (%) de que se muestre el anuncio (0: nunca, 100: siempre)',
                'required'     => 0,
                'browse'       => 1,
                'read'         => 1,
                'edit'         => 1,
                'add'          => 1,
                'delete'       => 1,
                'order'        => 8,
            ])->save();
        }
        $dataRow = $this->dataRow($dataType, 'created_at');
        if (!$dataRow->exists) {
            $dataRow->fill([
                'type'         => 'timestamp',
                'display_name' => 'Fecha de creación',
                'required'     => 0,
                'browse'       => 0,
                'read'         => 0,
                'edit'         => 0,
                'add'          => 0,
                'delete'       => 0,
                'order'        => 9,
            ])->save();
        }
        $dataRow = $this->dataRow($dataType, 'updated_at');
        if (!$dataRow->exists) {
            $dataRow->fill([
                'type'         => 'timestamp',
                'display_name' => 'Última modificación',
                'required'     => 0,
                'browse'       => 0,
                'read'         => 0,
                'edit'         => 0,
                'add'          => 0,
                'delete'       => 0,
                'order'        => 10,
            ])->save();
        }
    }

    public function createAdminMenuItem() {
        $menu = Menu::where('name', 'admin')->firstOrFail();
        $menuItem = MenuItem::firstOrNew([
            'menu_id' => $menu->id,
            'title'   => 'Anuncios',
            'url'     => '',
            'route'   => 'voyager.'.$this->dataTypeSlug.'.index',
        ]);
        if (!$menuItem->exists) {
            $menuItem->fill([
                'target'     => '_self',
                'icon_class' => 'voyager-dollar',
                'color'      => null,
                'parent_id'  => null,
                'order'      => 4,
            ])->save();
        }
    }

    /**
     * [dataRow description].
     *
     * @param [type] $type  [description]
     * @param [type] $field [description]
     *
     * @return [type] [description]
     */
    protected function dataRow($type, $field) {
        return DataRow::firstOrNew([
            'data_type_id' => $type->id,
            'field'        => $field,
        ]);
    }

    /**
     * [dataType description].
     *
     * @param [type] $field [description]
     * @param [type] $for   [description]
     *
     * @return [type] [description]
     */
    protected function dataType($field, $for) {
        return DataType::firstOrNew([$field => $for]);
    }

    protected function generatePermissions() {
        Permission::generateFor($this->tableName);

        $role = Role::where('name', 'admin')->firstOrFail();

        $permissions = Permission::all();

        $role->permissions()->sync(
            $permissions->pluck('id')->all()
        );
    }

}
