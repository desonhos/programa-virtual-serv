<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use setasign\Fpdi\Fpdi;

class CertificateController extends Controller {

    /** Generar certificado pdf con nombre de usuario */
    /* Documentación:
    https://manuals.setasign.com/fpdi-manual/v2/the-fpdi-class/#index-3
    http://www.fpdf.org/ */
    public function generate() {
        // $pdfW = 2923;
        // $pdfH = 2067;
        // $certificateWidth = 800; // el certificado tendrá este ancho y el alto será el proporcional en función del tamaño de las dos anteriores variables
        $pdf = new Fpdi();
        $pdf->AddPage();
        // PDF que queremos modificar
        $pdf->setSourceFile(config('web.event.pdfAttendanceCertificateLink'));
        // importar la página 1
        $tplIdx = $pdf->importPage(1);
        // usar la página importada y colocarla en el punto 0,0; calcular ancho y alto
        // automáticamente reproducir y ajustar el tamaño de la página al tamaño de la página importada
        $pdf->useImportedPage($tplIdx, 0, 0, 292, 206, true); // "x", "y", "width", "height", "adjustPageSize" (width  y height que tengan las mismas proporciones que la imagen original)
        //$pdf->useImportedPage($tplIdx, 0, 0, $certificateWidth, $certificateWidth * $pdfH / $pdfW, true);
        // establecemos un estilo de fuente y escribimos el nombre de usuario
        // $pdf->SetFont('Arial', 'B', '14');
        $pdf->SetFont('Arial', '', '14');
        $pdf->SetTextColor(0, 83, 127);#00537F
        // $pdf->SetXY(99, 101); // establecer posición en documento pdf
        $pdf->SetY(100); // establecer posición en documento pdf
        //$pdf->Write(0, utf8_decode(auth()->user()->name)); // el primer parámetro define la altura de la línea
        // $pdf->Write(0, utf8_decode(\Auth::guard('multi')->user()->getCompleteName()));
        $name = utf8_decode(\Auth::guard('multi')->user()->getCompleteName());
        $pdf->Cell(0, 0, $name, 0, 1, 'C'); // Texto centrado en una celda (sin borde) con cuadro 40*0 mm y salto de línea
        
        // forzar al navegador a descargar la salida
        $nombreArchivo = \Illuminate\Support\Str::slug(config('web.event.shortName'));
        $pdf->Output('D', __('certificado-asistencia') . '-' . $nombreArchivo . '.pdf');
    }
}
