<?php

namespace App\Models;
use Illuminate\Support\Facades\Cookie;

// TODO: hacer esto un ServiceProvider para que solo se pueda instanciar una vez
class Notebook {

    protected $connection= 'cyex_online';
    private $cookieName = 'notas';
    private $cookieExpirationTime = 525600; // un año (se ponen minutos)

    function getSessionNote($sessionId) {
        $notes = Cookie::get($this->cookieName);
        if (empty($notes)) { return ""; }
        $notes = json_decode($notes, true);
        if (isset($notes[$sessionId])) { return $notes[$sessionId]; }
        else { return ""; }
    }

    /** Asocia una nota a una sesión */
    function modifySessionNote($sessionId, $text) {
        $notes = Cookie::get($this->cookieName);
        if (empty($notes)) { $notes = array(); }
        else { $notes = json_decode($notes, true); }
        $notes[$sessionId] = $text;
        Cookie::queue(Cookie::make($this->cookieName, json_encode($notes), $this->cookieExpirationTime));
    }

    /** Devuelve verdadero en caso de que la sesión tenga alguna nota asociada */
    function isSessionInNotebook($sessionId) {
        $notes = Cookie::get($this->cookieName);
        $notes = json_decode($notes, true);
        if (empty($notes) || (!isset($notes[$sessionId]))) {
            return false;
        }
        return true;
    }

    /** Devuelve el contenido completo de las notas */
    function get() {
        $notes = Cookie::get($this->cookieName);
        if (empty($notes)) {
            return null;
        }
        return json_decode($notes, true);
    }

}
