@php $schedule = new App\Models\Schedule(); @endphp

<ul id="sessionActions" class="links-list is-horizontal mb-4">
    <li class="">
        <a id="sharePage" class="noDynamicStyles has-text-text is-flex is-align-items-baseline">
            <span class="mr-1 is-size-7">{{__("Compartir") }}</span>
            @svg('ico-compartir', 'ico-compartir')
        </a>
    </li>
    <li class="">
        <a class="anotar noDynamicStyles has-text-text is-flex is-align-items-baseline"
            href="{{ route('sessions.modifyNote', ['id' => $session->getId() ]) }}"
            data-get-note="{{ route('session.getNote', ['id' => $session->getId() ]) }}">
            <span class="mr-1 is-size-7">{{__("Notas")}}</span>
            @if ($session->hasNote()) 
                @svg('ico-notas-selected', 'ico-notas')
                @if (Request::get('note') == 1)
                    <script>
                        $(document).ready(function() {
                            setTimeout(function(){
                                $('svg.ico-notas').click();
                            }, 10);
                        });
                    </script>
                @endif
            @else
                @svg('ico-notas', 'ico-notas')
            @endif
        </a>
    </li>
    <li class="">
        @php
            $showAgendar = ($session->isInSchedule()) ? "style=\"display: none;\"" : "style=\"display: flex;\"";
            $showAgendada = (!$session->isInSchedule()) ? "style=\"display: none;\"" : "style=\"display: flex;\"";
        @endphp
        <a class="agendar noDynamicStyles has-text-text is-align-items-baseline" {!! $showAgendar !!}
            href='{{ route('sessions.addToSchedule', ['id' => $session->getId()]) }}'>
            <span class="mr-1 is-size-7">{{__("Mi agenda")}}</span>
            @svg('ico-agendar', 'ico-agendar')
        </a>
        <a class="agendada noDynamicStyles has-text-text is-align-items-baseline"  {!! $showAgendada !!}
            href='{{ route('sessions.removeFromSchedule', ['id' => $session->getId() ]) }}'>
            @php
                $isOverlapping = ($schedule->isSessionInSchedule($session->getId()) && $schedule->isSetAsOverlapping($session->getId())) ? true : false;
                $classAgendada = 'ico-agendada';
                $classAgendaConflicto = 'ico-agenda-conflicto';
                if ($isOverlapping) { $classAgendada .= ' is-hidden';
                } else { $classAgendaConflicto .= ' is-hidden'; }
            @endphp
            <span class="mr-1 is-size-7">{{__("Mi agenda")}}</span>
            @svg('ico-agendada', $classAgendada)
            @svg('ico-agenda-conflicto', $classAgendaConflicto)
        </a>
    </li>
</ul>

@include('notes._modal')
