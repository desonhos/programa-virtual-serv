<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Room extends Model {

    protected $connection= 'cyex_online';

    /** Obtiene todas las del evento actual */
    public static function _getAll() {
        $rooms = Room::where('event_id', config('web.eventId'))
                    ->orderBy('orden')
                    ->orderBy('name')
                    ->get();
        return $rooms;
    }

    /** Obtiene por su id, del evento definido en config */
    public static function _get($roomId) {
        return Room::where('id', $roomId)->where('event_id', config('web.eventId'))->first();
    }

    /** Busca en el listado, aquel cuyo id se pasa por parámetro */
    public static function _find($id, $rooms = null) {
        if (empty($rooms)) {
            return Room::_get($id);
        }
        foreach ($rooms as $room) {
            if ($room->id == $id) {
                return $room;
            }
        }
        return null;
    }

    public function getName() {
        return $this->name;
    }

    public function getNameAbbr() {
        $field = 'name_abbr';
        return !empty($this->{$field}) ? $this->{$field} : $this->getName();
    }

    public function getId() {
        return $this->id;
    }
}
