@if (!empty($items))
    @foreach($items as $menuItem)
        <a class="navbar-item noDynamicStyles" href="{{ $menuItem->link() }}" target="{{ $menuItem->target }}">
            <span class="navbar-item-text">{{ $menuItem->title }}</span>
        </a>
    @endforeach

    @if (!empty($linkActosSociales))
        <a class="navbar-item noDynamicStyles" href="{{ $linkActosSociales }}">
            <span class="navbar-item-text">{{ __("Actos sociales") }}</span>
        </a>
    @endif
    <a class="navbar-item noDynamicStyles hideOnStart" href="{{ route('inicio-webapp') }}">
        <span class="navbar-item-text">{{ __("Menú principal") }}</span>
    </a>
    <a class="navbar-item noDynamicStyles hideOnStart showOnStartMobile" href="{{ route('webapp.add-to-homescreen') }}">
        <span class="navbar-item-text">{{ __("Añadir a la página de inicio") }}</span>
    </a>
    @if (!empty($linkWebInscripcion))
        <a class="navbar-item has-background-grey noDynamicStyles hideOnStart showOnStartMobile"
            href="{{ $linkWebInscripcion }}" target="_blank">
            <span class="navbar-item-text">{{ __("Web Inscripción") }}</span>
        </a>
    @endif
    {{-- @if (!\Auth::guard('multi')->user())
        <a class="navbar-item noDynamicStyles isHiddenInNavbar has-background-color-congreso2" href="{{ route('login') }}">
            <span class="navbar-item-text is-color-plataforma">Iniciar sesión</span>
        </a>
    @endif --}}
@else
        no hay items
@endif