import Alert from '@vizuaalog/bulmajs/src/plugins/alert';

$(document).ready(function() {
    showLegalAlert();
});

function showLegalAlert() {
    var alertTitle = $("#exhibitorInfo").data('alert-title');
    var alertBody = $("#exhibitorInfo").data('alert-body');
    var alertConfirm = $("#exhibitorInfo").data('alert-confirm');
    var alertCancel = $("#exhibitorInfo").data('alert-cancel');
    var redirect = $("#exhibitorInfo").data('alert-redirect');
    Alert().alert({
        type: 'warning',
        title: alertTitle,
        body: alertBody,
        confirm: {
            label: alertConfirm,
            classes: ['accept']
            // classes: ['is-link', 'pl-0', 'letter-spacing-0', 'has-border-0'],
            // onClick: function() {
            //     confirmAddToSchedule(url, $ico);
            // }
        },
        cancel: {
            label: alertCancel,
            classes: ['cancel'],
            onClick: function() {
                location.href = redirect;
            }
        }
    });
}
