<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class View extends Model {

    protected $connection= 'cyex_online';
    protected $table = 'pv_views';
    public $timestamps = false;
    protected static $_table = 'pv_views';

    /** Obtiene todos los del evento actual */
    public static function _getAll() {
        return View::where('event_id', config('web.eventId'))->get();
    }

    /** Obtiene por id de usuario e id de sesión, del evento definido en config en el día actual*/
    public static function _get($userIdInEvent, $sessionId) {
        return View::where('user_id_in_event', $userIdInEvent)
                    ->where('event_id', config('web.eventId'))
                    ->where('session_id', $sessionId)
                    ->where('day', date("Y-m-d"))
                    ->first();
    }

    /** Obtiene todos los del evento actual del usuario cuyo id se pasa por parámetro */
    public static function _getAllWhereUser($userId) {
        return View::where('user_id_in_event', $userId)
                    ->where('event_id', config('web.eventId'))
                    ->get();
    }

    /** Obtiene todos los del evento actual del usuario actual */
    public static function _getAllWhereCurrentUser() {
        $user = \Auth::guard('multi')->user();
        if (empty($user)) { return null; }
        return View::_getAllWhereUser($user->getIdInEvent());
    }

    /** Contabiliza un tiempo de visualización de un usuario en una sesión (en el evento actual)*/
    public static function _addSeconds($userIdInEvent, $sessionId, $seconds, $lastSecondPlayed) {
        $view = View::_get($userIdInEvent, $sessionId);
        if (!$view) {
            View::_insert($userIdInEvent, $sessionId);
            $view = View::_get($userIdInEvent, $sessionId);
        }
        $view->setSeconds($view->getSeconds() + $seconds, $lastSecondPlayed);
    }

    public static function _insert($userIdInEvent, $sessionId) {
        \DB::connection('cyex_online')->table(View::$_table)->insert([
            'user_id_in_event' => $userIdInEvent,
            'event_id' => config('web.eventId'),
            'session_id' => $sessionId,
            'seconds' => 0,
            'day' => date("Y-m-d")
        ]);
    }

    public static function _getLastSecondPlayedByCurrentUser($sessionId) {
        $user = \Auth::guard('multi')->user();
        $view = View::where('user_id_in_event', $user->getIdInEvent())
                    ->where('event_id', config('web.eventId'))
                    ->where('session_id', $sessionId)
                    ->orderBy('day', 'desc')
                    ->first();
        if (!$view) { return false; }
        return $view->getLastSecondPlayed();
    }

    /************************** GETTERS ***************************************/
    public function getSeconds() { return $this->seconds; }
    public function getUserIdInEvent() { return $this->user_id_in_event; }
    public function getEventId() { return $this->event_id; }
    public function getSessionId() { return $this->session_id; }
    /** Obtiene el punto en el que se quedó el usuario la última vez que reprodujo el vídeo */
    public function getLastSecondPlayed() { return $this->last_second_played; }

    /************************** SETTERS ***************************************/
    // TODO: No he podido hacerlas con Eloquent porque no admite claves primarias compuestas
    public function setSeconds($seconds, $lastSecondPlayed) {
        $params = ['seconds' => $seconds];
        if ($lastSecondPlayed !== 'false') {
            $params = [
                'seconds' => $seconds,
                'last_second_played' => $lastSecondPlayed
            ];
        }
        \DB::connection('cyex_online')->table($this->table)
              ->where('user_id_in_event', $this->getUserIdInEvent())
              ->where('event_id', $this->getEventId())
              ->where('session_id', $this->getSessionId())
              ->where('day', date("Y-m-d"))
              ->update($params);
    }

    /** Almacena el tanto por uno visto del vídeo
    * @param int $totalSeconds es el número de segundos que dura la sesión (diferencia entre hora de fin y de inicio)
    */
    public function setNViews($totalSeconds) {
        $nViews = 0;
        if ($totalSeconds == 0) {
            // pasa cuando la fecha de inicio y de fin de la sesión coinciden (no debería ser así, pero resulta que ponen que la inauguración empieza y termina a la misma hora por ejemplo)
            if ($this->seconds > 0) {
                $nViews = 1;
            }
        }
        else {
            $nViews = $this->seconds/$totalSeconds;
        }
        \DB::connection('cyex_online')->table($this->table)
              ->where('user_id_in_event', $this->getUserIdInEvent())
              ->where('event_id', $this->getEventId())
              ->where('session_id', $this->getSessionId())
              ->update(['nviews' => $nViews]);
    }
}
