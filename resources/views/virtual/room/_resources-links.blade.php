{{-- Ventanas emergentes de los vídeos --}}
@foreach ($mapareas as $maparea)
    @if (isVideo($maparea))
        @include('partials._modal-video', [
            'videoId' => getId($maparea),
            'videoTitle' => getNombre($maparea),
            'videoFile' => getHref($maparea)
        ])
    @endif
@endforeach

@include('virtual.room._modal-links')
