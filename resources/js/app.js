// require('jquery');
require('./modules/schedule');
require('./modules/notes');
require('./modules/share');
require('./modules/navbar');
require('./modules/portfolio');
require('./modules/addToHomescreen');

$(document).ready(function() {
    // Modal de preguntas
    $(".ask-link").click(function() {
        showAskForm($(this).data('paper-id'));
    })
    $(".modal-close").click(function() {
        $(this).parent(".modal").removeClass("is-active");
    })
    // Envío de preguntas al moderador
    setAskformHandler();

    // Mostrar ventana de evaluación de la sesión
    setClickEvaluateHandler();

    setClickTelevotoHandler();

    setDropdownHandler();

    setBackToTopHandler();

    utils();
})

/** Muestra el formulario de envío de preguntas por ponencias */
function showAskForm(paperId) {
    console.log(paperId);
    $("#modal-ask-" + paperId).addClass("is-active");
}

/** Inicializa manejador de formulario de preguntas de ponencia*/
function setAskformHandler() {
    $(".ask-form").each(function(index) {
        $(this).submit(function(ev) {
            ev.preventDefault();
            var $form = $(this);
            $.ajax({
                type: $(this).attr("method"),
                url: $(this).attr("action"),
                data: $(this).serialize(),
            	success: function(respuesta) {
            		console.log(respuesta);
                    $form.find(".result").html("Mensaje enviado correctamente")
                        .removeClass("is-danger").addClass("is-success");
            	},
                error: function(response) {
                    var res = JSON.parse(response.responseText);
                    var ul = "<ul>";
                    for (var i in res.errors) {
                        for (var j=0; j<res.errors[i].length; j++) {
                            ul = ul + "<li>" + res.errors[i][j] + "</li>";
                        }
                    }
                    var ul = ul + "</ul>";
                    $form.find(".result").addClass("is-danger").removeClass("is-success").html(ul);
                }
            });
        })
    })
}

/** Mostrar ventana de evaluación de la sesión */
function setClickEvaluateHandler() {
    $("#button-survey").click(function(){
        $("#modal-survey").addClass("is-active")
    });
}
function setClickTelevotoHandler() {
    $("#button-televoto").click(function(){
        $("#modal-televoto").addClass("is-active")
    });
}

function setDropdownHandler() {
    $(".dropdown span").click(function(){
        $(this).parent().toggleClass('is-selected');
    });
    $("#close-dropdownFilter").click(function(){
        $("#dropdownFilter span").click();
    });
}

function setBackToTopHandler() {
    var btn = $('#back-to-top');

    $(window).scroll(function() {
        // console.log("scrolling: " + $(window).scrollTop());
        if ($(window).scrollTop() > 300) {
            // console.log("le añade la clase show al botón de volver arriba");
            btn.addClass('show');
        } else {
            btn.removeClass('show');
        }
    });

    btn.on('click', function(e) {
        e.preventDefault();
        $('html, body').animate({scrollTop:0}, '300');
    });
}

function utils() {
    $(".on-click-submit").click(function() {
        $(this).closest("form").submit();
    });
    addClassIosSafari();
}

function addClassIosSafari() {
    var is_ios = /iP(ad|od|hone)/i.test(window.navigator.userAgent);
    var is_safari = !!navigator.userAgent.match(/Version\/[\d\.]+.*Safari/);
    if ( is_ios && is_safari ) {
        var $html = document.documentElement;
        var classes = $html.className.concat(' is-ios-safari');
        $html.className = classes;
    }
}
