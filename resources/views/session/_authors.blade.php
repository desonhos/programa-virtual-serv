@php
$authors = $paper->getAuthors();
@endphp
@if (!empty($authors))
    <ul class="authors">
        <li class="author has-text-14-mobile">
            {{-- <span class="initials">{{ TextHelper::getInitials($authors) }}</span> --}}
            <span>{!! nl2br($authors) !!}</span>
        </li>
        @php
        $current = strtotime(date("Y-m-d"));
        $date    = strtotime("2021-06-20");
        $datediff = $date - $current;
        $difference = floor($datediff/(60*60*24));
        @endphp
        @if (!\SettingHelper::isApp() && $paper->getVisibleMailcontact() && !$session->isTimeToLivechat())
            <li><a class="ask-link" data-paper-id="{{ $paper->getId() }}">{{__("Realizar pregunta")}}</a></li>
        @endif
    </ul>
@endif
