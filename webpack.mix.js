const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
    .js('resources/js/virtual.js', 'public/js')
    .js('resources/js/pages/program.js', 'public/js/pages')
    .js('resources/js/pages/speakersView.js', 'public/js/pages')
    .js('resources/js/pages/exhibitor.js', 'public/js/pages')
    .js('resources/js/pages/room.js', 'public/js/pages')
    .js('resources/js/pages/session.js', 'public/js/pages')
    .copy('resources/js/pages/programSchemeView.js', 'public/js/pages')
    .copy('resources/event', 'public/event')
    .copy('resources/js/voyager.js', 'public/js')
    .sass('resources/sass/app.scss', 'public/css');

if (mix.inProduction()) {
    mix.version();
}
