<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SurInfoevent extends Model {

    protected $connection= 'cyex_online';
    protected $table = 'sur_infoevents';

    public static function _getCurrent() {
        return SurInfoevent::where('event_id', config('web.eventId'))->first();
    }

    public function getLimitDate() {
        return $this->limit_date;
    }
}
