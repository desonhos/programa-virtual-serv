{{-- Vista lista de las sesiones del día $day --}}
@php
$schedule = new App\Models\Schedule();
@endphp
<script>
var reevaluateOverlappingSessionsUrl = '{{ route('sessions.reevaluateOverlapping') }}';
</script>
<div class="table sessions-table">
    <div class="thead">
        <div class="tr">
            <div class="th isDay">{{ __("Día / Hora") }}</div>
            <div class="th isTitle">{{ __("Título sesión") }}</div>
            <div class="th isType">@if (FilterHelper::_isArea()) {{__("Clasificación")}} @else {{__("Tipo sesión")}} @endif</div>
            {{-- <div class="th isN">Nº sesión</div> --}}
            <div class="th isRoom">{{__("Sala")}}</div>
            <div class="th isSchedule">{{__("Mi agenda")}}</div>
        </div>
    </div>
    <div class="tbody">
        @foreach ($sessions as $session)
            @php
                $area = FilterHelper::_isArea() ? $session->getArea($areas) : null;
                $sessiontype = FilterHelper::_isSessiontype() ? $session->getSessiontype($sessiontypes) : null;
                $color = "";
                if (!empty($area)) { $color = $area->getColor(true); }
                if (!empty($sessiontype)) { $color = $sessiontype->getColor(true); }
            @endphp
            @if (!empty($agenda) || $session->hasDay($day) || ($session->isPoster() && empty($day)))
                <div class="tr is-hidden-mobile @if (!empty($currentUserViews) && $session->hasAnyView($currentUserViews)) visited @endif">
                    <div class="th isDay">
                        <span class="day">{{ DateHelper::formatMini($session->getDate(), Lang::locale()) }}</span>
                        <span class="hour">
                            @php $hour = $session->getHour(); @endphp
                            @if (!empty($hour))
                                {{ $session->getHour() }} h
                            @endif
                        </span>
                    </div>

                    <div class="td has-text-black isTitle has-text-weight-bold">
                        <a class="sessionLink" href="{{ route('sessions.view', ['session' => $session->getId() ]) }}">
                            {!! $session->getNameAndIsLive() !!}
                        </a>
                        @if ($session->hasNote())
                            <a class="sessionLink" href="{{ route('sessions.view', ['session' => $session->getId(), 'note' => 1 ]) }}">
                                @svg('ico-notas-selected', 'ico-notas is-selected')
                            </a>
                        @endif
                    </div>
                    <div class="td {{ FilterHelper::_isArea() ? 'has-text-' . $color : ''}} isType">
                        @php
                            $a = $session->getAreaName($areas);
                        @endphp
                        @if (!empty($areas))
                            <span class="">{{ $a }}</span>
                        @endif
                    </div>
                    {{-- <div class="td">{{ $session->getAuthors() }}</div> --}}
                    {{-- <div class="td isN">{{ $session->getRef() }}</div> --}}
                    <div class="td isRoom">{{ $session->getRoomName($rooms) }}</div>
                    <div class="td isSchedule">
                        <a class="agendar" href='{{ route('sessions.addToSchedule', ['id' => $session->getId()]) }}' @if ($session->isInSchedule()) style='display: none' @endif>
                            @svg('ico-agendar', 'ico-agendar')
                        </a>
                        <a class="agendada" href='{{ route('sessions.removeFromSchedule', ['id' => $session->getId() ]) }}' @if (!$session->isInSchedule()) style='display: none' @endif>
                            @php
                            /* esto es si se quiere hacer el control de sesiones coincidentes sobre todas, no sobre las que se marcaron en el momento de insertarlas */
                            // $isOverlapping = ($schedule->isSessionInSchedule($session->getId()) && $schedule->isOverlappingSession($session->getId())) ? true : false;
                            // $classAgendada = 'ico-agendada';
                            // $classAgendaConflicto = 'ico-agenda-conflicto';
                            // if ($isOverlapping) {
                            //     $classAgendada .= ' is-hidden';
                            // } else {
                            //     $classAgendaConflicto .= ' is-hidden';
                            // }
                            $isOverlapping = ($schedule->isSessionInSchedule($session->getId()) && $schedule->isSetAsOverlapping($session->getId())) ? true : false;
                            $classAgendada = 'ico-agendada';
                            $classAgendaConflicto = 'ico-agenda-conflicto';
                            if ($isOverlapping) {
                                $classAgendada .= ' is-hidden';
                            } else {
                                $classAgendaConflicto .= ' is-hidden';
                            }
                            @endphp
                            @svg('ico-agendada', $classAgendada)
                            @svg('ico-agenda-conflicto', $classAgendaConflicto)
                        </a>
                    </div>
                </div>
                <div class="tr is-hidden-tablet is-flex is-flex-direction-row @if (!empty($currentUserViews) && $session->hasAnyView($currentUserViews)) visited @endif">
                    <a class="info sessionLink" href="{{ route('sessions.view', ['session' => $session->getId() ]) }}">
                        <div class="td is-flex">
                            <span class="hour">{{ $session->getInitHour() }}</span>
                            <span class="day">{{ DateHelper::formatMini($session->getDate(), Lang::locale()) }}</span>
                            @if (!empty($areas))
                                <span class="is-uppercase {{ FilterHelper::_isArea() ? 'has-text-' . $color : ''}} isType">
                                    {{ $session->getAreaNameAbbr($areas) }}
                                </span>
                            @endif
                            <span>{{ $session->getRoomNameAbbr($rooms) }}</span>
                        </div>
                        <div class="td isTitle has-text-weight-bold">
                            {!! $session->getNameAndIsLive() !!}
                        </div>
                    </a>
                    <div class="schedule">
                        @include('program.partials._schedule-btn')
                    </div>
                </div>
            @endif
        @endforeach
    </div>
</div>
