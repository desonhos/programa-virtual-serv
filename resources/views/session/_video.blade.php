{{-- NOTA: en caso de webapp nunca se incluye esta vista --}}
<script>
    var player;
    var playerPromo;
</script>

{{-- Sesión que no se retransmitirá --}}
@php $viewerVisibility = $session->getViewerVisibility(); @endphp

@if ($viewerVisibility == $session::PV_VIDEO_HIDDEN)
@elseif ($viewerVisibility == $session::PV_WARN_NO_VIDEO)
    @include('session.video.session-without-video')
@else
    {{-- Se ve el visor --}}
    {{-- Si modo congreso virtual --}}
    @if (!\SettingHelper::isApp())
        {{-- Si es una sesión restringida y el usuario actual no tiene acceso --> no se muestra --}}
        @if ($session->isRestrictedForCurrentUser())
            @include('session.video.restricted-session')
        @else
            @include('session.video.player')
        @endif
    @else
        {{-- Se está en modo Webapp --}}
        {{-- Aquí nunca debería llegar --}}
        @include('partials.virtual-platform-ad')
    @endif
@endif
