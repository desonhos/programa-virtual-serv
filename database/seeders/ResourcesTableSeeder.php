<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use TCG\Voyager\Models\DataRow;
use TCG\Voyager\Models\DataType;
use TCG\Voyager\Models\Menu;
use TCG\Voyager\Models\MenuItem;
use TCG\Voyager\Models\Permission;
use TCG\Voyager\Models\Role;
use \App\Models\Sponsor;

class ResourcesTableSeeder extends Seeder {
    private $tableName = 'resources';
    private $dataTypeSlug = 'resources';

    public function run() {
        $dataType = $this->createDataType();
        $this->createDataRows($dataType);
        $this->createAdminMenuItem();
        $this->generatePermissions();
    }

    public function createDataType() {
        $dataType = $this->dataType('slug', $this->dataTypeSlug);
        if (!$dataType->exists) {
            $dataType->fill([
                'name'                  => $this->tableName,
                'display_name_singular' => 'Recurso de cartera',
                'display_name_plural'   => 'Recursos de cartera',
                'icon'                  => 'voyager-wallet',
                'model_name'            => 'App\\Models\\Resource',
                // 'controller'            => 'App\\Http\\Controllers\\Voyager\\VoyagerImageController',
                'generate_permissions'  => 1,
                'details'               => [
                    "order_column" => "pos",
                    "order_display_column" => "name",
                    "order_direction" => "asc",
                    "default_search_key" => "name",
                    "scope" => null
                ]
            ])->save();
        }
        return $dataType;
    }

    public function createDataRows($dataType) {
        $dataRow = $this->dataRow($dataType, 'id');
        if (!$dataRow->exists) {
            $dataRow->fill([
                'type'         => 'number',
                'display_name' => 'ID',
                'required'     => 1,
                'browse'       => 0,
                'read'         => 0,
                'edit'         => 0,
                'add'          => 0,
                'delete'       => 0,
                'order'        => 1,
            ])->save();
        }
        $dataRow = $this->dataRow($dataType, 'file');
        if (!$dataRow->exists) {
            $dataRow->fill([
                'type'         => 'file',
                'display_name' => 'Archivo',
                'required'     => 1,
                'browse'       => 0,
                'read'         => 1,
                'edit'         => 1,
                'add'          => 1,
                'delete'       => 1,
                'order' => 2,
            ])->save();
        }
        $dataRow = $this->dataRow($dataType, 'name');
        if (!$dataRow->exists) {
            $dataRow->fill([
                'type'         => 'text',
                'display_name' => 'Nombre',
                'required'     => 1,
                'browse'       => 1,
                'read'         => 1,
                'edit'         => 1,
                'add'          => 1,
                'delete'       => 1,
                'order'        => 3,
            ])->save();
        }
        $dataRow = $this->dataRow($dataType, 'ico');
        if (!$dataRow->exists) {
            $dataRow->fill([
                'type'         => 'select_dropdown',
                'display_name' => 'Icono',
                'required'     => 1,
                'browse'       => 0,
                'read'         => 1,
                'edit'         => 1,
                'add'          => 1,
                'delete'       => 1,
                'details'      => [
                    'default' => 'PDF-DOCUMENTOS',
                    'options' => [
                        'DESCARGAR' => 'Descargar',
                        'PDF-CERTIFICADO-ASIST' => 'Certificado',
                        'PDF-DOCUMENTOS' => 'Documento',
                        'PDF-LIBRO' => 'Libro',
                        'VIDEO' => 'Vídeo',
                    ],
                ],
                'order' => 4,
            ])->save();
        }
        $dataRow = $this->dataRow($dataType, 'pos');
        if (!$dataRow->exists) {
            $dataRow->fill([
                'type'         => 'number',
                'display_name' => 'Pos',
                'required'     => 0,
                'browse'       => 0,
                'read'         => 0,
                'edit'         => 0,
                'add'          => 0,
                'delete'       => 0,
                'order'        => 5,
            ])->save();
        }
        $dataRow = $this->dataRow($dataType, 'created_at');
        if (!$dataRow->exists) {
            $dataRow->fill([
                'type'         => 'timestamp',
                'display_name' => 'Fecha de creación',
                'required'     => 0,
                'browse'       => 0,
                'read'         => 0,
                'edit'         => 0,
                'add'          => 0,
                'delete'       => 0,
                'order'        => 9,
            ])->save();
        }
        $dataRow = $this->dataRow($dataType, 'updated_at');
        if (!$dataRow->exists) {
            $dataRow->fill([
                'type'         => 'timestamp',
                'display_name' => 'Última modificación',
                'required'     => 0,
                'browse'       => 0,
                'read'         => 0,
                'edit'         => 0,
                'add'          => 0,
                'delete'       => 0,
                'order'        => 10,
            ])->save();
        }
    }

    public function createAdminMenuItem() {
        $menu = Menu::where('name', 'admin')->firstOrFail();
        $menuItem = MenuItem::firstOrNew([
            'menu_id' => $menu->id,
            'title'   => 'Cartera',
            'url'     => '',
            'route'   => 'voyager.'.$this->dataTypeSlug.'.index',
        ]);
        if (!$menuItem->exists) {
            $menuItem->fill([
                'target'     => '_self',
                'icon_class' => 'voyager-wallet',
                'color'      => null,
                'parent_id'  => null,
                'order'      => 5,
            ])->save();
        }
    }

    /**
     * [dataRow description].
     *
     * @param [type] $type  [description]
     * @param [type] $field [description]
     *
     * @return [type] [description]
     */
    protected function dataRow($type, $field) {
        return DataRow::firstOrNew([
            'data_type_id' => $type->id,
            'field'        => $field,
        ]);
    }

    /**
     * [dataType description].
     *
     * @param [type] $field [description]
     * @param [type] $for   [description]
     *
     * @return [type] [description]
     */
    protected function dataType($field, $for) {
        return DataType::firstOrNew([$field => $for]);
    }

    protected function generatePermissions() {
        Permission::generateFor($this->tableName);

        $role = Role::where('name', 'admin')->firstOrFail();

        $permissions = Permission::all();

        $role->permissions()->sync(
            $permissions->pluck('id')->all()
        );
    }

}
