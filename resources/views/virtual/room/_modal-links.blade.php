@php
$grupos = [];
foreach ($mapareas as $maparea) {
    $grupo = getGrupo($maparea);
    if ($grupo !== "") {
        if (!array_key_exists($grupo, $grupos)) { $grupos[$grupo] = array(); }
        array_push($grupos[$grupo], $maparea);
    }
}
@endphp

@foreach ($grupos as $grupo => $mapareas)
    <div class="modal linksModal" data-id="{{ $grupo }}">
        <div class="modal-background"></div>
        <div class="modal-card">
            <header class="modal-card-head">
                <p class="modal-card-title is-uppercase is-flex is-align-items-center mb-0 is-size-5">
                    {{ $vroom->getName() }}
                </p>
                <button class="delete" aria-label="close"></button>
            </header>
            <section class="modal-card-body">
                @foreach ($mapareas as $maparea)
                    <p class="portfolioItem is-flex is-align-items-center is-size-5">
                        @php $class = "exhibitorLink"; @endphp
                        @if (isVideo($maparea))
                            <span class="svgWrapper">@svg('ico-cartera-video', 'ico-cartera-video mr-2')</span>
                            @php $class .= " is-video"; @endphp
                        @else
                            <span class="svgWrapper">@svg('ico-cartera-pdf-documentos', 'ico-cartera-pdf-documentos mr-2')</span>
                            @php $class .= " is-doc"; @endphp
                        @endif
                        <span class="text">{{ getNombre($maparea) }}</span>
                        <a
                            class="ml-auto button {{ $class }}"
                            href="{{ getHref($maparea) }}"
                            target="{{ getTarget($maparea) }}"
                            alt="{{ getNombre($maparea) }}"
                            title="{{ getNombre($maparea) }}"
                            data-areaid="{{ getId($maparea) }}"
                        >Ver</a>
                    </p>
                @endforeach
            </section>
        </div>
    </div>
@endforeach
