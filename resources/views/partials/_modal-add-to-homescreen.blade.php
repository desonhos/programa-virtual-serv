<div class="modal alert" id="addToHomescreenModal">
    <div class="modal-background"></div>
    <div class="modal-card">
        <header class="modal-card-head">
            <p class="modal-card-title is-flex is-align-items-center mb-0 is-size-5">
                {{ __("Añadir a la pantalla de inicio") }}
            </p>
        </header>
        <section class="modal-card-body pb-0">
            <div class="container content">
                <div class="hideInAndroid pb-5">
                    <h2>@svg('ico-apple', 'ico-apple') iOS</h2>
                    {!! \SettingHelper::getInstruccionesInicioIos() !!}
                </div>
                <div class="hideInIos pb-5">
                    <h2>@svg('ico-android', 'ico-android') Android</h2>
                    {!! \SettingHelper::getInstruccionesInicioAndroid() !!}
                </div>
            </div>
        </section>
        <footer class="modal-card-foot is-flex is-justify-content-space-between">
            <a class="close button is-warning is-link letter-spacing-0 has-border-0 pl-0 has-text-white">{{__("Cerrar") }}</a>
        </footer>
    </div>
</div>

<script type="text/javascript">
    function detectBrowser() {

        let userAgent = navigator.userAgent;
        let browserName;

        // if (userAgent.match(/chrome|chromium|crios/i)) {
        // browserName = "Chrome";
        // } else if (userAgent.match(/firefox|fxios/i)) {
        // browserName = "Firefox";
        // } else if (userAgent.match(/safari/i)) {
        // browserName = "Safari";
        // } else if (userAgent.match(/opr\//i)) {
        // browserName = "Opera";
        // } else if (userAgent.match(/edg/i)) {
        // browserName = "Edge";
        // } else 
        if (userAgent.match(/android/i)) {
            browserName = "Android";
        }
        else if (userAgent.match(/iphone/i)) {
            browserName = "iPhone";
        }
        else {
            browserName = "Unknown";
        }
        return browserName;

    }

    $(document).ready(function() {
        var browser = detectBrowser();
        if (browser == "Android") {
            $(".hideInAndroid").hide();
        }
        else if (browser == "iPhone") {
            $(".hideInIos").hide();
        }
    })
</script>