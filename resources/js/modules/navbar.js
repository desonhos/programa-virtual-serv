$(document).ready(function() {
    $(".navbar .navbar-burger.is-mainmenu").click(function() {
        $(".navbar-menu.is-mainmenu").toggleClass('is-active');
        $(this).toggleClass('is-active');
        // si el otro menú está pulsado, lo desmarca
        closeSubmenu(".is-usermenu");
    });
    $(".navbar .navbar-burger.is-usermenu").click(function() {
        $(".navbar-menu.is-usermenu").toggleClass('is-active');
        $(this).toggleClass('is-active');
        // si el otro menú está pulsado, lo desmarca
        closeSubmenu(".is-mainmenu");
    });
});

function closeSubmenu(partialSelector) {
    $(".navbar-menu" + partialSelector).removeClass('is-active');
    $(".navbar .navbar-burger" + partialSelector).removeClass('is-active');
}
