{{-- Funcionalidad de zoom (esto más los estilos en program.scss --}}
<div class="zoom-controls">
    <button class="zoom-button" id="zoom-in">
        <svg style="enable-background:new 0 0 512 512;" version="1.1" viewBox="0 0 512 512" width="512px" xml:space="preserve" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><path d="M417.4,224H288V94.6c0-16.9-14.3-30.6-32-30.6c-17.7,0-32,13.7-32,30.6V224H94.6C77.7,224,64,238.3,64,256  c0,17.7,13.7,32,30.6,32H224v129.4c0,16.9,14.3,30.6,32,30.6c17.7,0,32-13.7,32-30.6V288h129.4c16.9,0,30.6-14.3,30.6-32  C448,238.3,434.3,224,417.4,224z"/></svg>
    </button>
    <button class="zoom-button" id="zoom-out">
        <svg style="enable-background:new 0 0 512 512;" version="1.1" viewBox="0 0 512 512" width="512px" xml:space="preserve" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><path d="M417.4,224H94.6C77.7,224,64,238.3,64,256c0,17.7,13.7,32,30.6,32h322.8c16.9,0,30.6-14.3,30.6-32  C448,238.3,434.3,224,417.4,224z"/></svg>
    </button>
</div>

<script>
    document.addEventListener('DOMContentLoaded', function() {
        let scale = 1;
        let elementToScale = document.querySelector("#programZoomableArea");

        document.getElementById('zoom-in').addEventListener('click', function() {
            scale += 0.1;
            elementToScale.style.zoom = `${scale*100}%`;
            addOverflowHiddenIfZoomed(scale);
        });

        document.getElementById('zoom-out').addEventListener('click', function() {
            scale = Math.max(1, scale - 0.1);
            elementToScale.style.zoom = `${scale*100}%`;
            addOverflowHiddenIfZoomed(scale);
        });
    });
    function addOverflowHiddenIfZoomed(scale) {
        if (scale == 1) {
            document.querySelector("body").classList.remove("is-clipped");
        }
        else {
            document.querySelector("body").classList.add("is-clipped");
        }
    }
</script>