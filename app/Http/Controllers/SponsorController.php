<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Sponsor;

class SponsorController extends Controller {

    public function index() {
        $sponsors = Sponsor::_getAll();
        return view('sponsors', compact('sponsors'));
    }

}
