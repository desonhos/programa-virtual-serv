@extends('layouts/virtual')

@section('page-class', 'page-virtual is-login')

@section('js')
@endsection

@section('content')
    <?php /*
    <section id="welcome-section" class="section">
        <div class="container">
            <div class="is-flex is-justify-content-space-between is-align-items-center is-flex-wrap-wrap">
                {{-- <h1 class="has-text-48 has-text-weight-bold has-text-white">{{ __('Bienvenidos') }}</h1> --}}
            </div>
        </div>
    </section>
    */ ?>
    <img src="{{ asset('event/login-fondo.jpg') }}">
    @if (!\DateHelper::isDatetimeInPast(\SettingHelper::getClosingDate()))
        <section id="login-section" class="section">
            <div class="container">
                <div id="login-description">
                    <p class="is-color-titulos has-text-weight-bold has-text-18-all">
                        {!! \SettingHelper::getWelcomeTitle() !!}
                    </p>
                    {!! \SettingHelper::getWelcomeText() !!}
                </div>
                <div id="login-form">
                    <p class="is-color-titulos has-text-weight-bold pb-4 has-text-18-all">
                        {!! \SettingHelper::getLoginTitle() !!}
                    </p>
                    @yield('form')
                </div>
            </div>
        </section>
   @else
       <section class="section">
           <div class="container">
               <p class="is-color-titulos has-text-weight-bold has-text-18-all">
                   {!! \SettingHelper::getWelcomeTitleAfterClosing() !!}
               </p>
               <p>{!! \SettingHelper::getWelcomeTextAfterClosing() !!}</p>
           </div>
       </section>
   @endif

@endsection

<style>
    html.has-navbar-fixed-top { padding-top: 0!important; }
</style>
