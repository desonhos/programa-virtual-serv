@php
    $colors = FilterHelper::_getColorlist($sessiontypes, $areas, true);
    array_push($colors, '')
@endphp

@foreach ($colors as $color)
    @include('partials._dynamic-styles-color', ['color' => $color])
@endforeach

<style>
    @if (\SettingHelper::getSchemaBackgroundColor())
        .skeduler-main-timeline div, .skeduler-main-body > div > div.skeduler-cell {
            background-color: {{ \SettingHelper::getSchemaBackgroundColor() }};
        }
    @endif
    @if (\SettingHelper::getSchemaSessionBackgroundColor())
        .page-program .sessions-scheme .info {
            background-color: {{ \SettingHelper::getSchemaSessionBackgroundColor() }};
        }
    @endif
    @if (\SettingHelper::getSchemaHeaderBackgroundColor())
        .page-program .sessions-scheme .skeduler-headers .scroller > div {
            background-color: {{ \SettingHelper::getSchemaHeaderBackgroundColor() }};
        }
    @endif
    @if (\SettingHelper::getSchemaHeaderFontColor())
        .page-program .sessions-scheme .skeduler-headers .scroller > div {
            color: {{ \SettingHelper::getSchemaHeaderFontColor() }};
        }
    @endif
    @if (\SettingHelper::getSchemaDefaultFontColor())
        .page-program .sessions-scheme .skeduler-headers .scroller > div {
            color: {{ \SettingHelper::getSchemaDefaultFontColor() }};
        }
    @endif
</style>
