<section class="section pt-5 pb-0 is-header">
    <div class="container">
        <p class='subtitle'>{{ FilterHelper::_isArea() ? $session->getAreaName() : $session->getSessiontypeName()}}</p>
        <h1 class="title">{!! $session->getNameAndIsLive() !!}</h1>
    </div>
</section>
