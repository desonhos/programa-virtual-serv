@php
$anuncio = \App\Models\Anuncio::latest()->first();
@endphp

@if ($anuncio && $anuncio->hasImageOrVideo())
    @php
    $video = $anuncio->getVideoUrl();
    $videoV = $anuncio->getVideoVerticalUrl();
    $img = $anuncio->getImagenUrl();
    $imgV = $anuncio->getImagenVerticalUrl();
    @endphp

    @php $tMinCierre = $anuncio->getTMinCierre(); @endphp

    <div class="modal alert" id="anuncioModal">
        <div class="modal-background"></div>
        <div class="modal-card">
            <section class="modal-card-body">
                {{-- <div class="container content"> --}}
                    @if ($video)
                        <video autoplay muted playsinline @if ($videoV) class="hide-portrait" @endif>
                            <source src="{{ $video }}" type="video/mp4">
                        </video>
                        @if ($videoV)
                            <video autoplay muted playsinline @if ($video) class="hide-landscape" @endif>
                                <source src="{{ $videoV }}" type="video/mp4">
                            </video>
                        @endif
                    @else
                        @if ($img)
                            <img src="{{ $img }}" @if ($imgV) class="hide-portrait" @endif>
                        @endif
                        @if ($imgV)
                            <img src="{{ $imgV }}" @if ($img) class="hide-landscape" @endif>
                        @endif
                    @endif
                {{-- </div> --}}
            </section>
            <footer class="modal-card-foot is-flex is-justify-content-flex-end">
                <a class="close button is-warning is-link letter-spacing-0 has-border-0 has-text-white is-hidden">Cerrar</a>
                @if ($tMinCierre)
                <p class="button is-warning is-link letter-spacing-0 has-border-0 has-text-white" id="avisoTiempoRestante">
                    {{__("Podrá cerrar esta ventana en")}}&nbsp;<span id="tRestante">{{$tMinCierre}}</span>&nbsp;s
                </p>
                @endif
            </footer>
        </div>
    </div>
    <script type="text/javascript">
        var selAnuncioModal = "#anuncioModal";

        $(document).ready(function(){
            /*
            - isAlreadyShowed --> para solo mostrarlo una vez por sesión
            - (history.length == 1) --> para mostrar el anuncio cuando se haya abierto la webapp en una pestaña nueva (aunque ya se hubiera mostrado previamente en esta sesión)
            -   como en Safari de iphone esto no se cumple (ya empieza en 2), compruebo si el referrer no contiene programa.congresoaev.net, en ese caso se tiene que mostrar el anuncio
            - document.addEventListener("visibilitychange" --> para mostrar el anuncio cuando se haya ido a otra pestaña y vuelto a la de la webapp
            */
            var isAlreadyShowed = "{{ $anuncio->isAlreadyShowed() }}";
            if (!isAlreadyShowed || (history.length < 2) || referrerNotContainsBaseUrl()) {
                console.log("primera vez o pestaña nueva");
                showAnuncio();
                closeAnuncioHandler();
            }
        });

        /** Cuando se alterna entre pestañas, el anuncio se tiene que mostrar otra vez */
        document.addEventListener("visibilitychange", () => {
            // it could be either hidden or visible
            // document.title = document.visibilityState;
            if (document.visibilityState == "visible") {
                console.log("alterna entre pestañas");
                showAnuncio();
            }
        });

        function referrerNotContainsBaseUrl() {
            var referrer = document.referrer;
            var baseUrl = "{{config('app.url')}}";
            return !referrer.includes(baseUrl);
            
        }
        function closeAnuncio() {
            $(selAnuncioModal + " .close").closest(".modal").removeClass("is-active");
        }
        function closeAnuncioHandler() {
            $(selAnuncioModal + " .close").click(closeAnuncio);
        }
        function showAnuncio() {
            // Desde el panel definen un porcentaje que determina con qué probabilidad se va a mostrar el anuncio
            if (!probability({{$anuncio->getProbabilidadMostrar()}})) {
                console.log("no se mostrará el anuncio");
                return;
            }

            $(selAnuncioModal).addClass("is-active");

            // Cierre automático
            @if ($tCierreAuto = $anuncio->getTCierreAuto())
                setTimeout( function() { closeAnuncio(); }, parseInt({{ $tCierreAuto }})*1000);
            @endif

            // Se podrá cerrar tras x tiempo
            @if ($tMinCierre)
                var s = {{$tMinCierre}};
                var intervalId = setInterval(function() {
                    s--;
                    if (s <= 0) {
                        clearInterval(intervalId);
                        $("#avisoTiempoRestante").hide();
                        $(selAnuncioModal + " .close.button").removeClass("is-hidden");
                    }else {
                        $("#tRestante").html(s);
                    }
                }, 1000);

            @endif
        }
        /** Devuelve verdadero un porcentaje de veces que viene dado por el parámetro n (número entre 0 y 100)*/
        function probability(n) {
            console.log(n);
            var n = n/100;
            return !!n && Math.random() <= n;
        }
    </script>
@endif