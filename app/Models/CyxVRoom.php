<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/** Clase que se utiliza para guardar las estadísticas de visitas a los
 * diferentes stands por parte de los usuarios
 */
class CyxVRoom extends Model {

    protected $connection= 'cyex_online';
    protected $table = 'pv_vroomviews';
    public $timestamps = false;
    protected static $_table = 'pv_vroomviews';

    /** Obtiene todas las del evento actual */
    public static function _getAll() {
        $rooms = CyxVRoom::where('event_id', config('web.eventId'))
                    ->orderBy('room_name')
                    ->get();
        return $rooms;
    }

    /** Obtiene por su id, del evento definido en config */
    public static function _get($userIdInEvent, $vroom) {
        return CyxVRoom::where('vroom_id', $vroom->getId())
                    ->where('event_id', config('web.eventId'))
                    ->where('user_id_in_event', $userIdInEvent)
                    ->where('day', date("Y-m-d"))
                    ->first();
    }

    /** Busca en el listado, aquel cuyo id se pasa por parámetro */
    public static function _find($id, $rooms = null) {
        if (empty($rooms)) {
            return CyxVRoom::_get($id);
        }
        foreach ($rooms as $room) {
            if ($room->id == $id) {
                return $room;
            }
        }
        return null;
    }

    public static function _userVisitsRoom($userIdInEvent, $vroom) {
        $vroom_view = CyxVRoom::_get($userIdInEvent, $vroom);
        if (!$vroom_view) {
            CyxVRoom::_insert($userIdInEvent, $vroom);
            $vroom_view = CyxVRoom::_get($userIdInEvent, $vroom);
        }
        $vroom_view->setNViewsAndRoomName($vroom_view->getNviews() + 1, $vroom);
    }

    public static function _insert($userIdInEvent, $vroom) {
        \DB::connection('cyex_online')->table(CyxVRoom::$_table)->insert([
            'user_id_in_event' => $userIdInEvent,
            'event_id' => config('web.eventId'),
            'vroom_id' => $vroom->getId(),
            'vroom_name' => $vroom->getName(),
            'nviews' => 1,
            'day' => date("Y-m-d")
        ]);
    }

    public function getId() { return $this->id; }
    public function getEventId() { return $this->event_id; }
    public function getName() { return $this->room_name; }
    public function getNviews() { return $this->nviews; }
    public function getRoomId() { return $this->vroom_id; }
    public function getUserIdInEvent() { return $this->user_id_in_event; }
    public function setNViewsAndRoomName($nViews, $vroom) {
        \DB::connection('cyex_online')->table($this->table)
              ->where('user_id_in_event', $this->getUserIdInEvent())
              ->where('event_id', $this->getEventId())
              ->where('vroom_id', $this->getRoomId())
              ->where('day', date("Y-m-d"))
              ->update(['nviews' => $nViews, 'vroom_name' => $vroom->getName() ]);
    }

}
