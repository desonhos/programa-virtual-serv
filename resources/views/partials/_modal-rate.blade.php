{{-- Variables que tienen que llegar:
- $paper
 --}}

<div class="modal is-rate" id="rateModal_{{$paper->getId()}}">
    <div class="modal-background"></div>
    <div class="modal-card">
        <form method="POST" action="{{ route('papers.rate') }}" class="ratePoster" id="ratePoster-{{ $paper->getId() }}">
            @csrf
            <section class="modal-card-body">
                <p class="messageBeforeSend">{{ __("¿Qué puntuación te gustaría darle a este póster?") }}</p>
                <p class="messageAfterSend is-hidden">{{ __("Gracias por su puntuación") }}</p>
                <input type="hidden" name="paper_id" value="{{ $paper->getId() }}">
                <input type="hidden" name="session_id" value="{{ $paper->getSessionid() }}">
                <fieldset class="star-cb">
                    {{-- https://codepen.io/lsirivong/pen/nRNLYL --}}
                    <span class="star-cb-group">
                        @for ($i = config('web.ratings.maxScore'); $i > 0; $i--)
                            <input type="radio" id="rating-{{$i}}-{{ $paper->getId() }}" name="rating" value="{{$i}}" />
                            <label for="rating-{{$i}}-{{ $paper->getId() }}">{{$i}}</label>
                        @endfor
                    </span>
                </fieldset>
                <p class="is-hidden has-text-danger noSelectionError">{{ __("Seleccione primero su valoración") }}</p>
            </section>
            <footer class="modal-card-foot is-flex is-justify-content-space-between">
                <a class="close button is-warning is-link letter-spacing-0 has-border-0 pl-0">{{ __("Cancelar") }}</a>
                <input class="button is-secondary is-outline" type="submit" value="{{ __("Valorar póster") }}">
            </footer>
        </form>
    </div>
</div>
