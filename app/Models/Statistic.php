<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Statistic extends Model {

    protected $connection= 'cyex_online';
    protected $table = 'pv_sessionstatistics';
    public $timestamps = false;
    protected static $_table = 'pv_sessionstatistics';

    /** Obtiene todos los del evento actual */
    public static function _getAll() {
        return Statistic::where('event_id', config('web.eventId'))->get();
    }

    /** Obtiene por id de usuario e id de sesión, del evento definido en config */
    public static function _get($userIdInEvent, $sessionId) {
        return Statistic::where('user_id_in_event', $userIdInEvent)
                    ->where('event_id', config('web.eventId'))
                    ->where('session_id', $sessionId)
                    ->first();
    }

    /** Contabiliza un acceso a una sesión por parte de un usuario */
    public static function _addAccess($userIdInEvent, $sessionId) {
        $statistic = Statistic::_get($userIdInEvent, $sessionId);
        if (!$statistic) {
            Statistic::_insert($userIdInEvent, $sessionId);
            $statistic = Statistic::_get($userIdInEvent, $sessionId);
        }
        $statistic->setAccesses($statistic->getAccesses() + 1);
    }

    public static function _insert($userIdInEvent, $sessionId) {
        \DB::connection('cyex_online')->table(Statistic::$_table)->insert([
            'user_id_in_event' => $userIdInEvent,
            'event_id' => config('web.eventId'),
            'session_id' => $sessionId,
            'naccesses' => 0,
            'naccesses_during_event' => 0,
        ]);
    }

    /************************** GETTERS ***************************************/
    public function getAccesses() { return $this->naccesses; }
    public function getAccessesDuringEvent() { return $this->naccesses_during_event; }
    public function getUserIdInEvent() { return $this->user_id_in_event; }
    public function getEventId() { return $this->event_id; }
    public function getSessionId() { return $this->session_id; }

    /************************** SETTERS ***************************************/
    // TODO: No he podido hacerlas con Eloquent porque no admite claves primarias compuestas
    public function setAccesses($accesses) {
        \DB::connection('cyex_online')->table($this->table)
              ->where('user_id_in_event', $this->getUserIdInEvent())
              ->where('event_id', $this->getEventId())
              ->where('session_id', $this->getSessionId())
              ->update(['naccesses' => $accesses]);
    }
}
