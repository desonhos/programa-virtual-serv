<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use App\Models\PVUser;
use App\Mail\RememberPass;

class UserController extends Controller {

    /** Muestra el login del congreso virtual */
    public function showLogin() {
        return (!\Auth::guard('multi')->user() && !\SettingHelper::isAntesInicioVirtual())
            ? view('login')
            : redirect()->route('inicio-webapp');
    }

    /** Procesa los datos de acceso introducidos en la página de login */
    public function virtualEventLogin(Request $request) {
        $errorMsg = 'Correo electrónico o contraseña incorrectos';
        $user = PVUser::where('email', $request->email)
                    ->where('event_id', config('web.eventId'))
                    ->first();
        if (empty($user)) {
            return back()->withErrors([$errorMsg]);
        }
        // si ese usuario no ha hecho nunca antes login
        if (!empty($user->getDefaultPassword())) {
            // comprueba si la contraseña introducida es la que puso CYEX por defecto
            if ($user->getDefaultPassword() == $request->password) {
                // en ese caso, quita la contraseña por defecto y la guarda cifrada
                $user->setDefaultPassword(null);
                $user->setPassword($request->password);
                $user->save();
            }
            else {
                // no ha entrado nunca antes pero metió mal la contraseña --> error
                return back()->withErrors([$errorMsg]);
            }
        }
        else {
            // ya había hecho login alguna vez antes. Comprueba en la contraseña cifrada
            if (!Hash::check($request->password, $user->getPassword())) {
                return back()->withErrors([$errorMsg]);
            }
        }
        // si llega hasta aquí es que la contraseña es correcta y el usuario existe
        \Auth::guard('multi')->login($user);
        //cierra las otras sesiones que tenga abiertas (https://codezen.io/how-to-manage-logged-in-devices-in-laravel/#Track_User_Logins_in_Database)
        $this->logoutAllDevices();
        return redirect()->intended(route('inicio-webapp'));
    }

    /** Informa de que la sesión se ha abierto desde otro dispositivo */
    public function informAnotherSessionOpened() {
        return view('user.inform-another-session-opened');
    }

    /** Actualiza los datos del perfil */
    public function updateProfile(Request $request) {
        $user = \Auth::guard('multi')->user();

        // TODO Natalia: hacer esto con el validator de Laravel: https://laravel.com/docs/8.x/validation

        // comprueba la contraseña actual
        if (!Hash::check($request->password, $user->getPassword())) {
            return back()->withErrors(["Contraseña actual incorrecta"]);
        }
        // comprueba la contraseña nueva: ambas son iguales
        if (($request->password_new == "") || ($request->password_new2 == "")) {
            return back()->withInput()->withErrors(["Faltan campos obligatorios"]);
        }
        // comprueba la contraseña nueva: cumple los criterios de longitud y caracteres especiales
        if (strlen($request->password_new) < 8) {
            return back()->withInput()->withErrors(["La nueva contraseña no tiene una longitud mínima de 8 caracteres"]);
        }
        $user->setPassword($request->password_new);
        if (!$user->save()) {
            return back()->withInput()->withErrors(["Error al guardar"]);
        }
        else {
            return back()->with('success', 'Datos guardados');
        }
    }

    /** Muestra la página en la que se muestra el formulario de recordar contraseña */
    public function showRememberPass() {
        if (!\Auth::guard('multi')->check()) {
            return view('user.remember-pass');
        }
        else {
            return redirect()->route('inicio-webapp');
        }
    }

    /** Establece una contraseña provisional y la envía por correo */
    public function rememberPass(Request $request) {
        $request->validate(['email' => 'required|email']);

        $user = PVUser::_findByEmail($request->email);
        if (!$user) {
            return back()->withErrors(["No existe ninguna cuenta con esa dirección de correo"]);
        }
        $user->setDefaultPassword(Str::random(8));
        $user->save();

        \Mail::to($request->email)->send(new RememberPass($user->getDefaultPassword()));

        return back()->with('success', 'Se ha enviado un correo electrónico a ' . $request->email);
    }

    /** Cierra la sesión actual */
    public function logout() {
        \Auth::guard('multi')->logout();
        return redirect()->route('inicio-webapp');
    }

    /**
     * Logouts a user from all other devices except the current one.
     * @link https://codezen.io/how-to-manage-logged-in-devices-in-laravel/#Track_User_Logins_in_Database
     *
     * @return boolval
     */
    private function logoutAllDevices() {
        $user = \Auth::guard('multi')->user();
        if (empty($user)) { return false; }

        \DB::connection('cyex_online')
            ->table('pv_sessions')
            ->where('user_id', $user->getId())
            ->where('id', '!=', \Session::getId())
            ->delete();

        return true;
    }

}
