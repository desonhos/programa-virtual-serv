@if ($session->hasSurveyToShow())

    @php
        $surveyType = $session->getSurveyType();
    @endphp

    @if ($surveyType == $session::SURVEY_MONKEY)
        <a class="button is-info" id="button-survey">
            {{__("Evaluar sesión")}}
        </a>

        <div class="modal" id="modal-survey">
            <div class="modal-background"></div>
            <div class="modal-content">
                {!! $session->getSurveyCode() !!}
            </div>
            <button class="modal-close is-large" aria-label="close"></button>
        </div>
    @elseif ($surveyType == $session::CYEX)
        @php
        $surveyLink = $session->getCyexSurveyLink();
        @endphp
        @if (!empty($surveyLink))
            <a class="button is-info" href="{!! $surveyLink !!}" target="_blank">
                {{__("Evaluar sesión")}}
            </a>
        @endif
    @endif

@endif
