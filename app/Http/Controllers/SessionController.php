<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Session;
use App\Models\Area;
use App\Models\Sessiontype;
use App\Models\View;
use App\Models\Statistic;

class SessionController extends Controller {
    //
    public function view($sessionId) {
        $session = Session::_get($sessionId);
        if (empty($session)) { abort(404); }

        $papers = ($session) ? $session->getPapers() : null;
        $relatedSessions = ($session) ? $session->getRelatedSessions() : null;
        $areas = Area::_getAll();
        $sessiontypes = Sessiontype::_getAll();

        // guarda acceso del usuario a esta sesión
        if (!empty($user)) {
            Statistic::_addAccess($user->getIdInEvent(), $sessionId);
        }

        return view('session', compact('session', 'papers', 'relatedSessions', 'areas', 'sessiontypes'));
    }

    public function viewVirtual($sessionId) {
        return $this->view($sessionId);
    }

    /** Llamada a través de AJAX */
    public function storeViewing($session_id, Request $request) {
        $session = Session::_get($session_id);
        $user = \Auth::guard('multi')->user();
        if (empty($user)) { echo "false"; return; }
        $userIdInEvent = $user->getIdInEvent();
        $sessionId = $session->getId();
        $seconds = $request->seconds;
        $lastSecondPlayed = $request->lastSecondPlayed;
        $view = View::_addSeconds($userIdInEvent, $sessionId, $seconds, $lastSecondPlayed);

        echo "true";
    }

    /** Almacena el porcentaje visto de cada sesión por cada usuario en función
    * de los segundos de visión almacenados y las horas de inicio y fin de la sesión */
    public function calculateNViews() {
        $views = View::_getAll();
        $sessions = Session::_getAll(true);
        foreach ($views as $view) {
            $session = Session::_getFromList($view->session_id, $sessions);
            if (!$session) { continue; }
            $sessionDuration = $session->getDuration();
            $view->setNViews($sessionDuration);
        }
        echo "Calculados los porcentajes de visionado";
    }

    /** Llamada por AJAX, añade a la agenda la sesión cuyo ID se pasa por parámetro */
    public function addToSchedule($sessionId, Request $request) {
        $schedule = new \App\Models\Schedule();
        if (empty($request->ignoreOverlap)) {
            // aviso de que la sesión se solapa con otra ya añadida
            if ($schedule->isOverlappingSession($sessionId)) { echo "2"; return; }
            $schedule->addSession($sessionId);
        } else {
            $schedule->addSession($sessionId, true);
        }
        echo "1";
    }

    /** Llamada por AJAX, quita de la agenda la sesión cuyo ID se pasa por parámetro */
    public function removeFromSchedule($sessionId) {
        $schedule = new \App\Models\Schedule();
        $schedule->removeSession($sessionId);
        echo "true";
    }

    /** Llamada por AJAX, evalúa las sesiones añadidas a la agenda para
    * determinar cuáles son las que se solapan y cuáles no */
    public function reevaluateOverlappingSessions() {
        $schedule = new \App\Models\Schedule();
        $schedule->reevaluateOverlappingSessions();
        echo "true";
    }

    /** Llamada por AJAX, obtiene la nota que se haya hecho previamente sobre esta sesión */
    public function getNote($sessionId, Request $request) {
        $notes = new \App\Models\Notebook();
        echo $notes->getSessionNote($sessionId);
    }

    /** Llamada por AJAX */
    public function modifyNote($sessionId, Request $request) {
        $notes = new \App\Models\Notebook();
        $notes->modifySessionNote($sessionId, $request->noteText);
        echo "true";
    }

    /** Llamada por AJAX */
    public function getCalendarUrls($sessionId, Request $request) {
        $session = Session::_getAnonymously($sessionId);
        if (empty($session)) {
            echo "false";
            return;
        }
        $res = [
            'google-calendar' => $session->getGoogleCalendarLink(),
            'outlook-calendar' => $session->getOutlookCalendarLink(),
            'apple-calendar' => $session->getAppleCalendarLink()
        ];
        echo json_encode($res);
    }
}
