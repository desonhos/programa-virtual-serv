// import Alert from '@vizuaalog/bulmajs/src/plugins/alert';

$(document).ready(function() {
    setPortfolioHandlers();
});

function setPortfolioHandlers(getNoteUrl) {
    var modalId = "portfolioModal";
    $(".portfolioBtn").click(function(ev) {
        // TODO: obtiene los elementos a mostrar en la cartera (certificado de asistencia, cosas que haya ido añadiendo a su perfil…)
        // ev.preventDefault();
        // var $link = $(this);
        // var url = $link.attr('href');
        // var getNoteUrl = $(this).data('get-note');
        // var $noteTextToUpdate = $(this).parent().find('.noteTextToUpdate');
        // $.ajax({
        //     type: "GET",
        //     url: getNoteUrl,
        //     complete: function(data) {
        //         $("#notesModal textarea[name=noteText]").val(data.responseText);
        //         $("#notesModal .submit").unbind('click').click(function() {
        //             saveNote($("#notesModal textarea[name=noteText]").val(), url);
        //             if (typeof($noteTextToUpdate) !== "undefined") {
        //                 $noteTextToUpdate.first().html($("#notesModal textarea[name=noteText]").val());
        //             }
        //         });
                if ($(".navbar-burger.is-usermenu.is-active").length) {
                    $(".navbar-burger.is-usermenu.is-active").click();
                }
                showPortfolioModal(modalId);
        //     }
        // });
    });
    $("#" + modalId + " .modal-background, button.delete, .cancel").click(function() {
        $(this).closest('.modal').removeClass('is-active');
    });
    $("#" + modalId + " .deleteNote").click(function() {
        $("#notesModal textarea[name=noteText]").val('');
        $("#notesModal .submit").click();
    })
}

/** Hace visible el cuadro de diálogo con el contenido de la cartera */
function showPortfolioModal(modalId) {
    $("#" + modalId).addClass('is-active');
    if ($(".is-mainmenu.is-active").length) {
        $(".is-mainmenu.is-active").click();
    }
}
