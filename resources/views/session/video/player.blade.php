@php
$promoVideo = $session->getPromoVideoVimeoUrl();
$sessionVimeoUrl = $session->getVimeoUrl();
$sessionPrevVimeoUrl = $session->getPrevVimeoUrl();
$sessionInitialVimeoUrl = (!empty($sessionPrevVimeoUrl)) ? $sessionPrevVimeoUrl : $sessionVimeoUrl;
@endphp

@if (!empty($promoVideo))
    @include('partials._promo-video', ['showOnEnd' => '#playerNoPromo', 'video' => $promoVideo])
@endif

<div id="playerNoPromo" @if (!empty($promoVideo)) class="is-hidden" @endif>
@if ($sessionInitialVimeoUrl)
    @php
    $papersVimeoUrls = null;
    $hasPapersVideos = false;
    $secondsRemainingToStart = $session->getSecondsRemainingToStart();
    $timeRemainingToStart = \DateHelper::seconds2human($secondsRemainingToStart);
    $secondsRemainingToShowVideo = $session->getSecondsRemainingToShowVideo();
    $videoReadyToShow = $session->isVideoReadyToShow();
    $iframe = '<iframe src="' . $sessionInitialVimeoUrl . '" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen style="position:absolute;top:0;left:0;width:100%;height:100%;"></iframe>';
    $noVideoTextColor = $session->getNoVideoTextColor();
    @endphp
    {{-- Si aún no es la hora de mostrar el vídeo --}}
    @if (!$videoReadyToShow)
        <div class="is-relative" id="prevVideo">
            @php
            $countdownImg = setting('web.countdown_img');
            $countdownImg = (!empty($countdownImg)) ? \VoyagerHelper::getImageUrl($countdownImg) : config('web.event.defaultImg');
            @endphp
            <img src="{{ $countdownImg }}">
            <div class="is-overlay is-flex is-justify-content-center is-align-items-center" @if (!empty($noVideoTextColor)) style="color:{{ $noVideoTextColor }}" @endif>
                <div class="has-text-24">
                    <p class="is-uppercase has-text-centered" style="color: white;">Comenzamos en:</p>
                    <div id="timeRemainingToStart"></div>
                </div>
            </div>
        </div>
        @include('session.video.manage-player-visibility-js')
        <div id="sessionVideoContainer" style="padding:56.25% 0 0 0;position:relative; display: none;"></div>
    @else
        <div id="sessionVideoContainer" style="padding:56.25% 0 0 0;position:relative;">
            <iframe src="{{ $sessionInitialVimeoUrl }}" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen style="position:absolute;top:0;left:0;width:100%;height:100%;"></iframe>
        </div>
    @endif
    @include('session.video.player-events-js')
@endif
</div>
