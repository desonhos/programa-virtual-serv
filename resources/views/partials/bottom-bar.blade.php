<div id="virtualBottomNavbar">
    <div class="fondo"></div>
    <div class="suelo"></div>
    <div class="navbar">
        @php $pdfProgram = \VoyagerHelper::getFileUrl(setting('web.program_pdf')); @endphp

        <a class="navbar-item noDynamicStyles" href="/">
            @svg('ico-vestibulo', 'ico-vestibulo')
            <span class="navbar-item-text">{{ __("Home") }}</span>
        </a>
        <a class="navbar-item noDynamicStyles" href="{{ route('program.view') }}">
            @svg('ico-menu-programa')
            <span class="navbar-item-text">{{ __("Programa") }}</span>
        </a>
        <a class="navbar-item noDynamicStyles" href="{{ route('agenda') }}">
            @svg('ico-agenda', 'ico-agenda')
            <span class="navbar-item-text">{{ __("Mi agenda") }}</span>
        </a>
        <a class="navbar-item noDynamicStyles" href="{{ route('webapp.info') }}">
            @svg('ico-info')
            <span class="navbar-item-text">{{ __("Info") }}</span>
        </a>

        {{-- @if ($pdfProgram)
            <a class="navbar-item noDynamicStyles" href="{{ $pdfProgram }}">
                @svg('ico-programa-pdf', 'ico-programa-pdf')
                <span class="navbar-item-text">Programa en PDF</span>
            </a>
        @endif --}}
    </div>
</div>