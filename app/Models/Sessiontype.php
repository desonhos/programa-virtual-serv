<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Sessiontype extends Model {

    protected $connection= 'cyex_online';
    protected $table = 'programs';

    /** Obtiene todos los del evento actual */
    public static function _getAll() {
        $sessiontypes = Sessiontype::where('event_id', config('web.eventId'))
                            ->orderBy('name')
                            ->get();
        return $sessiontypes;
    }

    /** Busca en el listado de tipos de sesión, aquel cuyo id se pasa por parámetro */
    public static function _find($id, $sessiontypes) {
        foreach ($sessiontypes as $sessiontype) {
            if ($sessiontype->id == $id) {
                return $sessiontype;
            }
        }
        return null;
    }

    /** Obtiene el listado de estilos únicos */
    public static function _getColorlist($sessiontypes, $removeHashtag = false) {
        $colors = [];
        foreach ($sessiontypes as $sessiontype) {
            $color = $sessiontype->getColor($removeHashtag);
            if (!empty($color) && !in_array($color, $colors)) {
                array_push($colors, $color);
            }
        }
        return $colors;
    }

    public function getName() {
        return $this->name;
    }

    public function getNameAbbr() {
        $field = 'name_abbr';
        return !empty($this->{$field}) ? $this->{$field} : $this->getName();
    }

    public function getCss() {
        return $this->css;
    }

    public function getColor($removeHashtag = false) {
        $field = "main_color";
        if (!$removeHashtag) {
            return $this->{$field};
        }
        else {
            return str_replace("#", "", $this->{$field});
        }
    }

    public function getId() {
        return $this->id;
    }
}
