@php
$menu = 'app';
if (\SettingHelper::isAppPV()) { $menu = 'apppv'; }
if (\SettingHelper::isPV()) { $menu = 'pv'; }
@endphp
{{ menu($menu, 'partials._navbar-menuitems') }}