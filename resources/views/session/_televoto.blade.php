@if (!empty($televoto))
    <a class="button is-info" id="button-televoto">
        {{__("Votación")}}
    </a>

    <div class="modal" id="modal-televoto">
        <div class="modal-background"></div>
        <div class="modal-content">
            {!! $televoto !!}
        </div>
        <button class="modal-close is-large" aria-label="close"></button>
    </div>
@endif
