<nav id="virtualNavbar" class="navbar is-fixed-top container is-fluid @if (config('web.topbarDarkLogin')) is-tbdark @endif" role="navigation" aria-label="main navigation">
    <a href="javascript:history.go(-1)" class="ico-back-link">
        @svg('ico-back', 'ico-back')
    </a>
    <a class="logos" href="{{ route('inicio-webapp') }}">
        @svg('topbar-logo-congreso', 'logo-congreso')
        <span class="has-text-weight-bold" id="webTitle">
            {{ \SettingHelper::getNombreCategoriaPlataforma() }}
        </span>
        @svg('topbar-logo-sociedades', 'logo-header-sociedades')
        {{-- <img class="logo-header-sociedades" src="/event/logo-sociedad.png"> --}}
    </a>
    @if (Route::current()->getName() != "login")
        <a role="button" class="navbar-burger is-usermenu burger ml-auto" aria-label="menu" aria-expanded="false" data-target="navbarBasicExample">
            @svg('ico-usuario', 'ico-usuario')
        </a>
        <div class="navbar-menu is-usermenu">
            @include('partials._navbar-usermenuitems')
        </div>
        <a role="button" class="navbar-burger is-mainmenu burger" aria-label="menu" aria-expanded="false" data-target="navbarBasicExample">
            <span aria-hidden="true"></span>
            <span aria-hidden="true"></span>
            <span aria-hidden="true"></span>
        </a>
        <div class="navbar-menu is-mainmenu">
            {{ menu(\SettingHelper::getModoCongreso(), 'partials._navbar-menuitems') }}
        </div>
    @endif
</nav>
