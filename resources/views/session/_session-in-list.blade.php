<article class="paper">
    <a class="sessionLink" href="{{ route('sessions.view', ['session' => $session->getId() ]) }}">
        @php
            $thumb = $session->getVimeoThumb();
            if (!$thumb) {
                $thumb = setting('web.defaultvideo_img');
                $thumb = (!empty($thumb)) ? \VoyagerHelper::getImageUrl($thumb) : config('web.event.defaultImg');
            }
        @endphp
        <img src="{{ $thumb }}" style="width: 100%">
        <span class="paperHour has-text-14-mobile is-block">{{ $session->getDateAndHour() }}</span>
        <h3 class="paperTitle">{!! $session->getName() !!}</h3>
    </a>
</article>
