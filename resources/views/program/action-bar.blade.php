<section class="section pt-0 pb-0" id="actionBar">
    <div id="action-bar" class="container__ is-transparent">
        @include('program._search')
        @include('program._view-switcher')
        @include('program._user-actions')
    </div>
    <div id="action-bar-filter-info" class="py-2 container has-text-16" style="display: none;">
        <p>{{__("Resultados filtrados por:")}}</p>
        <div id="action-bar-filter-info-text" class="has-text-weight-bold"></div>
    </div>
</section>
