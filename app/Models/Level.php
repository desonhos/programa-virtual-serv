<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Level extends Model {

    protected $connection= 'cyex_online';

    /** Obtiene todos los del evento actual */
    public static function _getAll() {
        $levels = Level::where('event_id', config('web.eventId'))
                    ->orderBy('name')
                    ->get();
        return $level;
    }

    /** Obtiene por su id, del evento definido en config */
    public static function _get($id) {
        return Level::where('id', $id)->where('event_id', config('web.eventId'))->first();
    }

    /** Busca en el listado, aquel cuyo id se pasa por parámetro */
    public static function _find($id, $levels = null) {
        if (empty($levels)) {
            return Level::_get($id);
        }
        foreach ($levels as $level) {
            if ($level->id == $id) {
                return $level;
            }
        }
        return null;
    }

    public function getName() {
        return $this->name;
    }

    public function getId() {
        return $this->id;
    }
}
