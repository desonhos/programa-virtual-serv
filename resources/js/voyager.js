// function tinymce_setup_callback(editor) {
//     editor.remove();
//     editor = null;
  
//     tinymce.init({
//         menubar: false,
//         selector: 'textarea.richTextBox',
//         skin: 'voyager',
//         min_height: 200,
//         resize: 'vertical',
//         //plugins: 'link, image, code, youtube, table, textcolor, lists, paste',
//         plugins: 'link, image, code, youtube, table, textcolor, colorpicker, lists, table, paste, template',
//         extended_valid_elements: 'input[id|name|value|type|class|style|required|placeholder|autocomplete|onclick]',
//         toolbar: 'styleselect removeformat | bold italic underline | forecolor backcolor | template | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image table youtube | code',
//         paste_as_text: true,
//         convert_urls: false,
//         image_caption: true,
//         image_title: true,

//         link_class_list: [
//             { title: 'None', value: '' },
//             { title: 'Botón', value: 'button is-cta' },
//             { title: 'Botón oscuro', value: 'button is-cta is-dark' }
//         ],
//         language: 'es',
//         /*
//         https://www.tinymce.com/docs/configure/content-formatting/
//         https://www.tinymce.com/docs/configure/content-formatting/
//         */
//         content_css: '/css/app.css',
//         // style_formats: [
//         //     {title: 'Headers', items: [
//         //         {title: 'Header 1', format: 'h1'},
//         //         {title: 'Header 2', format: 'h2'},
//         //         {title: 'Header 3', format: 'h3'},
//         //         {title: 'Header 4', format: 'h4'}
//         //     ]},
//         //     {title: 'Inline', items: [
//         //         {title: 'Bold', icon: 'bold', format: 'bold'},
//         //         {title: 'Italic', icon: 'italic', format: 'italic'},
//         //         {title: 'Underline', icon: 'underline', format: 'underline'},
//         //         {title: 'Strikethrough', icon: 'strikethrough', format: 'strikethrough'},
//         //         {title: 'Superscript', icon: 'superscript', format: 'superscript'},
//         //         {title: 'Subscript', icon: 'subscript', format: 'subscript'},
//         //         {title: 'Code', icon: 'code', format: 'code'}
//         //     ]},
//         //     {title: 'Alignment', items: [
//         //         {title: 'Left', icon: 'alignleft', format: 'alignleft'},
//         //         {title: 'Center', icon: 'aligncenter', format: 'aligncenter'},
//         //         {title: 'Right', icon: 'alignright', format: 'alignright'},
//         //         {title: 'Justify', icon: 'alignjustify', format: 'alignjustify'}
//         //     ]},
//         //     {title: 'Texto', items: [
//         //         {title: 'Normal', inline: 'span', classes: 'text-body'},
//         //         {title: 'Grande', inline: 'span', classes: 'text-introduction'},
//         //         {title: 'Pequeño', inline: 'span', classes: 'text-small'},
//         //         {title: 'Destacado', inline: 'span', classes: 'text-quote'},
//         //         {title: 'Nota pequeña', inline: 'span', classes: 'text-options'}
//         //     ]}
//         // ],
//         templates : [
//             {
//                 title: "Fila de imágenes",
//                 description: "Fila de imágenes espaciadas. Es muy recomendable que las imágenes tengan el mismo tamaño o que al menos tengan la misma altura. Puede usarse con los socios estratégicos por ejemplo.",
//                 url: "/misc/tinymce/templates/fila_img.php"
//             },
//             {
//                 title: "Cuotas de inscripción",
//                 description: "Tabla con las cuotas de inscripción al congreso",
//                 url: "/misc/tinymce/templates/tabla_inscripcion.html"
//             },
//             {
//                 title: "Tarifas de alojamiento",
//                 description: "Tabla con las tarifas de alojamiento en hoteles oficiales",
//                 url: "/misc/tinymce/templates/tabla_hoteles.html"
//             }
//         ],
//         file_browser_callback: function file_browser_callback(field_name, url, type, win) {
//             if (type == 'image') {
//                 $('#upload_file').trigger('click');
//             }
//         },
//     });
// }