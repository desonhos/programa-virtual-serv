<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Paper extends Model {

    protected $connection= 'cyex_online';
    protected $table = 'papers';
    public $timestamps = false;

    /** Obtiene ponencia por su id, del evento definido en config */
    public static function _get($paperId) {
        $papers = Paper::where('id', $paperId)->where('event_id', config('web.eventId'))->get();
        if ($papers) {
            return $papers->first();
        }
        return null;
    }
    /** Obtiene todas las de una sesión en el evento definido en config */
    public static function _getWhereSession($sessionId) {
        $isVirtual = !\SettingHelper::isApp();
        $papers = Paper::where('session_id', $sessionId)
                        ->where('event_id', config('web.eventId'))
                        ->when($isVirtual == true, function ($q) {
                            return $q->whereNull('pv_hide_pv')
                                    ->orWhere('pv_hide_pv', '!=', 1);
                        })
                        ->when($isVirtual == false, function ($q) {
                            return $q->whereNull('pv_hide_webapp')
                                    ->orWhere('pv_hide_webapp', '!=', 1);
                        })
                        ->orderBy('orden')
                        ->orderBy('hour')
                        ->orderBy('hour_end')
                        ->get();
        return $papers;
    }
    /** Obtiene todas las ponencias del evento actual */
    public static function _getAll() {
        return Paper::where('event_id', config('web.eventId'))->get();
    }
    /** Obtiene las ponencias cuyo id se encuentra en el array pasado por parámetro
     * (para el evento actual) */
    public static function _getWhereIn($paperIds) {
        return Paper::where('event_id', config('web.eventId'))
                    ->whereIn('id', $paperIds)
                    ->get();
    }

    public function generateVimeoUrl($data) {
        foreach ($data as $vimeoInfo) {
            if ($vimeoInfo['name'] == $this->getId()) {
                $vimeoId = substr($vimeoInfo['uri'], strrpos($vimeoInfo['uri'], "/")+1);
                $this->setVimeoUrl(config('web.vimeo.playerUrl') . $vimeoId);
                return true;
            }
        }
        return false;
    }

    /** Obtiene los conflictos de una ponencia
     * (TODO: está por ver qué hacer cuando lo editan desde la sala de ponentes porque
     * por lo visto lo guardan en otra base de datos) */
    public function getConflicts() {
        $management = \DB::connection('cyex_online')
                    ->table('managements')
                    ->where('event_id', $this->event_id)
                    ->where('id_ponencia', $this->id)
                    ->first();
        if (empty($management)) { return null; }
        $conflicts = $management->conflictos;
        if (empty($conflicts) || $conflicts == '[]') { return null; }
        
        /*
        Por lo visto en la BD, a veces tiene formato JSON, otras texto plano con saltos
        de línea y otras formato JSON de un solo valor donde ese valor tiene saltos de línea
        en su interior (en una posición tiene varios conflictos de interés)
        */
        if (json_validate($conflicts)) {
            return json_decode($conflicts);
        }

        $conflicts = str_replace('["', '', $conflicts);
        $conflicts = str_replace('"]', '', $conflicts);

        // quita el salto de línea
        $conflicts = preg_replace("/\R/", '","', $conflicts);
        return explode('","', $conflicts);
    }

    /************************** GETTERS ***************************************/
    public function getName() {
        return $this->title;
    }

    public function getAuthors() {
        return trim($this->author_string);
    }

    public function getInitHour() {
        return substr($this->hour, 0, 5);
    }

    public function getEndHour() {
        return substr($this->hour_end, 0, 5);
    }

    public function getHour() {
        $initHour = $this->getInitHour();
        $endHour = $this->getEndHour();
        if (empty($endHour)) { return $initHour; }
        if (empty($initHour)) { return $endHour; }
        return $initHour . "-" . $endHour;
    }

    public function getId() {
        return $this->id;
    }

    public function getMailcontact() {
        return explode(';', $this->mailcontact);
    }

    public function getSessionid() {
        return $this->session_id;
    }

    /** Se pasa la sesión a la que pertenece la ponencia (para ahorrar llamadas a la base de datos) */
    public function getVimeoUrl($session) {
        if (\App::environment('production')) {
            if ($session->isPast()) {
                return $this->pv_vimeo_url;
            }
            return null;
        }
        return $this->pv_vimeo_url;
    }

    public function getDocs() {
        $docs = [];
        if (($this->pv_doc1_link !== null) && ($this->pv_doc1_link !== "")) {
            array_push($docs, [
                'link' => $this->pv_doc1_link,
                'ico' => ($this->pv_doc1_ico == null) ? "pdf" : $this->pv_doc1_ico,
                'webapp' => ($this->pv_doc1_visible_webapp == 1) ? 1 : 0,
                'plataforma' => ($this->pv_doc1_visible_plataforma == 1) ? 1 : 0,
            ]);
        }
        if (($this->pv_doc2_link !== null) && ($this->pv_doc2_link !== "")) {
            array_push($docs, [
                'link' => $this->pv_doc2_link,
                'ico' => ($this->pv_doc2_ico == null) ? "pdf" : $this->pv_doc2_ico,
                'webapp' => ($this->pv_doc2_visible_webapp == 1) ? 1 : 0,
                'plataforma' => ($this->pv_doc2_visible_plataforma == 1) ? 1 : 0,
            ]);
        }
        return $docs;
    }

    /** Devuelve verdadero en caso de que esta ponencia tenga un email de contacto
     * y de que se haya marcado con un 1 el campo visible_mailcontact
     */
    public function getVisibleMailcontact() {
        $field = "pv_visible_mailcontact";
        return (!empty($this->getMailcontact())) && $this->{$field};
    }

    /************************** SETTERS ***************************************/
    public function setVimeoUrl($url) {
        // TODO: No he podido hacerlo con Eloquent porque no admite claves primarias compuestas
        // y de usarlo almacena en todas las ponencias cuyo id sea igual al que se pasa aquí
        \DB::connection('cyex_online')->table($this->table)
              ->where('id', $this->id)
              ->where('event_id', $this->event_id)
              ->update(['pv_vimeo_url' => $url]);
    }

    public function isPoster() {
        return $this->poster;
    }
}
