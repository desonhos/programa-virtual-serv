@extends('layouts.app' )

@section('css')
    <style>
        .section h2 { text-transform: uppercase; }
    </style>
@endsection

@section('js')
@endsection


@section('page-class', "page-info isVisor")

@section('content')
    <article>
        <section class="section">
            <div class="container content">
                {!! $info !!}
            </div>
        </section>
    </article>
@endsection
