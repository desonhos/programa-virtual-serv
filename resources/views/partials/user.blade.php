@php
    $user = Auth::guard('multi')->user();
@endphp

@if (!empty($user))
    <section class="section pt-1 pb-1" id="profile-bar">
        <div class='container'>
            <div class='content'>
                {{ $user->getName() . " " . $user->getSurname() . " " . $user->getSurname2() }}
            </div>
        </div>
    </section>
@endif
