@php
$sessionVimeoUrl = $session->getVimeoUrl();
@endphp

<script>
    playingVideo = false;
    interval = 60000; // cada cuánto (ms) se almacena tiempo reproducido
    alreadyPlayed = false;
    pcv = false;    // indica si hay que controlar que el usuario sigue viendo el vídeo
    pcvSecs = false;
    pcvTimeToAnswer = false;
    pcvQuestion = false;
    pcvConfirm = false;
    checkViewingIntervalId = false;
    storeLastSecondPlayed = eval("@php if (empty($sessionPrevVimeoUrl)) { echo "true"; } else { echo "false"; } @endphp");
    lastSecondPlayed = parseInt("{{ $session->getLastSecondPlayedByCurrentUser() }}") || 0;
    firstPlayAfterReloadPage = true;
    esAnuncio = eval("@php if (!empty($sessionPrevVimeoUrl)) { echo "true"; } else { echo "false"; } @endphp");

        @if (\SettingHelper::isCheckingUserView())
            pcv = true;
            pcvMsecs = parseInt("{{ \SettingHelper::getPlayerCheckViewSecs() }}")*1000;
            pcvQuestion = "{{ \SettingHelper::getPlayerCheckViewQuestion() }}";
            pcvConfirm = "{{ \SettingHelper::getPlayerCheckViewConfirm() }}";
        @endif
        @if (!empty(\SettingHelper::getPlayerStoreViewSecs()))
            var inter = parseInt("{{ \SettingHelper::getPlayerStoreViewSecs() }}") * 1000;
            if (!isNaN(inter) && (inter > 0)) { interval = inter; }
        @endif

    $(document).ready(function() {
        initVimeoIframe();
        console.log(pcv);
        $("#checkViewingModal .close").click(checkViewingOk);
    });

    function initVimeoIframe() {
        console.log("init video iframe");
        var iframe = document.querySelector('#sessionVideoContainer iframe');
        if (iframe == null) { return; }
        player = new Vimeo.Player(iframe);

        player.on('play', function() {
            console.log('played the video!');
            playingVideo = true;
            if (pcv) {
                console.log("iniciar el contador hasta que salta la pregunta");
                checkViewingIntervalId = setInterval(checkViewing, pcvMsecs);
            }
            //AQUÍ: Si es el anuncio no hace nada más
            console.log(esAnuncio, firstPlayAfterReloadPage, lastSecondPlayed);
            if (!esAnuncio) {
                if (firstPlayAfterReloadPage) {
                    // Si no, si es el primer play y había último segundo almacenado --> salta a ese segundo
                    firstPlayAfterReloadPage = false;
                    if (lastSecondPlayed) {
                        player.setCurrentTime(lastSecondPlayed).then(function(seconds) {
                            console.log("Se avanza hasta el último punto visualizado");
                        });
                    }
                }
            }
        });
        player.on('pause', function() {
            console.log('paused the video!');
            playingVideo = false;
            if (pcv) {
                console.log("Para (lo reinicializa) el contador hasta que salta la pregunta");
                clearInterval(checkViewingIntervalId);
            }
        });
        player.on('ended', function() {
            console.log('ended the video!');
            playingVideo = false;
            @if (!empty($sessionPrevVimeoUrl) && !empty($sessionVimeoUrl))
                if (!alreadyPlayed) {
                    // ya se reprodujo el vídeo de anuncio, ahora se da al play automáticamente al vídeo en sí
                    alreadyPlayed = true;
                    esAnuncio = false;
                    player.loadVideo('{{ $sessionVimeoUrl }}').then(function() {
                        player.play();
                        storeLastSecondPlayed = true;
                    });
                }
            @endif
            if (pcv) {
                console.log("STOP. Para (lo reinicializa) el contador hasta que salta la pregunta");
                clearInterval(checkViewingIntervalId);
            }
        });

        player.ready().then(function(){
            setInterval(checkPlaying, interval);
        });
    }

    /** Se llama cada X tiempo para que en caso de que se esté reproduciendo el vídeo
    actualizar el número de segundos que el usuario ha visto del mismo */
    function checkPlaying() {
        if (playingVideo) {
            player.getCurrentTime().then(function(seconds) {
                console.log("Último segundo reproducido: " + seconds);
                var data = {
                    'seconds':interval/1000,
                    '_token': '{{ csrf_token() }}',
                    'lastSecondPlayed': 'false'
                };
                if (storeLastSecondPlayed) {
                    console.log("guarda el último segundo reproducido");
                    data = {
                        'seconds':interval/1000,
                        '_token': '{{ csrf_token() }}',
                        'lastSecondPlayed': Math.floor(seconds)
                    };
                }
                else {
                    console.log("no guarda el último segundo reproducido");
                }
                // está en reproducción, añade tiempo de visualización
                $.ajax({
                    type: "POST",
                    url: '{{ route('sessions.viewing', ['session_id' => $session->getId()]) }}',
                    data: data,
                    success: function(respuesta) {
                        console.log("se añadió tiempo de visualización");
                    },
                    error: function(response) {
                        console.log("error guardando el tiempo de reproducción");
                        if (jqXHR.status === 0) {
                            console.log('Not connect: Verify Network.');
                        } else if (jqXHR.status == 404) {
                            console.log('Requested page not found [404]');
                        } else if (jqXHR.status == 500) {
                            console.log('Internal Server Error [500].');
                        } else if (textStatus === 'parsererror') {
                            console.log('Requested JSON parse failed.');
                        } else if (textStatus === 'timeout') {
                            console.log('Time out error.');
                        } else if (textStatus === 'abort') {
                            console.log('Ajax request aborted.');
                        } else if (jqXHR.status == 401) {
                            // se le ha cerrado la sesión
                            window.alert("Ha caducado su sesión o se ha iniciado desde otro dispositivo");
                            window.location.href =  $("#urlLogin").html();
                        } else {
                            console.log('Uncaught Error: ' + jqXHR.responseText);
                        }

                        setTimeout(checkSessionChange, interval);
                    }
                });
            });
        }
    }

    /** Se llama cuando se quiere comprobar que el usuario sigue viendo el vídeo */
    function checkViewing() {
        $("#checkViewingModal").addClass("is-active");
        document.exitFullscreen();
        player.pause();
    }

    /** Se llama cuando el usuario ha indicado que está viendo el vídeo */
    function checkViewingOk() {
        console.log("check viewing OK");
        $("#checkViewingModal").removeClass("is-active");
        player.play();
    }
</script>

@include('session.video.modal-check-viewing')
