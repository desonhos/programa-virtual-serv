<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PosterRating extends Model {

    protected $connection= 'cyex_online';
    protected $table = 'pv_poster_ratings';
    public $timestamps = false;
    protected static $_table = 'pv_poster_ratings';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['paper_id', 'event_id', 'user_id_in_event', 'rating'];

    /** Obtiene todas las del evento actual */
    public static function _getAll() {
        $rooms = PosterRating::where('event_id', config('web.eventId'))
                    ->orderBy('orden')
                    ->orderBy('name')
                    ->get();
        return $rooms;
    }

    /** Obtiene por su id, del evento definido en config */
    public static function _get($id) {
        return PosterRating::where('id', $id)->where('event_id', config('web.eventId'))->first();
    }

    /** Obtiene la valoración media de un póster.
     * @return Array False si no hay votaciones o un array(media, n_votaciones)
    */
    public static function _getAverageRatingOfPaper($paperId) {
        $average = 0;
        $posterRatings = PosterRating::where('paper_id', $paperId)
                            ->where('event_id', config('web.eventId'))
                            ->get();
        if (!$posterRatings->count()) { return false; }
        foreach ($posterRatings as $pr) {
            $average = $average + $pr->rating;
        }
        return [($average/$posterRatings->count()), $posterRatings->count()];
    }

    /** Obtiene la puntuación que dio el usuario actual a la ponencia cuyo id se pasa por parámetro */
    public static function _getUserRatingOfPaper($paperId) {
        $user = \Auth::guard('multi')->user();
        if (empty($user)) { return false; }
        return PosterRating::where('paper_id', $paperId)
                            ->where('event_id', config('web.eventId'))
                            ->where('user_id_in_event', $user->getIdInEvent())
                            ->value('rating');

    }

    public static function _setUserRatingOfPaper($paperId, $rating) {
        $user = \Auth::guard('multi')->user();
        if (empty($user)) { return false; }

        PosterRating::updateOrCreate(
            [
                'paper_id' => $paperId,
                'event_id' => config('web.eventId'),
                'user_id_in_event' => $user->getIdInEvent()
            ],
            ['rating' => $rating]
        );
    }

    public function getPaperId() { return $this->paper_id; }
    public function getEventId() { return $this->event_id; }
    //public function getUserIdInEvent() { return $this->user_id_in_event; }
    //public function getRating() { return $this->rating; }
}
