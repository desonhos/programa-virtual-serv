@extends('layouts/app')

@section('css')
@endsection

@section('js')
@endsection

@section('page-class', 'page-info')

@section('content')
    <article>
        <section class="section">
            <div class="container">
                <h1 class="title is-uppercase">{{ __("Patrocinadores") }}</h1>
                <div class="">
                    @foreach ($sponsors as $sponsor)
                        @php
                        /*$mt = (in_array($sponsor->getLogo(), [
                            'bayer.jpg', 'daiichi.jpg', 'edwards.jpg', 'ge-healthcare.jpg',
                            'rovi.jpg', 'novo-nordisk.jpg', 'boston.jpg', 'menarini.jpg'
                        ])) ? "3rem" : "1rem";*/
                        $mt = "1rem";
                        @endphp
                        <div class="sponsor" style="margin-top: {{ $mt }};">
                            @if (!empty($sponsor->getLogo()))
                                <img
                                    style="max-height: 80px; margin-bottom: .5rem;{{ $sponsor->getAdditionalStyle() }}"
                                    src="{{ $sponsor->getLogo() }}">
                            @endif
                            <span class="is-block has-text-weight-bold">{{ $sponsor->getName() }}</span>
                            @if (!empty($sponsor->getNstand()))
                                <span class="is-block">{{ __("Nº de stand:") }} {{ $sponsor->getNstand() }}</span>
                            @endif
                            @if (!empty($sponsor->getUrl()))
                                <a class="is-block" href="{{ $sponsor->getUrl() }}">
                                    {{ $sponsor->getUrl() }}
                                </a>
                            @endif
                        </div>
                    @endforeach
                </div>
            </div>
        </section>
    </article>
@endsection
