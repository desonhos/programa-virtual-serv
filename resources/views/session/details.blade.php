@php
    $dateAndHour = $session->getDateAndHour();
    $room = $session->getRoomName();
    $area = $session->getAreaName();
    $level = $session->getLevelName();
    $type = $session->getType();
    $moderators = $session->getModerators();
    $objectives = $session->getObjectives();
    $sessionType = $session->getSessiontypeName();
    $sessionId = $session->getId();
    $sponsorHtml = $session->getSponsorHtml();
    $televoto = $session->getTelevoto();
@endphp

<div id="sessionDetails" class="_content_ has-text-16 has-text-14-mobile">
    @include('session._actions')
    @if (!empty($dateAndHour))
        <p><span class="label">{{__("Fecha")}}</span>: {{ $dateAndHour }}</p>
    @endif
    @if (!empty($room))
        <p><span class="label">{{__("Sala")}}</span>: {{ $room }}</p>
    @endif
    {{-- @if (!empty($area))
        <p><span class="label">Categoría</span>: {{ $area }}</p>
    @endif --}}
    @if (!empty($sessionType))
        <p><span class="label">{{__("Tipo sesión")}}</span>: {{ $sessionType }}</p>
    @endif
    @if (!empty ($level))
        <p><span class="label">{{__("Nivel")}}</span>: {{ $level }}</p>
    @endif
    @if (!empty($type))
        <p><span class="label">{{__("Tipo")}}</span>: {{ $type }}</p>
    @endif
    @if (!empty($moderators))
        <p><span class="label">{{__("Modera")}}</span>: <span class="mod_1">{{ $moderators }}</span></p>
    @endif
    @if (!empty($objectives))
        <p><span class="label">{{__("Objetivos")}}</span>: <span class="objectives">{{ $objectives }}</span></p>
    @endif
    @if (!empty($sponsorHtml))
        <div>{!! $sponsorHtml !!}</div>
    @endif
    @if (!empty($televoto))
        <p class="mt-4">
            @include('session/_televoto')
        </p>
    @endif
    @if (!\SettingHelper::isApp())
        <p class="mt-4">
            @include('session/_survey')
        </p>
    @endif
</div>
