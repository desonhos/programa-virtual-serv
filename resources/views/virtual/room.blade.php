@extends('layouts.app')

@if ($vroom->isExhibitorOrExhibition())
    @section('page-class', 'is-exhibitor')
    @section('js')
        <script src="{{ mix('js/pages/room.js') }}" defer></script>
        <script src="{{ mix('js/pages/exhibitor.js') }}" defer></script>
    @endsection
@else
    @section('page-class', '')
    @section('js')
        <script src="{{ mix('js/pages/room.js') }}" defer></script>
    @endsection
@endif

@section('content')

    {{-- TODO: crear un modelo con el contenido de esta vista --}}
    @include('virtual.room.utils')

    {{-- Vista de la sala con las diferentes áreas clicables --}}
    <section class="has-text-centered mapAreasContainer">
        @include('virtual.room.mapareas')
        @include('virtual.room.navigation')
    </section>

    @include('virtual.room.legal')

@endsection
