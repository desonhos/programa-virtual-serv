{{-- Enlaces de los archivos --}}
@php $docs = $paper->getDocs(); @endphp
@if (count($docs))
    <div class="docs mt-3">
        @foreach ($docs as $doc)
            {{-- <p>{{ json_encode($doc) }}</p> --}}
            @if (isVisible($doc))
                @if (strtolower($doc['ico']) !== "audio")
                    <a href="{{ $doc['link'] }}">
                        @php $ico = 'ico-ponencia-' . strtolower($doc['ico']); @endphp
                        @svg($ico, $ico)
                    </a>
                @else
                    <audio controls>
                        <source src="{{ $doc['link'] }}" type="audio/mpeg">
                        {{__("Tu navegador no dispone de reproductor de audio, prueba con otro.")}}
                    </audio>
                @endif
            @endif
        @endforeach
    </div>
@endif
{{-- Fin enlaces de los archivos --}}
