$(document).ready(function() {
    // Eventos de pulsar en una de las letras
    handleClickPaginationLinks();

    // Por defecto muestra el primero
    showDefaultPage();
});

function handleClickPaginationLinks() {
    $(".pagination-link").click(function(ev) {
        ev.preventDefault();
        window.location.hash = $(this).attr("href");
    });
    // al hacerlo así, también se llama si se le da al botón atrás y te lleva a otra letra
    $( window ).on( 'hashchange', function( e ) {
        showPageFromHash();
    });
}

// Por defecto muestra el primero, si se le  ha dado atrás se  muestra la página
// que se estaba viendo
function showDefaultPage() {
    var hash = window.location.hash;
    if (hash && ($("input[name=searchText]").val() == "")) {
        showPageFromHash();
    }
    else {
        // por defecto muestra el primero
        $pagLinks = $(".pagination.has-paginated-content .pagination-link");
        if ($pagLinks.length) {
            $(".alphaTab.is-hidden").removeClass("is-hidden");
            if ($pagLinks.length > 1) {
                $($pagLinks[1]).click();
                $($pagLinks[0]).click();
            }
            else {
                $($pagLinks[0]).click();
            }
        }
    }
}

function showPageFromHash() {
    var hash = window.location.hash;
    var $elemToSelect = $(".pagination-list li a[href='" + hash + "']");
    $(".pagination-link").removeClass("is-selected");
    $elemToSelect.addClass("is-selected");
    $(".paginated-content").addClass("is-hidden");
    $("*[data-id='"+$elemToSelect.attr("href")+"']").removeClass("is-hidden");
}
