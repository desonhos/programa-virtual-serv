$(document).ready(function() {
    // Abre el cuadro al pulsar en el botón "compartir"
    $("#sharePage").click(function(ev){
        ev.preventDefault();
        $("#sharePageModal").addClass('is-active');
    });

    // Cierra al pulsar en cancelar
    $("#sharePageModal .close.button").click(function() {
        $("#sharePageModal").removeClass('is-active');
    });
});
