<a class="agendar" href='{{ route('sessions.addToSchedule', ['id' => $session->getId()]) }}' @if ($session->isInSchedule()) style='display: none' @endif>
    @svg('ico-agendar', 'ico-agendar')
</a>
<a class="agendada" href='{{ route('sessions.removeFromSchedule', ['id' => $session->getId() ]) }}' @if (!$session->isInSchedule()) style='display: none' @endif>
    @php
    /* esto es si se quiere hacer el control de sesiones coincidentes sobre todas, no sobre las que se marcaron en el momento de insertarlas */
    // $isOverlapping = ($schedule->isSessionInSchedule($session->getId()) && $schedule->isOverlappingSession($session->getId())) ? true : false;
    // $classAgendada = 'ico-agendada';
    // $classAgendaConflicto = 'ico-agenda-conflicto';
    // if ($isOverlapping) {
    //     $classAgendada .= ' is-hidden';
    // } else {
    //     $classAgendaConflicto .= ' is-hidden';
    // }
    $isOverlapping = ($schedule->isSessionInSchedule($session->getId()) && $schedule->isSetAsOverlapping($session->getId())) ? true : false;
    $classAgendada = 'ico-agendada';
    $classAgendaConflicto = 'ico-agenda-conflicto';
    if ($isOverlapping) {
        $classAgendada .= ' is-hidden';
    } else {
        $classAgendaConflicto .= ' is-hidden';
    }
    @endphp
    @svg('ico-agendada', $classAgendada)
    @svg('ico-agenda-conflicto', $classAgendaConflicto)
</a>
