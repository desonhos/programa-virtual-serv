<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class InfoController extends Controller {
    public function view() {
        $info = \SettingHelper::getInfo();
        return view('info', compact('info'));
    }
}
