<div class="modal" id="notesModal">
    <div class="modal-background"></div>
    <div class="modal-card">
        <header class="modal-card-head">
            <p class="modal-card-title is-uppercase is-flex is-align-items-center mb-0 is-size-5">
                @svg('ico-notas', 'ico-notas mr-1') {{ __("Crear una nota") }}
            </p>
            <button class="delete" aria-label="close"></button>
        </header>
        <section class="modal-card-body">
            <p><span class="is-uppercase">{!! FilterHelper::_isArea() ? $session->getAreaName() : $session->getSessiontypeName()!!}</span>. {!! $session->getName() !!}</p>
            <textarea class="w-100 p-1" name='noteText' rows='8' placeholder="{{ __("Escribe tu nota sobre la sesión") }}"></textarea>
        </section>
        <footer class="modal-card-foot is-flex is-justify-content-space-between">
            <a class="button is-warning is-link pl-0 letter-spacing-0 deleteNote">{{ __("Eliminar") }}</a>
            <button class="button submit">{{ __("Guardar") }}</button>
        </footer>
    </div>
</div>
