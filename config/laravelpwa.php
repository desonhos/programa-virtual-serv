<?php

function getVURL($relativePath) {
    // Parámetro a cambiar cuando se modifiquen los iconos y forzar la recarga de las aplicaciones ya instaladas
    $v = '20240418'; // no usé el paquete hecho para EZP porque desde aquí no se tienen las funciones de Laravel
    return $relativePath . "?v=".$v;
}

return [
    'name' => env('EVENTO_NOMBRE'),
    'manifest' => [
        'name' => env('APP_NAME', 'Programa virtual'),
        'short_name' => env('EVENTO_NOMBRE_CORTO', 'Evento'),
        'start_url' => '/',
        'background_color' => '#ffffff',
        'theme_color' => '#000000',
        // 'display' => 'minimal-ui',
        'display' => 'fullscreen',
        'orientation'=> 'any',
        'status_bar'=> 'black',
        'icons' => [
            '72x72' => [
                'path' => getVURL('/event/icons/72.png'),
                'purpose' => 'any'
            ],
            '96x96' => [
                'path' => getVURL('/event/icons/96.png'),
                'purpose' => 'any'
            ],
            '128x128' => [
                'path' => getVURL('/event/icons/128.png'),
                'purpose' => 'any'
            ],
            '144x144' => [
                'path' => getVURL('/event/icons/144.png'),
                'purpose' => 'any'
            ],
            '152x152' => [
                'path' => getVURL('/event/icons/152.png'),
                'purpose' => 'any'
            ],
            '192x192' => [
                'path' => getVURL('/event/icons/192.png'),
                'purpose' => 'any'
            ],
            '384x384' => [
                'path' => getVURL('/event/icons/384.png'),
                'purpose' => 'any'
            ],
            '512x512' => [
                'path' => getVURL('/event/icons/512.png'),
                'purpose' => 'any'
            ],
        ],
        'splash' => [
            '640x1136' => getVURL('/event/icons/inicio.jpg'),
            '750x1334' => getVURL('/event/icons/inicio.jpg'),
            '828x1792' => getVURL('/event/icons/inicio.jpg'),
            '1125x2436' => getVURL('/event/icons/inicio.jpg'),
            '1242x2208' => getVURL('/event/icons/inicio.jpg'),
            '1242x2688' => getVURL('/event/icons/inicio.jpg'),
            '1536x2048' => getVURL('/event/icons/inicio.jpg'),
            '1668x2224' => getVURL('/event/icons/inicio.jpg'),
            '1668x2388' => getVURL('/event/icons/inicio.jpg'),
            '2048x2732' => getVURL('/event/icons/inicio.jpg'),
        ],
        
        'custom' => []
    ]
];
