@component('mail::message')

# {{ __("Has solicitado la regeneración de contraseña para") }} {{ config('app.name') }}.

{{ __("Si no has solicitado esta operación, puedes ignorar este mensaje") }}.

{{ __("Utiliza la siguiente contraseña provisional para acceder a tu cuenta. Te recomendamos que la cambies desde «Mi perfil» una vez hayas accedido al sistema") }}

* {{ __("Contraseña provisional")}}: {{ $defaultPassword }}

@component('mail::button', ['url' => route('login')])
    {{ __("Accede a") }} {{ config('app.name') }}
@endcomponent

@endcomponent
