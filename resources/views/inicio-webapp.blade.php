@extends('layouts/app')

@section('page-class', 'page-inicio-webapp')

@section('content')
    <div id="wrap">
        <div id="logos">
            {{-- <img class="logo-congreso" src="/event/inicio-logo-congreso.png"> --}}
            <img class="logo-sociedad" src="/event/inicio-logo-sociedad.png">
            {{-- <a class="logo-patrocinador" href="https://www.jnj.com/" target="_blank">
                <img src="/event/inicio-patro.png">
            </a> --}}
        </div>
        <div id="gridMenu">
            @include('partials._menu')
        </div>
    </div>
    {{-- Botón de añadir a la página de inicio --}}
    @if (config('web.hasWebapp'))
        <a class="btnAddHome" href="{{ route('webapp.add-to-homescreen') }}">
            @svg('ico-+inicio')
            <p class="leyenda">
                {{ __('Añadir a pantalla de inicio') }}
            </p>
        </a>
    @endif
    {{-- Botón de iniciar sesión --}}
    @if (!\Auth::guard('multi')->user() && !\SettingHelper::isAntesInicioVirtual())
        <a href="{{ route('login') }}" id="loginBtnInicio" role="button" class="navbar-burger is-usermenu burger ml-auto is-flex is-align-items-center is-justify-content-center">
            @svg('ico-usuario', 'ico-usuario')
        </a>
    @endif

    @include('partials.anuncio')
@endsection
