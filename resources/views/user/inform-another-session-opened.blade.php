@extends('layouts/app')

@section('page-class', 'page-inform-another-session-opened')

@section('content')
    <article class="message is-warning">
        <div class="message-body">
            {{__("Ha iniciado sesión desde otro dispositivo.")}}
        </div>
    </article>
    <section class="section pt-5 pb-5">
        <div class="container">
            <h1 class="title">{{__("Sesión cerrada")}}</h1>
            <p class="content">{{__("Se ha iniciado sesión desde otro dispositivo. Puede volver a acceder y cerrará la otra sesión abierta.")}}</p>
            <a href="{{ config('web.loginUrl') }}" class="button is-dark">{{__("Volver a acceder")}}</a>
        </div>
@endsection
