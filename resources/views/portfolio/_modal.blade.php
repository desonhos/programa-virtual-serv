@php
    $pdfProgram = \VoyagerHelper::getFileUrl(setting('web.program_pdf'));
    $pdfAbstract = \VoyagerHelper::getFileUrl(setting('web.abstract_pdf'));
    $resources = \App\Models\Resource::_getAll();
@endphp

<div class="modal" id="portfolioModal">
    <div class="modal-background"></div>
    <div class="modal-card">
        <header class="modal-card-head">
            <p class="modal-card-title is-uppercase is-flex is-align-items-center mb-0 is-size-5">
                @svg('ico-cartera', 'ico-cartera mr-2') {{__("Mi cartera")}}
            </p>
            <button class="delete" aria-label="close"></button>
        </header>
        <section class="modal-card-body">
            @if (!empty(config('web.event.pdfAttendanceCertificateLink')))
                <p class="portfolioItem is-flex is-align-items-center is-size-5">
                    <span class="svgWrapper">@svg('ico-cartera-pdf-certificado-asist', 'ico-cartera-pdf-certificado-asist mr-2')</span>
                    <span class="text">{{__("Certificado de asistencia")}}</span>
                    <a class="ml-auto button" href="{{ route('certificate.generate') }}">{{__("Ver")}}</a>
                </p>
            @endif
            @if (!empty($pdfAbstract))
                <p class="portfolioItem is-flex is-align-items-center is-size-5">
                    <span class="svgWrapper">@svg('ico-cartera-pdf-libro', 'ico-cartera-pdf-libro mr-2')</span>
                    <span class="text">{{ __("Libro de abstracts") }}</span>
                    <a class="ml-auto button" href="{{ $pdfAbstract }}">{{__("Ver")}}</a>
                </p>
            @endif
            @if (!empty($pdfProgram))
                <p class="portfolioItem is-flex is-align-items-center is-size-5">
                    <span class="svgWrapper">@svg('ico-programa-pdf', 'ico-programa-pdf mr-2')</span>
                    <span class="text">{{ __("Programa en PDF") }}</span>
                    <a class="ml-auto button" href="{{ $pdfProgram }}">Ver</a>
                </p>
            @endif
            @foreach ($resources as $resource)
                <p class="portfolioItem is-flex is-align-items-center is-size-5">
                    <span class="svgWrapper">@svg($resource->getIco(), $resource->getIco() . ' mr-2')</span>
                    <span class="text">{{ $resource->getName() }}</span>
                    <a class="ml-auto button" href="{{ $resource->getDocumentUrl() }}">Ver</a>
                </p>
            @endforeach
        </section>
    </div>
</div>
