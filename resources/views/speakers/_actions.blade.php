<ul id="speakerActions" class="links-list is-horizontal mb-4">
    <li class="">
        <a id="sharePage" class="noDynamicStyles has-text-text is-flex is-align-items-baseline">
            <span class="mr-1 is-size-7">{{__("Compartir")}}</span>
            @svg('ico-compartir', 'ico-compartir')
        </a>
    </li>
</ul>
