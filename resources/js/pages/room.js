$(document).ready(function() {
    clicAreaHandler();
});

/** Si el área no tiene grupo, la abre directamente.
 * En otro caso muestra la ventana del grupo con los diferentes enlaces que hay
 */
function clicAreaHandler() {
    $(document).ready(function() {
        $("map area").click(function(ev) {
            if ($(this).data("grupo") !== "") {
                var grupo = $(this).attr("data-grupo");
                ev.preventDefault();
                $('.linksModal[data-id="'+grupo+'"]').addClass('is-active'); // Muestra la ventanta que tiene los enlaces del grupo
            }
            else if (true) {

            }

        });
        // Cierra al pulsar en cancelar
        $(".linksModal .close.button").click(function() {
            $(".linksModal").removeClass('is-active');
        });

        // Ventana con el visor de vídeo (al pulsar en la ventana del grupo)
        if ($(".exhibitorLink.is-video").length) {
            // Muestra el vídeo al pulsar en el enlace
            $(".exhibitorLink.is-video").click(function(ev){
                ev.preventDefault();
                // Cierra la ventana de enlaces
                $(".linksModal").removeClass('is-active');
                // Muestra el visor del vídeo
                var dataAreaId = $(this).attr("data-areaid");
                $(".modalVideo[data-id='" + dataAreaId + "']").addClass('is-active');
            });
        }

        // Ventana con el visor del vídeo, cuando se muestra directamente
        if ($(".area.is-video").length) {
            // Muestra el vídeo al pulsar en el enlace
            $(".area.is-video").click(function(ev){
                ev.preventDefault();
                // Cierra la ventana de enlaces
                $(".linksModal").removeClass('is-active');
                // Muestra el visor del vídeo
                var dataAreaId = $(this).attr("data-areaid");
                $(".modalVideo[data-id='" + dataAreaId + "']").addClass('is-active');
            });
        }

        // Cierra al pulsar en cancelar
        $(".modalVideo .close.button").click(function() {
            // para que se para el play
            var $modalVideo = $(this).closest(".modalVideo");
            if ($modalVideo) {
                $modalVideo.removeClass('is-active');
                var $modalCardBody = $modalVideo.find(".modal-card-body");
                var html = $modalCardBody.html();
                $modalCardBody.html("");
                $modalCardBody.html(html);
            }
        });
    });
}
