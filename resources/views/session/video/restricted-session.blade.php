@php
$dImg = setting('web.defaultvideo_img');
$dImg = (!empty($dImg)) ? \VoyagerHelper::getImageUrl($dImg) : config('web.event.defaultImg');
@endphp

<div class="is-relative" id="prevVideo">
    <img src="{{ $dImg }}">
    <div class="is-overlay is-flex is-justify-content-center is-align-items-center">
        <div class="has-text-24">
            <p class="is-uppercase has-text-centered">{{ __("No tienes acceso a esta sesión") }}</p>
        </div>
    </div>
</div>
