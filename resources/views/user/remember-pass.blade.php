@extends('layouts/login')

@section('page-class', 'page-virtual is-login is-remember-pass')

@section('form')

    @if (!\Session::has('success'))
        <div class="has-text-16 mb-4 has-text-grey">
            {{__("Si has olvidado tu contraseña, indica tu correo electrónico y te enviaremos instrucciones para poder acceder.")}}
        </div>

        <form method="POST" action="{{ route('remember-password') }}">
            @csrf
            <div class="field">
                <div class="control">
                    <input name="email" class="input" type="text" value="" placeholder="{{__("Correo electrónico")}}">
                </div>
            </div>
            <div class="field pt-2">
                <div class="control has-text-right">
                    <input class="button is-primary is-outline" type="submit" value="{{__("Enviar")}}">
                </div>
            </div>
        </form>
    @endif
    @if ($errors->any())
        <p class="help is-danger">
            {{$errors->first()}}
        </p>
    @endif
    @if (\Session::has('success'))
        <p class="help is-success">
            {!! \Session::get('success') !!}</li>
        </p>
    @endif
@endsection
