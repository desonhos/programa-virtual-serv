<?php

namespace App\Helpers;
use Illuminate\Support\Facades\Storage;

class VoyagerH {

    public static function getFileUrl($string) {
        if (empty($string)) { return false; }
        if (!\TextHelper::isJson($string)) { return $string; }
        return Storage::url(json_decode($string, true)[0]['download_link']);
    }

    public static function getImageUrl($string) {
        if (!\TextHelper::isUrl($string)) {
            if (!empty($string)) {
                return Storage::disk(config('voyager.storage.disk'))->url($string);
            }
        }
        return $string;
    }

}
