import imageMapResize from 'image-map-resizer/js/imageMapResizer.js';

$(document).ready(function() {
    $('map').imageMapResize();
    window.dispatchEvent(new Event('resize'));

    // if ($("body").hasClass("is-login")) {
    //     $("form input[name=email]").focus();
    // }
})
