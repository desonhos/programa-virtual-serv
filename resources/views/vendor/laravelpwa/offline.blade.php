@extends('layouts.app')

@section('content')

    <h1 style="width: 100%; text-align: center; font-weight: 700; margin-top: 5rem;">No ha sido posible conectarse a la red.<br/>Compruebe su conexión a internet.</h1>

@endsection