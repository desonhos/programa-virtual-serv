<?php

//TODO: Buscar un sitio mejor para esta clase. En helpers debería haber helpers genéricos y reusables entre proyectos

namespace App\Helpers;
use \App\Models\Area;
use \App\Models\Sessiontype;

class Filter {

    /** Obtiene la tabla por la que se destacarán y filtrarán las sesiones (programs [tipos de sesión] o areas) */
    public static function _getFilterTable() {
        return config('web.filterTable');
    }

    public static function _getColorlist($sessiontypes, $areas, $removeHashtag = false) {
        $filterTable = Filter::_getFilterTable();
        if ($filterTable == "programs") {
            return Sessiontype::_getColorlist($sessiontypes, $removeHashtag);
        }
        elseif ($filterTable == "areas") {
            return Area::_getColorlist($areas, $removeHashtag);
        }
        return null;
    }

    public static function _getFilterNameOfSession($sessiontypes, $areas, $session) {
        if (Filter::_isSessiontype()) {
            return $session->getSessiontypeName($sessiontypes);
        }
        if (Filter::_isArea()) {
            return $session->getAreaName($areas);
        }
        return false;
    }

    public static function _isSessiontype() {
        $filterTable = Filter::_getFilterTable();
        return ($filterTable === "programs");
    }

    public static function _isArea() {
        $filterTable = Filter::_getFilterTable();
        return ($filterTable === "areas");
    }
}
