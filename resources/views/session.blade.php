@extends('layouts.app' )

{{-- @section('title', $page->title)
@section('meta_description', $page->meta_description)
@section('meta_keywords', $page->meta_keywords)
@section('og_image', Storage::disk(config('voyager.storage.disk'))->url($page->image) )
@section('meta_robots', 'index, follow')
@section('meta_revisit-after', 'content="3 days') --}}

@section('css')
    @include('partials.dynamic-styles')
@endsection

<script>
    var reevaluateOverlappingSessionsUrl = '{{ route('sessions.reevaluateOverlapping') }}';
</script>

@php $isVirtual = !\SettingHelper::isApp(); @endphp

@section('js')
    <script src="https://player.vimeo.com/api/player.js"></script>
    @if (!empty($isVirtual))
        {!! $session->getLivechatCode() !!}
    @endif
    <script src="{{ mix('js/pages/session.js') }}" defer></script>
@endsection

@php
    $pageClass = 'page-session';
    if (!empty($isVirtual)) { $pageClass .= ' is-congreso-virtual'; }
@endphp
@if (FilterHelper::_isSessiontype())
    @php
        $sessiontype = $session->getSessiontype($sessiontypes);
        $color = (!empty($sessiontype)) ? $sessiontype->getColor(true) : "";
        $pageClass .= ' is-' . $color;
    @endphp
@else
    @php
        $area = $session->getArea($areas);
        $color = (!empty($area)) ? $area->getColor(true) : "";
        $pageClass .= ' is-' . $color;
    @endphp
@endif
@section('page-class', $pageClass)

@section('content')
    <article>
        @include('session.header', ['backLink' => route('program.view')])

        <section class="section">
            <div class="container">
                @include('partials._message')
                <div id="video-summary-sessions">
                    @if ($isVirtual)
                        @include('session._video')
                    @else
                        @include('partials.virtual-platform-ad')
                    @endif
                    @include('session.details')
                    @include('session.paper-list')
                </div>
            </div>
        </div>
        @include('session.related-content')
        @include('partials._modal-share', ['textToShare' => $session->getName()])
        @include('partials._modal-another-agenda')
    </article>
@endsection
