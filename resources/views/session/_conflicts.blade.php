@if (
        (SettingHelper::isApp() && setting('web.conflicto_visible_app')) ||
        (SettingHelper::isAppPV() && setting('web.conflicto_visible_apppv')) ||
        (SettingHelper::isPV() && setting('web.conflicto_visible_pv'))
)
    @php
    $conflicts = $paper->getConflicts();
    @endphp
    @if (!empty($conflicts))
        <div class="conflictos">
            <p class="conflictosTitulo">{{__("Declaración conflicto de intereses")}} @svg('ico-down', 'aspa')</p>
            <ul class="conflictosListado" style="display: none">
                @foreach ($conflicts as $c)
                    <li>{{ $c }}</li>
                @endforeach
            </ul>
        </div>
    @endif
@endif