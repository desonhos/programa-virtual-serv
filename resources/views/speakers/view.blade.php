@php $sessions = $speaker->getSessions(); @endphp

@extends('layouts.app')

@section('css')
    @include('partials.dynamic-styles')
@endsection

@section('js')
    <script
      src="https://code.jquery.com/jquery-2.2.4.min.js"
      integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44="
      crossorigin="anonymous"></script>
    <script src="{{ asset('vendor/jquery-skeduler/jquery.skeduler.js') }}" type="text/javascript" defer></script>
    <script src="{{ mix('js/pages/program.js') }}" defer></script>
    <script src="{{ asset('js/pages/programSchemeView.js') }}" type="text/javascript" defer></script>
@endsection

@section('page-class', 'page-program is-agenda')

@section('content')
    <article>
        <section class="section section-sessions">
            <div class="container">
                <div class="is-flex is-justify-content-space-between">
                    <div class="speakerInfo">
                        @php $img = $speaker->getImageUrl(); @endphp
                        @if ($img)
                            <img src="{{ $img }}" onerror="$(this).hide()"/>
                        @endif
                        <div>
                            <h1 class="title is-uppercase @if (!empty($speaker->getCenter())) mb-3 @endif">
                                {{ $speaker->getSurname() }}, {{ $speaker->getName() }}
                            </h1>
                            @if (!empty($speaker->getCenter()))
                                <p class="mb-5">{{ $speaker->getCenter() }}</p>
                            @endif
                        </div>
                    </div>
                    @include('speakers._actions')
                </div>
                @if (count($sessions) == 0)
                    <div class="mt-2">
                        {{__("No hay sesiones registradas para este ponente.")}}
                    </div>
                    <a href="{{ route('program.view') }}" class="button mt-4">{{__("Ver programa")}}</a>
                @else
                    @include('program._sessions-table', ['agenda' => true])
                @endif
            </div>
        </section>
    </article>
    @include('partials._modal-share', ['textToShare' => $speaker->getSurname() . ", " . $speaker->getName()])
@endsection
