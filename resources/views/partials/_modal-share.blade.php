<div class="modal alert" id="sharePageModal">
    <div class="modal-background"></div>
    <div class="modal-card">
        <header class="modal-card-head">
            <p class="modal-card-title is-flex is-align-items-center mb-0 is-size-5">
                {{ __("Compartir Programa") }} {{ config('web.event.shortName') }}
            </p>
            {{-- <button class="delete" aria-label="close"></button> --}}
        </header>
        <section class="modal-card-body">
            <div class="is-flex is-justify-content-space-between">
                @php
                    $subject = "Programa%20" . str_replace("+", " ", urlencode(config('web.event.shortName')));
                    $currentUrl = Request::url();
                    $body = str_replace("+", " ", urlencode($textToShare));
                    $currentUrlEncoded = str_replace("+", " ", urlencode($currentUrl));
                @endphp
                <a href="mailto:?subject={{ $subject }}&body={{ $body . " - " . $currentUrlEncoded }}"
                    class="is-flex is-flex-direction-column is-align-items-center mr-2">
                    @svg('ico-comparte-mail', 'ico-comparte-mail')
                    <span class="has-text-white is-size-7 pt-1">{{ __("Correo") }}</span>
                </a>
                <a href="http://www.facebook.com/sharer.php?u={{ $currentUrlEncoded }}"
                    target="_blank"
                    class="is-flex is-flex-direction-column is-align-items-center mr-2">
                    @svg('ico-comparte-fb', 'ico-comparte-fb')
                    <span class="has-text-white is-size-7 pt-1">Facebook</span>
                </a>
                <a href="whatsapp://send?text={{ $currentUrlEncoded }}" data-action="share/whatsapp/share"
                    class="is-flex is-flex-direction-column is-align-items-center mr-2">
                    <span style="position: relative;top: -3px;">@svg('ico-comparte-was', 'ico-comparte-was')</span>
                    <span class="has-text-white is-size-7 pt-1" style="margin-top: -13px;">Whatsapp</span>
                </a>
                {{-- <a href="https://www.linkedin.com/shareArticle?mini=true&url={{ $currentUrlEncoded }}&title={{ $body }}"
                    target="_blank"
                    class="is-flex is-flex-direction-column is-align-items-center mr-2">
                    @svg('ico-comparte-in', 'ico-comparte-in')
                    <span class="has-text-white is-size-7">Linkedin</span>
                </a> --}}
                <a href="http://twitter.com/share?text={{ $body }}&url={{ $currentUrlEncoded }}"
                    target="_blank"
                    class="is-flex is-flex-direction-column is-align-items-center mr-2 shareBtn">
                    @svg('ico-comparte-x', 'ico-comparte-x')
                    <span class="has-text-white is-size-7 pt-1">X</span>
                </a>
            </div>
        </section>
        <footer class="modal-card-foot is-flex is-justify-content-space-between">
            <a class="close button is-warning is-link letter-spacing-0 has-border-0 pl-0 has-text-white">{{ __("Cancelar") }}</a>
        </footer>
    </div>
</div>
