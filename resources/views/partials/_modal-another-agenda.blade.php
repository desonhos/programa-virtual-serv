{{-- <div class="modal alert anotherAgendaModal" id="anotherAgendaModal_{{ $session->getId() }}"> --}}
    <div class="modal alert anotherAgendaModal" id="anotherAgendaModal">
    
        <div class="modal-background"></div>
        <div class="modal-card">
            <header class="modal-card-head">
                <p class="modal-card-title is-flex is-align-items-center mb-0 is-size-5">
                    {{ __("Sesión añadida a agenda") }} de {{ config('web.event.shortName') }}
                </p>
                {{-- <button class="delete" aria-label="close"></button> --}}
            </header>
            <section class="modal-card-body">
                <div class="is-flex is-flex-direction-column">
                    <p class="mb-5">{{ __("Si lo deseas, puedes añadir esta sesión también a otras agendas") }}</p>
                    <div class="is-flex is-justify-content-space-between">
                        {{-- <a href="{{ $session->getGoogleCalendarLink() }}" target="_blank" --}}
                        <a href="#" target="_blank"
                            class="is-flex is-flex-direction-column is-align-items-center mr-2 link-google-calendar">
                            @svg('ico-google-calendar', 'ico-google-calendar')
                            <span class="has-text-white is-size-7 pt-1">{{ __("Google Calendar") }}</span>
                        </a>
                        {{-- <a href="{{ $session->getOutlookCalendarLink() }}" target="_blank" --}}
                        <a href="#" target="_blank"
                            class="is-flex is-flex-direction-column is-align-items-center mr-2 link-outlook-calendar">
                            @svg('ico-outlook', 'ico-outlook')
                            <span class="has-text-white is-size-7 pt-1">{{ __("Outlook") }}</span>
                        </a>
                        {{-- <a href="{{ $session->getAppleCalendarLink() }}" target="_blank" --}}
                        <a href="#" target="_blank"
                            class="is-flex is-flex-direction-column is-align-items-center mr-2 link-apple-calendar">
                            @svg('ico-apple-calendar', 'ico-apple-calendar')
                            <span class="has-text-white is-size-7 pt-1">{{ __("Apple Calendar") }}</span>
                        </a>
                    </div>
                </div>
            </section>
            <footer class="modal-card-foot is-flex is-justify-content-space-between">
                <a class="close button is-warning is-link letter-spacing-0 has-border-0 pl-0 has-text-white">{{ __("Cerrar") }}</a>
            </footer>
        </div>
    </div>