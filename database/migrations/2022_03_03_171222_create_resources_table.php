<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateResourcesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('resources', function (Blueprint $table) {
            $table->increments('id');
            $table->string('file');
            $table->string('name', 64)->nullable();
            $table->enum('ico', [
                'DESCARGAR',
                'PDF-CERTIFICADO-ASIST',
                'PDF-DOCUMENTOS',
                'PDF-LIBRO',
                'VIDEO'
            ]);
            $table->integer('pos')->default(99);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('resources');
    }
}
