<nav class="navbar is-fixed-top container is-fluid @if (config('web.topbarDark')) is-tbdark @endif"
    role="navigation" aria-label="main navigation"
>
    {{-- Volver atrás --}}
    <a href="#" class="ico-back-link">
        @svg('ico-back', 'ico-back')
    </a>
    <script>
        document.addEventListener('DOMContentLoaded', function() {
            if (!document.querySelector('iframe')) {
                document.querySelector('.ico-back-link').addEventListener('click', function(ev) {
                    // va a la anterior URL, a menos que estemos en info, en cuyo caso el botón atrás lleva siempre a la botonera de inicio
                    if ($(".page-inicio-webapp.page-info").length) {
                        ev.preventDefault();
                        window.location = "{{ route('inicio-webapp') }}";
                    }
                    else {
                        ev.preventDefault();
                        history.go(-1);
                    }
                });
            }
            else {
                // Cuando hay un iframe, guarda la URL desde la que se llegó aquí para que cuando se pulse 
                // el botón de volver atrás se vaya a esa y no a la que venga del historial, que podría estar
                // incluyedo el historial de navegación que se hizo dentro del iframe
                var referrer = document.referrer;
                var isExternal = false;

                if (referrer) {
                    var referrerURL = new URL(referrer);
                    isExternal = referrerURL.hostname !== window.location.hostname;
                }

                document.querySelector('.ico-back-link').addEventListener('click', function(ev) {
                    ev.preventDefault();
                    if (referrer && !isExternal) {
                        window.location = referrer;
                    } else {
                        window.history.back();
                    }
                });
            }
        });
    </script>

    {{-- Logo del congreso --}}
    <a class="logos" href="{{ route('inicio-webapp') }}">
        @svg('topbar-logo-congreso', 'logo-congreso')
        <span class="has-text-weight-bold" id="webTitle">
            {{ \SettingHelper::getNombreCategoriaPlataforma() }}
        </span>
        {{-- <img class="logo-header-sociedades" src="/event/logo-sociedad.png"> --}}
        @svg('topbar-logo-sociedades', 'logo-header-sociedades')
    </a>

    <div class="is-flex">
        @if (\Auth::guard('multi')->user())
            {{-- Menú de usuario --}}
            <a role="button" class="navbar-burger is-usermenu burger ml-auto is-flex is-align-items-center is-justify-content-center" aria-label="menu" aria-expanded="false" data-target="navbarBasicExample">
                @svg('ico-usuario', 'ico-usuario')
            </a>
            <div class="navbar-menu is-usermenu">
                @include('partials._navbar-usermenuitems')
            </div>
        @else
            @if (!\SettingHelper::isAntesInicioVirtual())
                <a href="{{ route('login') }}" role="button" class="navbar-burger is-usermenu burger ml-auto is-flex is-align-items-center is-justify-content-center">
                    @svg('ico-usuario', 'ico-usuario')
                </a>
            @endif
        @endif

        {{-- Menú hamburguesa --}}
        <a role="button" class="navbar-burger burger is-mainmenu noDynamicStyles" aria-label="menu" aria-expanded="false" data-target="navbarBasicExample">
            <span aria-hidden="true"></span>
            <span aria-hidden="true"></span>
            <span aria-hidden="true"></span>
        </a>
        <div class="navbar-menu is-mainmenu">
            @include('partials._menu')
        </div>
    </div>
</nav>
