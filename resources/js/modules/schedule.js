import Alert from '@vizuaalog/bulmajs/src/plugins/alert';

$(document).ready(function () {
    // botones de añadir/quitar de la agenda
    setScheduleHandlers();

    $("#action-bar").removeClass('is-transparent');

    // Cierra al pulsar en cancelar
    $(".anotherAgendaModal .close.button").click(function () {
        $(".anotherAgendaModal").removeClass('is-active');
    });
});

/* Manejadores de los botones de añadir y quitar de la agenda */
export function setScheduleHandlers() {
    setOnAddToSchedule();
    setOnRemoveFromSchedule();
}
function setOnAddToSchedule() {
    $(".agendar").unbind("click");
    $(".agendar").click(function (ev) {
        ev.preventDefault();
        var $ico = $(this);
        var url = $ico.attr('href');
        var sessionId = url.substring(url.lastIndexOf('/') + 1);
        $.ajax({
            type: "GET",
            // contentType: "application/json",
            url: url,
            // data: form.serialize(),
            // dataType: 'JSON',
            complete: function (data) {
                if (eval(data.responseText)) {
                    if (data.responseText == 2) {
                        Alert().alert({
                            type: 'warning',
                            title: 'Conflicto',
                            body: 'Esta sesión se solapa con otra añadida en tu agenda',
                            confirm: {
                                label: 'Añadir a mi agenda de todos modos',
                                classes: ['is-link', 'pl-0', 'letter-spacing-0', 'has-border-0'],
                                onClick: function () {
                                    confirmAddToSchedule(url, $ico);
                                    showAddAnotherAgenda(sessionId);
                                }
                            },
                            cancel: {
                                label: 'Cancelar'
                                // onClick: function() { alert("no mete nada"); }
                            }
                        });
                    }
                    else {
                        showAddAnotherAgenda(sessionId);
                        $ico.hide();
                        var $agendada = $ico.parent().find('.agendada');
                        $agendada.find('.ico-agendada').removeClass('is-hidden');
                        $agendada.find('.ico-agenda-conflicto').addClass('is-hidden');
                        $agendada.show();
                    }
                }
            }
        });
    })
}
function showAddAnotherAgenda(sessionId) {
    // Obtiene por AJAX los valores de los enlaces (no se ponen directamente porque en la vista del programa tardaría mucho en cargar al tener que generar la URL para cada uno de los elementos
    $.ajax({
        type: "GET",
        url: "/sesiones/obtener-urls-calendar/" + sessionId,
        complete: function(data) {
            var response = JSON.parse(data.responseText);
            if ((typeof response['google-calendar'] !== 'undefined') && (response['google-calendar'] !== false)) {
                $("#anotherAgendaModal .link-google-calendar").attr('href', response['google-calendar']);
            }
            if ((typeof response['outlook-calendar'] !== 'undefined') && (response['outlook-calendar'] !== false)) {
                $("#anotherAgendaModal .link-outlook-calendar").attr('href', response['outlook-calendar']);
            }
            if ((typeof response['apple-calendar'] !== 'undefined') && (response['apple-calendar'] !== false)) {
                $("#anotherAgendaModal .link-apple-calendar").attr('href', response['apple-calendar']);
            }
            if ((response['google-calendar'] !== false) || (response['outlook-calendar'] !== false) || (response['apple-calendar'] !== false)) {
                $("#anotherAgendaModal").addClass('is-active');
            }
        }
    })    
}
function confirmAddToSchedule(url, $ico) {
    $.ajax({
        type: "GET",
        url: url,
        data: {
            'ignoreOverlap': true
        },
        // dataType: 'JSON',
        complete: function (data) {
            if (eval(data.responseText)) {
                $ico.hide();
                var $agendada = $ico.parent().find('.agendada');
                $agendada.find('.ico-agendada').addClass('is-hidden');
                $agendada.find('.ico-agenda-conflicto').removeClass('is-hidden');
                $agendada.show();
            }
        }
    });
}
function setOnRemoveFromSchedule() {
    $(".agendada").unbind("click");
    $(".agendada").click(function (ev) {
        ev.preventDefault();
        var $ico = $(this);
        var url = $ico.attr('href');
        $.ajax({
            type: "GET",
            // contentType: "application/json",
            url: url,
            // data: form.serialize(),
            // dataType: 'JSON',
            complete: function (data) {
                if (eval(data.responseText)) {
                    $ico.hide();
                    $ico.parent().find('.agendar').show();
                    reevaluateOverlappingSessions();
                }
            }
        });
    });
}
function reevaluateOverlappingSessions() {
    $.ajax({
        type: "GET",
        url: reevaluateOverlappingSessionsUrl,
    });
}
