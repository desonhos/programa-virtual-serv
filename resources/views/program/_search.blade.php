<form action="{{ route('program.search') }}" method="GET" role="search" id="programSearch" class="pr-4">
    <div class="dropdown" id="dropdownFilter">
        <span>
            {{-- @svg('filtrar', 'ico-filtrar') --}}
            @svg('ico-filtrar', 'ico-filtrar')
            {{-- @svg('ico-filtrar', ['class' => 'ico-filtrar']) --}}
             {{__("Filtro")}}</span>
        <div class="dropdown-content"></div>
    </div>

    <div class="select" id="searchSelectArea">
        @if (FilterHelper::_isArea() && !empty($areas))
            <select name="area">
                <option selected="true" disabled="disabled">{{__("Seleccionar")}}</option>
                <option value="">{{__("Todas las categorías")}}</option>
                @foreach ($areas as $area)
                    <option value="{{ $area->getId() }}" @if (!empty($oldRequest['area']) && $oldRequest['area'] == $area->getId()) selected @endif>{{ $area->getName() }}</option>
                @endforeach
            </select>
        @endif
        @if (FilterHelper::_isSessiontype() && !empty($sessiontypes))
            <select name="tipo_sesion">
                <option selected="true" disabled="disabled">{{__("Tipo de sesión")}}</option>
                <option value="">{{__("Todos los tipos")}}</option>
                @foreach ($sessiontypes as $sessiontype)
                    <option value="{{ $sessiontype->getId() }}" @if (!empty($oldRequest['tipo_sesion']) && $oldRequest['tipo_sesion'] == $sessiontype->getId()) selected @endif>{{ $sessiontype->getName() }}</option>
                @endforeach
            </select>
        @endif
    </div>

    <div class="search" id="searchTextField">
        <div class="control has-icons-right">
            <input class="input" type="text" placeholder="{{ __("Buscar") }}" name="texto" @if (!empty($oldRequest['texto'])) value="{{ $oldRequest['texto'] }}" @endif>
            <span class="icon is-small is-right on-click-submit">
                @svg('ico-search')
            </span>
        </div>
        <div class="has-text-right is-hidden-tablet" id="contenedorCerrarFiltro">
            <a class="button is-plain is-size-7" id="close-dropdownFilter">{{__("Cerrar")}}</a>
        </div>
    </div>

    <a class="button is-plain has-text-weight-normal" id="clear-filter">{{__("Borrar filtros")}}</a>
</form>
