<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;

class PVUser extends Authenticatable {

    protected $connection= 'cyex_online';
    protected $table = "pv_users";
    public const ACCESS_TYPE_A = 'A';
    public const ACCESS_TYPE_V = 'V';
    public const ACCESS_TYPE_P = 'P';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id_in_event', 'event_id', 'name', 'surname', 'surname2',
    ];

    public static function _findByEmail($email) {
        return PVUser::where('event_id', config('web.eventId'))
                    ->where('email', $email)->first();
    }


    /************************** GETTERS ***************************************/
    public function getId() { return $this->id; }
    public function getName() { return $this->name; }
    public function getSurname() { return $this->surname; }
    public function getSurname2() { return $this->surname2; }
    public function getEventId() { return $this->event_id; }
    public function getIdInEvent() { return $this->id_in_event; }
    public function getPassword() { return $this->password; }
    public function getEmail() { return $this->email; }
    public function getDefaultPassword() { return $this->default_password; }
    public function getAccessType() { return $this->access_type; }

    public function getRestrictedAllowedSessionsIds() {
        $restrictedAllowedIds = [];
        for ($i = 1; $i < 15; $i++) {
            if (!empty($this->{'restricted_session_allowed' . $i})) {
                array_push($restrictedAllowedIds, $this->{'restricted_session_allowed' . $i});
            }
        }
        return $restrictedAllowedIds;
    }

    public function getIdForSurvey() {
        return $this->getIdInEvent() * 7;
    }

    public function getCompleteName() {
        $name = $this->getName();
        $surname = $this->getSurname();
        $surname2 = $this->getSurname2();
        $res = '';
        if (!empty($name)) { $res = $name; }
        if (!empty($surname)) { $res .= " " . $surname; }
        if (!empty($surname2)) { $res .= " " . $surname2; }
        return trim($res);
    }

    public function getRestrictedProgramLetter() {
        return $this->restricted_program_letter;
    }

    public function getSurveySessionlistUrl() {
        return $this->survey_sessionlist_url;
    }

    public function getSurveyCongressUrl() {
        return $this->survey_congress_url;
    }
    
    /************************ SETTERS ******************************************/
    public function setDefaultPassword($defaultPassword) {
        $this->default_password = $defaultPassword;
    }
    public function setPassword($password) {
        $this->password = bcrypt($password);
    }
    /**************/

    /** ¿Usuario con acceso solo presencial? */
    public function isP() {
        return $this->getAccessType() == PVUser::ACCESS_TYPE_P;
    }
    /** ¿Usuario con acceso virtual? */
    public function isV() {
        return $this->getAccessType() == PVUser::ACCESS_TYPE_V;
    }
    /** ¿Usuario con acceso presencial y virtual? */
    public function isA() {
        return $this->getAccessType() == PVUser::ACCESS_TYPE_A;
    }

    /** Devuelve si el usuario puede evaluar sesiones y congreso o no.
     * Podrá si estamos después del comienzo del congreso y antes del
     * limit_date establecido en sur_infoevents */
    public function canEvaluate() {
        $inicioAppPV = setting('web.fecha_inicio_apppv');
        $inicioPV = setting('web.fecha_inicio_pv');
        if (empty($inicioAppPV) && empty($inicioPV)) { return false; }
        if (empty($inicioAppPV)) { $inicioAppPV = '2999-01-01 00:00:00'; }
        if (empty($inicioPV)) { $inicioPV = '2999-01-01 00:00:00'; }
        $fechaInicio = min(setting('web.fecha_inicio_apppv'), setting('web.fecha_inicio_pv'));
        if (!\DateHelper::isDatetimeInPast($fechaInicio)) { return false; }

        $surInfoevent = SurInfoevent::_getCurrent();
        if (empty($surInfoevent)) { return true; }
        $limitDate = $surInfoevent->getLimitDate();
        if (empty($limitDate)) { return true; }
        
        return !\DateHelper::isDatetimeInPast($limitDate);
    }
}
