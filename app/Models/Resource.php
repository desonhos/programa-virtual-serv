<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Resource extends Model {
    public static function _get($resourceId) {
        return Resource::where('id', $resourceId)->first();
    }

    public static function _getAll() {
        return Resource::orderBy('pos')->get();
    }

    public function getName() {
        if (!empty($this->name)) {
            return $this->name;
        }
        $file = json_decode($this->file);
        if ($file) {
            return $file[0]->original_name;
        }
        return false;
    }
    public function getIco() { return 'ico-cartera-' . strtolower($this->ico); }
    public function getDocumentUrl() {
        $file = json_decode($this->file);
        if ($file) {
            return Storage::disk(config('voyager.storage.disk'))->url($file[0]->download_link);
        }
        return $this->file;
    }

}
