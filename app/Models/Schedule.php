<?php

namespace App\Models;
use Illuminate\Support\Facades\Cookie;

// TODO: hacer esto un ServiceProvider para que solo se pueda instanciar una vez
class Schedule {

    protected $connection= 'cyex_online';
    private $cookieName = 'agenda';
    private $cookieExpirationTime = 525600; // un año (se ponen minutos)

    /** Añade una sesión a la agenda. ¿Cuál? - Aquella cuyo ID se pasa por parámetro */
    function addSession($sessionId, $overlaps = null) {
        if ($this->isSessionInSchedule($sessionId)) { return true; }
        $schedule = json_decode(Cookie::get($this->cookieName), true);
        if (empty($schedule['sessions'])) {
            $schedule['sessions'] = array($sessionId);
        } else {
            array_push($schedule['sessions'], $sessionId);
        }
        if ($overlaps) {
            if (empty($schedule['overlappingSessions'])) {
                $schedule['overlappingSessions'] = array($sessionId);
            } else {
                array_push($schedule['overlappingSessions'], $sessionId);
            }
        }
        Cookie::queue(Cookie::make($this->cookieName, json_encode($schedule), $this->cookieExpirationTime));
    }

    /** Borra una sesión de la agenda. ¿Cuál? - Aquella cuyo ID se pasa por parámetro */
    function removeSession($sessionId) {
        if (!$this->isSessionInSchedule($sessionId)) { return true; }
        $schedule = json_decode(Cookie::get($this->cookieName), true);
        if (empty($schedule['sessions'])) { return true; }
        array_splice($schedule['sessions'], array_search($sessionId, $schedule['sessions']), 1);
        if (!empty($schedule['overlappingSessions'])) {
            array_splice($schedule['overlappingSessions'], array_search($sessionId, $schedule['overlappingSessions']), 1);
        }
        Cookie::queue(Cookie::make($this->cookieName, json_encode($schedule), $this->cookieExpirationTime));
    }


    function addPaper() {}
    function removePaper() {}

    function reevaluateOverlappingSessions() {
        $schedule = json_decode(Cookie::get($this->cookieName), true);
        $schedule['overlappingSessions'] = [];
        if (empty($schedule['sessions'])) {
            Cookie::queue(Cookie::make($this->cookieName, json_encode($schedule), $this->cookieExpirationTime));
            return true;
        }
        $sessions = $schedule['sessions'];
        foreach ($sessions as $sessionId) {
            if ($this->isOverlappingSession($sessionId)) {
                array_push($schedule['overlappingSessions'], $sessionId);
            }
        }
        Cookie::queue(Cookie::make($this->cookieName, json_encode($schedule), $this->cookieExpirationTime));
    }

    function isSessionInSchedule($sessionId) {
        $schedule = Cookie::get($this->cookieName);
        $schedule = json_decode($schedule, true);
        if (
            empty($schedule) ||
            (!isset($schedule['sessions']))
        ) {
            return null;
        }
        foreach ($schedule['sessions'] as $session) {
            if ($session == $sessionId) {
                return true;
            }
        }
        return false;
    }
    function isPaperInSchedule() {}

    /** Devuelve si la sesión cuyo id se pasa por parámetro se solapa con
     * algunas de las ya existentes en la agenda */
    function isOverlappingSession($sessionId) {
        $schedule = json_decode(Cookie::get($this->cookieName), true);
        if (empty($schedule['sessions'])) { return false; }
        foreach ($schedule['sessions'] as $session) {
            $session = Session::_get($session);
            if ($session->isOverlapping($sessionId)) { return true; }
        }
        return false;
    }

    /** Devuelve si la sesión cuyo id se pasa por parámetro fue marcada como que
     * solapaba con otra a la hora de insertarla en la agenda */
    function isSetAsOverlapping($sessionId) {
        $schedule = Cookie::get($this->cookieName);
        $schedule = json_decode($schedule, true);
        if (
            empty($schedule) ||
            (!isset($schedule['overlappingSessions']))
        ) {
            return null;
        }
        foreach ($schedule['overlappingSessions'] as $session) {
            if ($session == $sessionId) {
                return true;
            }
        }
        return false;
    }

    /** Devuelve el contenido completo de la agenda */
    function get() {
        $schedule = Cookie::get($this->cookieName);
        if (empty($schedule)) {
            return null;
        }
        return json_decode($schedule, true);
    }

}
