@php
$vroom = App\Models\VRoom::find($dataTypeContent->id);
@endphp
<div class="contenedor-imagen">
    <img src="{{ Voyager::image($dataTypeContent->background) }}" class="imagen"
    {{-- style="max-width: 100%;" --}}
    {{-- TODO: overflow scroll en el contenedor imagen y luego en los svg poner overflow visible --}}
    >
</div>
<div class="formulario-areas">
    <input type="button" class="btn-anadir-area" value="Añadir área" onclick="nuevaArea()">
</div>
{{-- <input type="button" class="btn-guardar-map" value="Guardar áreas" onclick="generarJSONMap()"> --}}

@section('javascript')
    @parent

    {{-- Ricardo --}}
    <script>
        var offsetX = $(".imagen").offset().left;
        var offsetY = $(".imagen").offset().top;
        var contadorAreas = -1;
        var elementoActivo = 0;
        var areas = new Array();
        var elementoActivo = 0;
        var timeout;


        function Area(id, inicioX, inicioY, finX, finY, nombre, url, target, grupo){
            this.id = id;
            this.inicioX = inicioX;
            this.inicioY = inicioY;
            this.finX =finX;
            this.finY =finY;
            this.nombre = nombre;
            this.url = url;
            this.target = target;
            this.grupo = grupo;
        }

        $('.formulario-areas').change(function(){
            elementoActivo = $("input[type='radio'][name='area']:checked").val();
            console.log(elementoActivo);
            for(i = 0; i <= areas.length - 1; i++){dibujarArea(areas[i].id)}
        });

        function nuevaArea(){
            contadorAreas += 1;
            var id = "area-" + contadorAreas;
            var idRadio = "radio-" + contadorAreas;
            var idSVG = "svg-" + contadorAreas;
            var idRect = "rect-" + contadorAreas;

            elementoActivo = contadorAreas;
            for(i = 0; i <= areas.length - 1; i++){dibujarArea(areas[i].id)}

            $(".formulario-areas").append('<div class="input-area" id="'+id+'" data-area-index="'+contadorAreas+'"></div>');
            $("#"+id).append('<input type="radio" id="'+idRadio+'" name="area" checked="checked" value="'+contadorAreas+'">');
            $("#"+id).append('<input type="text" class="X0">');
            $("#"+id).append('<input type="text" class="Y0">');
            $("#"+id).append('<input type="text" class="X">');
            $("#"+id).append('<input type="text" class="Y">');
            $("#"+id).append('<span class="maplabel">Nombre:</span><input type="text" class="nombre">');
            $("#"+id).append('<span class="maplabel">Tipo:</span><select class="target" name="target"><option value="ext">Enlace externo</option><option value="int">Enlace interno</option><option value="video">Vídeo</option><option value="doc">Documento</option></select>');
            $("#"+id).append('<span class="maplabel">Url:</span><input type="text" class="URL">');
            $("#"+id).append('<span class="maplabel">Grupo:</span><select class="grupo" name="grupo"><option value="">Sin grupo</option><option value="1">1</option><option value="2">2</option><option value="3">3</option><option value="4">4</option><option value="5">5</option><option value="6">6</option><option value="7">7</option><option value="8">8</option><option value="9">9</option><option value="10">10</option></select>');
            $("#"+id).append('<input type="button" class="btn-eliminar-area" value="X" onclick="eliminarArea('+contadorAreas+')">');
            $(".contenedor-imagen").append('<svg class="svg-map" id="'+idSVG+'" onClick="definirArea()"><rect id="'+idRect+'"></rect></svg>');

            area = new Area(contadorAreas, -1, -1, -1, -1);
            areas.push(area);
        }

        function definirArea(){
            elementoActivo = $("input[type='radio'][name='area']:checked").val();

            var indice = areas.findIndex(element => element.id == elementoActivo);

            offsetX = $(".imagen").offset().left;
            offsetY = $(".imagen").offset().top;

            if(areas[indice].inicioX == -1 && areas[indice].inicioY == -1){
                areas[indice].inicioX = event.clientX - offsetX + $(document).scrollLeft();
                areas[indice].inicioY = event.clientY - offsetY + $(document).scrollTop();
            }else if(areas[indice].finX == -1 && areas[indice].finY == -1){
                areas[indice].finX = event.clientX - offsetX + $(document).scrollLeft();
                areas[indice].finY = event.clientY - offsetY + $(document).scrollTop();
                dibujarArea(elementoActivo)
            }

        }

        function dibujarArea(id){
            var indice = areas.findIndex(element => element.id == id);

            rectX = areas[indice].inicioX < areas[indice].finX ? areas[indice].inicioX : areas[indice].finX;
            rectY = areas[indice].inicioY < areas[indice].finY ? areas[indice].inicioY : areas[indice].finY;
            rectWidth = areas[indice].finX > areas[indice].inicioX ? areas[indice].finX - areas[indice].inicioX : areas[indice].inicioX -  areas[indice].finX;
            rectHeight = areas[indice].finY > areas[indice].inicioY ? areas[indice].finY - areas[indice].inicioY : areas[indice].inicioY -  areas[indice].finY;

            rectElement = '<rect id="rect-'+id+'" data-area-index="'+id+'" x="'+rectX+'" y="'+rectY +'" width="'+rectWidth+'" height="'+rectHeight+'" style="fill:rgb(200,200,200);stroke-width:3;stroke:rgb(0,0,0);opacity:0.6;" ></rect>';
            tiradorUnoElemento = '<circle id="'+id+'a'+'" class="tirador" data-area-index="'+id+'" cx="'+areas[indice].inicioX+'" cy="'+areas[indice].inicioY+'" r="10" style="position:absolute;" onmousedown="moverTirador('+id+',0'+')"/>';
            tiradorDosElemento = '<circle id="'+id+'b'+'" class="tirador" data-area-index="'+id+'" cx="'+areas[indice].finX+'" cy="'+areas[indice].finY+'" r="10" style="position:absolute;" onmousedown="moverTirador('+id+',1'+')"/>';

            $("#svg-"+id).remove();
            if(id == elementoActivo){
                //console.log("dibujando elemento activo")
                $(".contenedor-imagen").append('<svg class="svg-map" id="svg-'+id+'"  onClick="definirArea()" style="pointer-events: auto;">'+rectElement+tiradorUnoElemento+tiradorDosElemento+'</svg>');
            }else{
                //console.log("dibujando elemento NO activo")
                $(".contenedor-imagen").append('<svg class="svg-map" id="svg-'+id+'"  onClick="definirArea()" style="pointer-events: none;">'+rectElement+'</svg>');
            }

            //console.log(offsetX);
            //console.log(offsetY);

            rellenarFormulario(id);
        }

        function rellenarFormulario(id){
            var indice = areas.findIndex(element => element.id == id);

            $("#area-"+id).children(".X0").val(areas[indice].inicioX);
            $("#area-"+id).children(".Y0").val(areas[indice].inicioY);
            $("#area-"+id).children(".X").val(areas[indice].finX);
            $("#area-"+id).children(".Y").val(areas[indice].finY);
        }

        function moverTirador(id,corner){
            var indice = areas.findIndex(element => element.id == id);

            console.log(areas.findIndex(element => element.id == id));

            offsetX = $(".imagen").offset().left;
            offsetY = $(".imagen").offset().top;

            if(corner == 0){
                var currentMousePosX = areas[indice].inicioX;
                var currentMousePosY = areas[indice].inicioY;
            }else{
                var currentMousePosX = areas[indice].finX;
                var currentMousePosY = areas[indice].finY;
            }

            timeout = setInterval(function(){
                $(document).mousemove(function(event) {
                    currentMousePosX = event.pageX - offsetX;
                    currentMousePosY = event.pageY - offsetY;
                });

                if(corner == 0){
                    areas[indice].inicioX = currentMousePosX;
                    areas[indice].inicioY = currentMousePosY;
                }else{
                    areas[indice].finX = currentMousePosX;
                    areas[indice].finY = currentMousePosY;
                }
                dibujarArea(id);
            }, 100);

            return false;
        }

        $(document).mouseup(function(){
            clearInterval(timeout);
            return false;
        });

        //Eliminar area
        function eliminarArea(id){
            var idAreaEliminar = areas.findIndex(element => element.id == id);
            var ultimoElemento = areas[areas.length-1].id;

            areas.splice(idAreaEliminar, 1);
            $("#area-"+id).remove();
            $("#svg-"+id).remove();
            $("#svg-"+ultimoElemento).remove();
            for(i = 0; i <= areas.length - 1; i++){dibujarArea(areas[i].id)}
            console.log(areas);
        }

        //Guardar JSON
        function generarJSONMap(){
            addFormDataToAreas();
            var jsonData = JSON.stringify(areas);
            download(jsonData, 'json.txt', 'text/plain');
        }

        function addFormDataToAreas(){

            for(i = 0; i <= areas.length - 1; i++){
                var indice = areas[i].id;
                areas[i].url = $("#area-"+indice).children(".URL").val();
                areas[i].nombre = $("#area-"+indice).children(".nombre").val();
                areas[i].target = $("#area-"+indice).children(".target").val();
                areas[i].grupo = $("#area-"+indice).children(".grupo").val();
            }
        }

        function download(content, fileName, contentType) {
            var a = document.createElement("a");
            var file = new Blob([content], {type: contentType});
            a.href = URL.createObjectURL(file);
            a.download = fileName;
            a.click();
        }

        //Cargar JSON
        function loadFile() {
          var input, file, fr;

          if (typeof window.FileReader !== 'function') {
            alert("The file API isn't supported on this browser yet.");
            return;
          }

          input = document.getElementById('fileinput');
          if (!input) {
            alert("Um, couldn't find the fileinput element.");
          }
          else if (!input.files) {
            alert("Este navegador no soporta la importación de archivos.");
          }
          else if (!input.files[0]) {
            alert("Selecciona un archivo anter de 'Cargar'");
          }
          else {
            file = input.files[0];
            fr = new FileReader();
            fr.onload = receivedText;
            fr.readAsText(file);
          }

          function receivedText(e) {
            let lines = e.target.result;
            var areasJSON = JSON.parse(lines);
            cargarAreasJSON(areasJSON);
          }
        }

        function cargarAreasJSON(areasJSON){
            for(i = 0; i <= areasJSON.length - 1; i++){
                area = new Area(areasJSON[i].id, areasJSON[i].inicioX, areasJSON[i].inicioY, areasJSON[i].finX, areasJSON[i].finY, areasJSON[i].nombre, areasJSON[i].url, areasJSON[i].target, areasJSON[i].grupo);
                areas.push(area);

                var id = "area-" + area.id;
                var idRadio = "radio-" + area.id;
                var idSVG = "svg-" + area.id;
                var idRect = "rect-" + area.id;

                $(".formulario-areas").append('<div class="input-area" id="'+id+'" data-area-index="'+area.id+'"></div>');
                $("#"+id).append('<input type="radio" id="'+idRadio+'" name="area" checked="checked" value="'+area.id+'">');
                $("#"+id).append('<input type="text" class="X0">');
                $("#"+id).append('<input type="text" class="Y0">');
                $("#"+id).append('<input type="text" class="X">');
                $("#"+id).append('<input type="text" class="Y">');
                $("#"+id).append('<span class="maplabel">Nombre:</span><input type="text" class="nombre">');
                $("#"+id).append('<span class="maplabel">Tipo:</span><select class="target" name="target"><option value="ext">Enlace externo</option><option value="int">Enlace interno</option><option value="video">Vídeo</option><option value="doc">Documento</option></select>');
                $("#"+id).append('<span class="maplabel">Url:</span><input type="text" class="URL">');
                $("#"+id).append('<span class="maplabel">Grupo:</span><select class="grupo" name="grupo"><option value="">Sin grupo</option><option value="1">1</option><option value="2">2</option><option value="3">3</option><option value="4">4</option><option value="5">5</option><option value="6">6</option><option value="7">7</option><option value="8">8</option><option value="9">9</option><option value="10">10</option></select>');
                $("#"+id).append('<input type="button" class="btn-eliminar-area" value="X" onclick="eliminarArea('+area.id+')">');


                $("#"+id).children(".nombre").val(area.nombre);
                $("#"+id).children(".URL").val(area.url);
                $("#"+id).children(".target").val(area.target);
                $("#"+id).children(".grupo").val(area.grupo);

                dibujarArea(area.id);
            }

            contadorAreas = areas[areas.length-1].id;
        }

    </script>

    {{-- Más JS --}}
    <script>
        $(document).ready(function(){
            initAreas();
            onSubmitForm();
        });
        /** Inicializa las áreas a partir de la información que haya almacenada en la BD */
        function initAreas() {
            $("textarea[name=mapareas]").hide();
            var areasBD = '{!! $dataTypeContent->mapareas !!}';
            if (areasBD !== '') {
                var areasJSON = JSON.parse(areasBD);
                if (areasJSON.length) {
                    cargarAreasJSON(areasJSON);
                }
            }
        }
        function onSubmitForm() {
            $("form.form-edit-add").one('submit', function(ev) {
                ev.preventDefault();
                addFormDataToAreas();
                $("textarea[name=mapareas]").html(JSON.stringify(areas));
                $(this).submit();
            })
        }
    </script>
@stop

<style>
    .contenedor-imagen{
        position: relative;
        display: inline-block;
        /* margin: 6rem; */
    }

    .svg-map{
        position: absolute;
        top: 0;
        bottom: 0;
        left: 0;
        right: 0;
        height: 100%;
        width: 100%;
    }

    .input-area{
        display: block;
        margin-top: 1rem;
    }
</style>

<style>
    .cabeceras {
        margin-left: 13px;
    }
    .cabeceras span {
        width: 156px;
        font-weight: bold;
    }
    .maplabel { margin-right: .5rem; }
    input + .maplabel { margin-left: .5rem; }
    .btn-anadir-area { margin-top: 1rem; }
    .input-area .X0,
    .input-area .X,
    .input-area .Y0,
    .input-area .Y {
        display: none;
    }
</style>
