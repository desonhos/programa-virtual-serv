<div class="modal alert modalVideo" data-id="{{ $videoId }}">
    <div class="modal-background"></div>
    <div class="modal-card">
        <header class="modal-card-head">
            <p class="modal-card-title is-flex is-align-items-center mb-0 is-size-5">
                {{ $videoTitle }}
            </p>
        </header>
        <section class="modal-card-body">
            <div class="container content">
                @if (isYoutubeUrl($videoFile))
                    {!! getYoutubeIframeFromUrl($videoFile) !!}
                @else
                    <video controls="">
                        <source src="{{ $videoFile }}" type="video/mp4">
                    </video>
                @endif
            </div>
        </section>
        <footer class="modal-card-foot is-flex is-justify-content-space-between">
            <a class="close button is-warning is-link letter-spacing-0 has-border-0 pl-0 has-text-white">{{__("Cerrar") }}</a>
        </footer>
    </div>
</div>
