<ul class="links-list is-horizontal mt-0 mb-0" id="viewSwitcher">
    <li class="is-flex is-align-items-center">
        <a class="is-text-link" id="link-list">@svg('ico-modo-lista', 'ico-modo-lista')</a>
    </li>
    <li class="is-flex is-align-items-center ml-0">
        <a class="is-text-link" id="link-scheme">@svg('ico-modo-esquema', 'ico-modo-esquema')</a>
    </li>
</ul>
