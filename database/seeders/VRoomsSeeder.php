<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
use App\Vroom;

class VRoomsSeeder extends Seeder {

     public function run() {
         echo "\e[0;32;40mCreando:\e[0m salas\n";
         $this->importVRooms();
         echo "\e[0;33;40mCreadas:\e[0m salas\n";
     }

     protected function importVRooms() {
         $vroom = Vroom::firstOrCreate(['slug' => 'vestibulo'],
             [
                 'name' => 'Vestíbulo',
                 'slug' => 'vestibulo',
                 'background' => '/event/virtual/vestibulo.jpg',
                 'mapareas' => json_encode([
                     [
                         "id" => 0,
                         "inicioX" => 968,
                         "inicioY" => 245,
                         "finX" => 702,
                         "finY" => 92,
                         "url" => "https://web.congresosec.org",
                         "target" => "_blank",
                         "nombre" => "Congreso SEC"
                     ],
                     [
                         "id" => 1,
                         "inicioX" => 371,
                         "inicioY" => 168,
                         "finX" => 500,
                         "finY" => 217,
                         "url" => route('vroom.view', ['slug' => 'sala-sec']),
                         "target" => "",
                         "nombre" => "Sala SEC"
                     ],
                     [
                         "id" => 2,
                         "inicioX" => 367,
                         "inicioY" => 400,
                         "finX" => 522,
                         "finY" => 429,
                         "url" => route('vroom.view', ['slug' => 'info']),
                         "target" => "",
                         "nombre" => "Información"
                     ],
                     [
                         "id" => 3,
                         "inicioX" => 616,
                         "inicioY" => 435,
                         "finX" => 686,
                         "finY" => 583,
                         "url" => route('webapp.sponsors'),
                         "target" => "",
                         "nombre" => "Sponsors"
                     ],
                     [
                         "id" => 4,
                         "inicioX" => 731,
                         "inicioY" => 367,
                         "finX" => 947,
                         "finY" => 411,
                         "url" => route('vroom.view', ['slug' => 'exposicion']),
                         "target" => "",
                         "nombre" => "Exposición"
                     ],
                     [
                         "id" => 5,
                         "inicioX" => 1082,
                         "inicioY" => 95,
                         "finX" => 1200,
                         "finY" => 139,
                         "url" => route('vroom.view', ['slug' => 'sala-poster']),
                         "target" => "",
                         "nombre" => "Póster"
                     ],
                     [
                         "id" => 6,
                         "inicioX" => 1091,
                         "inicioY" => 387,
                         "finX" => 1257,
                         "finY" => 427,
                         "url" => route('vroom.view', ['slug' => 'auditorio']),
                         "target" => "",
                         "nombre" => "Auditorio"
                     ],
                 ]),
             ]
         );
         $vroom = Vroom::firstOrCreate(['slug' => 'auditorio'],
             [
                 'name' => 'Auditorio',
                 'slug' => 'auditorio',
                 'background' => '/event/virtual/auditorio.jpg',
                 'mapareas' => json_encode([
                     [
                         "id" => 0,
                         "inicioX" => 469,
                         "inicioY" => 166,
                         "finX" => 557,
                         "finY" => 255,
                         "url" => "https://secardiologia.es",
                         "target" => "_blank",
                         "nombre" => "Sociedad Española de Cardiología"
                     ],
                     [
                         "id" => 1,
                         "inicioX" => 467,
                         "inicioY" => 277,
                         "finX" => 555,
                         "finY" => 362,
                         "url" => route('vroom.view', ['slug' => 'vestibulo']),
                         "target" => "",
                         "nombre" => "Vestíbulo"
                     ],
                     [
                         "id" => 2,
                         "inicioX" => 653,
                         "inicioY" => 166,
                         "finX" => 772,
                         "finY" => 368,
                         "url" => route('virtual.program.view'),
                         "target" => "",
                         "nombre" => "Programa completo"
                     ],
                     [
                         "id" => 3,
                         "inicioX" => 790,
                         "inicioY" => 168,
                         "finX" => 907,
                         "finY" => 258,
                         "url" => route('virtual.program.view'),
                         "target" => "",
                         "nombre" => "Sesiones grupos de trabajo"
                     ],
                     [
                         "id" => 4,
                         "inicioX" => 787,
                         "inicioY" => 277,
                         "finX" => 907,
                         "finY" => 368,
                         "url" => route('virtual.program.view'),
                         "target" => "",
                         "nombre" => "Simposios Satélite"
                     ],
                     [
                         "id" => 5,
                         "inicioX" => 922,
                         "inicioY" => 166,
                         "finX" => 1040,
                         "finY" => 258,
                         "url" => route('virtual.program.view'),
                         "target" => "",
                         "nombre" => "Pósteres"
                     ],
                     [
                         "id" => 6,
                         "inicioX" => 922,
                         "inicioY" => 279,
                         "finX" => 1040,
                         "finY" => 368,
                         "url" => route('virtual.program.view'),
                         "target" => "",
                         "nombre" => "Comunicaciones Orales"
                     ],
                     [
                         "id" => 7,
                         "inicioX" => 1138,
                         "inicioY" => 168,
                         "finX" => 1224,
                         "finY" => 258,
                         "url" => "https://aedv.es/servicios-al-dermatologo/premios-congreso-aedv/",
                         "target" => "",
                         "nombre" => "Premios AEDV"
                     ],
                     [
                         "id" => 8,
                         "inicioX" => 1135,
                         "inicioY" => 276,
                         "finX" => 1226,
                         "finY" => 368,
                         "url" => route('virtual.exhibition'),
                         "target" => "",
                         "nombre" => "Expo"
                     ],
                 ]),
             ]
         );
         $vroom = Vroom::firstOrCreate(['slug' => 'exposicion'],
             [
                 'name' => 'Exposición',
                 'slug' => 'exposicion',
                 'background' => '/event/virtual/exposicion.jpg',
                 'mapareas' => json_encode([
                     [
                         "id" => 0,
                         "inicioX" => 344,
                         "inicioY" => 381,
                         "finX" => 499,
                         "finY" => 467,
                         "url" => route('vroom.view', ['slug' => 'abbvie']),
                         "target" => "",
                         "nombre" => "Abbvie"
                     ],
                     [
                         "id" => 1,
                         "inicioX" => 512,
                         "inicioY" => 383,
                         "finX" => 668,
                         "finY" => 469,
                         "url" => route('vroom.view', ['slug' => 'amgen']),
                         "target" => "",
                         "nombre" => "AMGEN"
                     ],
                     [
                         "id" => 2,
                         "inicioX" => 681,
                         "inicioY" => 383,
                         "finX" => 834,
                         "finY" => 466,
                         "url" => route('vroom.view', ['slug' => 'janssen']),
                         "target" => "",
                         "nombre" => "Janssen"
                     ],
                 ]),
             ]
         );
         $vroom = Vroom::firstOrCreate(['slug' => 'abbvie'],
             [
                 'name' => 'Abbvie',
                 'slug' => 'abbvie',
                 'background' => '/event/virtual/abbvie.jpg',
                 'legal' => '<p>Acepto que la información contenida en este stand está dirigida a personal profesional sanitario facultado para prescribir o dispensar medicamentos en España.</p><p>El titular de este stand es AbbVie Spain S.L.U. responsable del contenido y difusión del mismo, siguiendo las restricciones impuestas y de conformidad con la normativa aplicable</p>',
                 'legal_yes' => 'Acepto',
                 'legal_no' => 'No acepto',
                 'is_exhibitor' => 1
             ]
         );
         $vroom = Vroom::firstOrCreate(['slug' => 'amgen'],
             [
                 'name' => 'Amgen',
                 'slug' => 'amgen',
                 'background' => '/event/virtual/amgen.jpg',
                 'legal' => '<p>La información contenida en este sitio web está dirigida exclusivamente a profesionales sanitarios facultados para prescribir o dispensar medicamentos en España (requiere una formación especializada para su correcta interpretación).</p>',
                 'legal_yes' => 'Soy sanitario',
                 'legal_no' => 'No soy sanitario',
                 'is_exhibitor' => 1
             ]
         );
         $vroom = Vroom::firstOrCreate(['slug' => 'janssen'],
             [
                 'name' => 'Janssen',
                 'slug' => 'janssen',
                 'background' => '/event/virtual/janssen.jpg',
                 'legal' => '<p>Acepto que la información contenida en este stand está dirigida a personal profesional sanitario facultado para prescribir o dispensar medicamentos en España.</p><p>El titular de este stand es AbbVie Spain S.L.U. responsable del contenido y difusión del mismo, siguiendo las restricciones impuestas y de conformidad con la normativa aplicable</p>',
                 'legal_yes' => 'Acepto',
                 'legal_no' => 'No acepto',
                 'is_exhibitor' => 1
             ]
         );
         $vroom = Vroom::firstOrCreate(['slug' => 'lilly'],
             [
                 'name' => 'Lilly',
                 'slug' => 'lilly',
                 'background' => '/event/virtual/lilly.jpg',
                 'legal' => '<p>Acepto que la información contenida en este stand está dirigida a personal profesional sanitario facultado para prescribir o dispensar medicamentos en España.</p><p>El titular de este stand es AbbVie Spain S.L.U. responsable del contenido y difusión del mismo, siguiendo las restricciones impuestas y de conformidad con la normativa aplicable</p>',
                 'legal_yes' => 'Acepto',
                 'legal_no' => 'No acepto',
                 'is_exhibitor' => 1
             ]
         );
         $vroom = Vroom::firstOrCreate(['slug' => 'novartis'],
             [
                 'name' => 'Novartis',
                 'slug' => 'novartis',
                 'background' => '/event/virtual/novartis.jpg',
                 'legal' => '<p>Acepto que la información contenida en este stand está dirigida a personal profesional sanitario facultado para prescribir o dispensar medicamentos en España.</p><p>El titular de este stand es AbbVie Spain S.L.U. responsable del contenido y difusión del mismo, siguiendo las restricciones impuestas y de conformidad con la normativa aplicable</p>',
                 'legal_yes' => 'Acepto',
                 'legal_no' => 'No acepto',
                 'is_exhibitor' => 1
             ]
         );
         $vroom = Vroom::firstOrCreate(['slug' => 'sanofi'],
             [
                 'name' => 'Sanofi',
                 'slug' => 'sanofi',
                 'background' => '/event/virtual/sanofi.jpg',
                 'legal' => '<p>Acepto que la información contenida en este stand está dirigida a personal profesional sanitario facultado para prescribir o dispensar medicamentos en España.</p><p>El titular de este stand es AbbVie Spain S.L.U. responsable del contenido y difusión del mismo, siguiendo las restricciones impuestas y de conformidad con la normativa aplicable</p>',
                 'legal_yes' => 'Acepto',
                 'legal_no' => 'No acepto',
                 'is_exhibitor' => 1
             ]
         );
     }
}
