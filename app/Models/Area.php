<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Area extends Model {

    protected $connection= 'cyex_online';

    /** Obtiene todas las del evento actual */
    public static function _getAll() {
        $areas = Area::where('event_id', config('web.eventId'))
                    ->orderBy('name')
                    ->get();
        return $areas;
    }

    /** Busca en el listado de áreas, aquella cuyo id se pasa por parámetro */
    public static function _find($id, $areas) {
        foreach ($areas as $area) {
            if ($area->id == $id) {
                return $area;
            }
        }
        return null;
    }

    /** Obtiene el listado de estilos únicos */
    public static function _getColorlist($areas, $removeHashtag = false) {
        $colors = [];
        foreach ($areas as $area) {
            $color = $area->getColor($removeHashtag);
            if (!empty($color) && !in_array($color, $colors)) {
                array_push($colors, $color);
            }
        }
        return $colors;
    }

    public function getName() {
        return $this->name;
    }

    public function getNameAbbr() {
        $field = 'name_abbr';
        return !empty($this->{$field}) ? $this->{$field} : $this->getName();
    }

    public function getCss() {
        return $this->css;
    }

    public function getColor($removeHashtag = false) {
        $field = "main_color";
        if (!$removeHashtag) {
            return $this->{$field};
        }
        else {
            return str_replace("#", "", $this->{$field});
        }
    }

    public function getId() {
        return $this->id;
    }
}
