<div class="modal modal-ask" id="modal-ask-{{ $paper->getId() }}">
    <div class="modal-background"></div>
    <div class="modal-content">
        <div class="section">
            <h2 class="title is-5">{{ $paper->getName() }}</h2>
            <p class="subtitle">{{__("Realizar pregunta")}}</p>
            <form method="POST" class="ask-form" action="{{ route('papers.ask', ['paper_id' => $paper->getId()]) }}">
                @csrf

                <div class="field is-horizontal">
                    <div class="field-label is-normal">
                        <label class="label">{{__("Nombre")}}</label>
                    </div>
                    <div class="field-body">
                        <div class="field">
                            <input type="text" class="input" name="name" placeholder="{{__("Nombre")}}">
                        </div>
                        <div class="field">
                            <input type="email" class="input" name="email" placeholder="{{__("Correo electrónico")}}" required>
                        </div>
                    </div>
                </div>

                <div class="field is-horizontal">
                    <div class="field-label is-normal">
                        <label class="label">{{__("Pregunta")}}</label>
                    </div>
                    <div class="field-body">
                        <div class="field">
                            <textarea class="textarea" name="message" placeholder="{{__("Texto de la consulta")}}" required></textarea>
                        </div>
                    </div>
                </div>

                <div class="field">
                    <div class="control">
                        <label class="checkbox">
                            <input type="checkbox" name="acceptance" required>
                            {{__("He leído y acepto la")}} <a href="" target="_blank">{{__("política de privacidad")}}</a>
                        </label>
                    </div>
                </div>

                <div class="field is-grouped">
                    <div class="control">
                        {{-- <button class="button is-link">Enviar</button> --}}
                        <input type="submit" value="{{__("Enviar")}}" class="button">
                    </div>
                    {{-- <div class="control">
                        <button class="button is-link is-light">Cancelar</button>
                    </div> --}}
                </div>

                {{-- <p class="help is-danger">NOTA: El sistema está en modo de desarrollo y el contenido de este formulario no se enviará al ponente, sino a pablogalan@logievents.com</p> --}}
                <p class="help result"></p>
            </form>
        </div>
    </div>
    <button class="modal-close is-large" aria-label="close"></button>
</div>
