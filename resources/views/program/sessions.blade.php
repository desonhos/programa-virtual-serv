<section class="section section-sessions">
    @foreach ($days as $day)
        <div class="content-tab" id="content-tab-{{ $day }}" style="display: none">
            @include('program._sessions-table')
            @include('program._sessions-scheme')
        </div>
    @endforeach
</section>
<script type="text/javascript">
    $(document).ready(function() {
        $('.skeduler-headers').each(function(){
            $(this).html('<div class="scroller">' + $(this).html() + '</div>');
        })
        $('.skeduler-headers .scroller, .skeduler-main').addClass('syncscroll'); // al final no se usa esta clase pero la dejo estar para saber cuáles son los que se sincronizan

        $(".skeduler-headers .scroller").scroll(function () {
            var scrollLeft = $(this).scrollLeft();
            $(".skeduler-main").scrollLeft(scrollLeft);
        });
        $(".skeduler-main").scroll(function () {
            var scrollLeft = $(this).scrollLeft();
            $(".skeduler-headers .scroller").scrollLeft(scrollLeft);
        });
    })
</script>

@include('partials._modal-another-agenda')
