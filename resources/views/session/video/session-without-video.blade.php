@php
    $podcastAedv = [1051, 1052, 1053];
    $noVideoImg = $session->getNoVideoImg();
    $noVideoImg = (!empty($noVideoImg)) ? \VoyagerHelper::getImageUrl($noVideoImg) : config('web.event.defaultImg');
    $posterImg = (!empty($noVideoImg)) ? \VoyagerHelper::getImageUrl(setting('web.img_reproductor_posters')) : config('web.event.defaultImg');
    if (!\TextHelper::isUrl($noVideoImg)) {
        $noVideoImg = Voyager::image($noVideoImg);
    }
    $noVideoTextColor = $session->getNoVideoTextColor();
    $isPoster = false;
    foreach ($papers as $p) {
        if ($p->poster) { $isPoster = true; break; }
    }
@endphp

<div class="is-relative" id="prevVideo">
    @if (!in_array($session->getId(), $podcastAedv))
        @if (!$isPoster)
            <img src="{{ $noVideoImg }}">
        @else
            <img src="{{ $posterImg }}">
        @endif
        <div class="is-overlay is-flex is-justify-content-center is-align-items-center">
            <div class="has-text-24 p-2">
                <p class="is-uppercase has-text-centered" @if (!empty($noVideoTextColor)) style="color:{{ $noVideoTextColor }}" @endif>
                    {{ $session->getNoVideoText() }}
                </p>
            </div>
        </div>
    @else
        <img src="/event/virtual/podcast-cover.jpg">
    @endif
</div>
