@php
$menu = 'infoapp';
if (\SettingHelper::isAppPV()) { $menu = 'infoapppv'; }
if (\SettingHelper::isPV()) { $menu = 'infopv'; }
@endphp

{{-- <div id="menuInfo"> --}}
    {{ menu($menu, 'partials._navbar-menuitems') }}
{{-- </div> --}}