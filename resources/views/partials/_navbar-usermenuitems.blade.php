<a class="navbar-item is-user noDynamicStyles portfolioBtn">
    {{-- @svg('ico-cartera', 'ico-cartera') --}}
    <span class="navbar-item-text">{{ __("Mi cartera") }}</span>
</a>
<a class="navbar-item noDynamicStyles" href="{{ route('user.profile') }}">
    {{-- @svg('ico-mi-perfil', 'ico-mi-perfil') --}}
    <span class="navbar-item-text">{{ __("Mi perfil") }}</span>
</a>

@if ($user = \Auth::guard('multi')->user())
    @if ($user->canEvaluate())
        {{-- Evaluación de sesiones --}}
        @if (!empty($user->survey_sessionlist_url))
            <a class="navbar-item noDynamicStyles" href="{{ $user->survey_sessionlist_url }}">
                <span class="navbar-item-text">{{ __("Evaluación de sesiones") }}</span>
            </a>
        @endif

        {{-- Evaluación del congreso --}}
        @if (!empty($user->survey_congress_url))
            <a class="navbar-item noDynamicStyles" href="{{ $user->survey_congress_url }}">
                <span class="navbar-item-text">{{ __("Evaluación de congreso") }}</span>
            </a>
        @endif
    @endif
@endif

<a class="navbar-item noDynamicStyles" href="{{ route('user.exit') }}">
    {{-- @svg('ico-cerrar-sesion', 'ico-cerrar-sesion') --}}
    <span class="navbar-item-text">{{ __("Cerrar sesión") }}</span>
</a>
