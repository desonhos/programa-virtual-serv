<?php
namespace App\Http\Middleware;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
class CheckClosingDate
{
    /**
     * Handle an incoming request.
     * Se asegura de que el usuario ha hecho login y que pertenece a la asociación a la que se está accediendo (lo controla con el parámetro asociacionId que va en la URL)
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next) {
        if (\DateHelper::isDatetimeInPast(\SettingHelper::getClosingDate())) {
            return redirect(route('user.exit'));
        }
        return $next($request);
    }
}