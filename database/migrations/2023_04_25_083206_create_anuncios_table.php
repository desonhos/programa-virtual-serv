<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('anuncios', function (Blueprint $table) {
            $table->id();
            $table->string('imagen', 255)->nullable();
            $table->string('imagen_vertical', 255)->nullable();
            $table->string('video', 255)->nullable();
            $table->string('video_vertical', 255)->nullable();
            $table->integer('t_min_cierre')->nullable();
            $table->integer('t_cierre_auto')->nullable();
            $table->integer('probabilidad_mostrar')->nullable();
            // $table->string('url_mostrar', 1024)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('anuncios');
    }
};
