<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Congress extends Model {

    protected $connection= 'cyex_online';
    protected $table = 'congresses';

    /** Obtiene todos los del evento actual */
    public static function _getAll() {
        $areas = Congress::where('event_id', config('web.eventId'))
                    ->orderBy('name')
                    ->get();
        return $areas;
    }

    public function getId() {
        return $this->id;
    }
}
