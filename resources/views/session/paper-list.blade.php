@php
    $videoNum = 0;
    if (!empty($session->getVimeoUrl())) {
        $videoNum = 1;
    }
@endphp
@if (!empty($papers))
    <section id="sessionPaperList" class="is-paperList">
        <div class="container">
            @if (!\SettingHelper::isApp())
                @include('session.sort-posters')
            @endif
            <div class="paperList">
                @foreach ($papers as $paper)
                    <article class="paper" data-average="0">
                        <span class="paperHours has-text-14-mobile">{{ $paper->getHour() }}</span>
                        <h2 class="paperTitle">{!! $paper->getName() !!}</h2>
                        <ul class="links-list is-horizontal has-separator">
                            @if (!empty($paper->getVimeoUrl($session)))
                                <li><a onclick="playVideo({{$videoNum}})">{{__("Ver ponencia")}}</a></li>
                                @php $videoNum++; @endphp
                            @endif
                        </ul>
                        @include('session._authors')
                        @include('session/_fileLinks')
                        @include('session._conflicts')
                        @if (!\SettingHelper::isApp())
                            @include('session.poster-ratings')
                            @include('session/_modal-question')
                        @endif
                    </article>
                @endforeach
            </div>
        </div>
    </section>
@endif
@php
    function isVisible($doc) {
        $virtual = (!\SettingHelper::isApp()) ? 1 : 0;
        if ($virtual && $doc['plataforma']) { return true; }
        if (!$virtual && $doc['webapp']) { return true; }
        return false;
    }
@endphp
