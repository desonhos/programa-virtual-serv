<?php

namespace App\Helpers;
use \Datetime;
use \DateTimeZone;

// Cómo crear un helper en Laravel: https://stackoverflow.com/questions/28290332/best-practices-for-custom-helpers-in-laravel-5
class Date {
    public static $days = [
        "es" => ["Domingo","Lunes","Martes","Miércoles","Jueves","Viernes","Sábado"],
        "en" => ["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"]
    ];
    public static $months = [
        "es" => ["Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"],
        "en" => ["January","February","March","April","May","June","July","August","September","October","November","December"]
    ];

    public static function formatLong($date, $lang="es") {
        if (empty($date)) { return "Poster"; }
        $time = strtotime($date);
        return self::$days[$lang][strftime("%w", $time)] . ", " . strftime("%d", $time) . " de " . strtolower(self::$months[$lang][((int)strftime("%m", $time))-1]);
    }

    public static function formatShort($date, $lang="es") {
        if (empty($date)) { return "Poster"; }
        $time = strtotime($date);
        return mb_substr(self::$days[$lang][strftime("%w", $time)], 0, 3) . ". " . strftime("%d", $time) . " " . substr(strtolower(self::$months[$lang][((int)strftime("%m", $time))-1]), 0, 3) . ".";
    }

    public static function formatMini($date, $lang="es") {
        if (empty($date)) { return "Poster"; }
        $time = strtotime($date);
        return mb_substr(self::$days[$lang][strftime("%w", $time)], 0, 3) . ". " . strftime("%d", $time);
    }

    public static function formatDayMonth($date, $lang="es") {
        if (empty($date)) { return ""; }
        $time = strtotime($date);
        if ($lang == "es") {
            return strftime("%d", $time) . " de " . strtolower(self::$months[$lang][((int)strftime("%m", $time))-1]);
        }
        return strftime('%A, %B %d, %Y', $time);
    }

    /**
    * Devuelve si el día pasado se corresponde a hoy
    * @param $date Fecha en formato Y-m-d
    */
    public static function isToday($date) {
        $current = strtotime(date("Y-m-d"));
        $date    = strtotime($date);
        $datediff = $date - $current;
        $difference = floor($datediff/(60*60*24));
        return ($difference == 0);
    }

    /**
    * Devuelve si la hora actual está dentro del rango pasado
    */
    public static function isHourInRange($hour1, $hour2) {
        $current = Date::HISToSeconds(date("H:i:s"));
        $hour2 = Date::HISToSeconds(date($hour2));
        $hour1 = Date::HISToSeconds(date($hour1));
        if (empty($hour1) && empty($hour2)) {
            return true;
        }
        elseif (empty($hour1) && !empty($hour2)) {
            return ($current <= $hour2);
        }
        elseif (!empty($hour1) && empty($hour2)) {
            return ($current >= $hour1);
        }
        return ($current >= $hour1) && ($current <= $hour2);
    }

    /** A partir de una hora en formato H:i:s, devuelve el valor en segundos */
    public static function HISToSeconds($his) {
        if (empty($his)) { return null; }
        if (strlen($his) == 5) { $his .= ":00";}
        $parts = explode(':', $his);
        if (count($parts) !== 3) { return null; }
        $secondsInHours = ((int) $parts[0]) * 3600;
        $secondsInMinutes = ((int) $parts[1]) * 60;
        $seconds = (int) $parts[2];
        return $secondsInHours + $secondsInMinutes + $seconds;
    }

    /** Función que deshace la conversión hecha con HISToSeconds */
    public static function secondsToHIS($seconds) {
        $hours = floor($seconds / 3600);
        $minutes = floor(($seconds / 60) % 60);
        $seconds = $seconds % 60;
        if ($seconds < 10) { $seconds = "0" . $seconds; }
        return "$hours:$minutes:$seconds";
    }

    /** Modifica las horas de una dada con el formato H:i:s, en una cantidad de $hourInc */
    public static function modifyHISHour($his, $hourInc) {
        $epoch = Date::HISToSeconds($his) + ((int)$hourInc * 3600);
        return Date::secondsToHIS($epoch);
    }

    /** Devuelve la diferencia entre dos strings que representan una marca de tiempo con el formato Y-m-d H:i:s */
    public static function getDifferenceBetweenDatetimes($dt1, $dt2) {
        $differenceInSeconds = strtotime($dt1) - strtotime($dt2);
        return $differenceInSeconds;
    }

    /** Devuelve verdadero si la marca de tiempo es de una fecha ya pasada
    * @param $dt String con el formato Y-m-d H:i:s
    */
    public static function isDatetimeInPast($dt) {
        return (Date::getDifferenceBetweenNow($dt) > 0);
    }

    /** Devuelve el número de segundos pasados entre el momento actual y la marca de tiempo pasada
    * @param $dt String con el formato Y-m-d H:i:s
    */
    public static function getDifferenceBetweenNow($dt) {
        $currentTime = time();
        $timeToCompare = strtotime($dt);
        $differenceInSeconds = $currentTime - $timeToCompare;
        return $differenceInSeconds;
    }

    /** Devuelve verdadero si la marca de tiempo está entre las dos pasadas */
    public static function isDatetimeInRange($dt, $dt0, $dt1) {
        $t = strtotime($dt);
        $t0 = strtotime($dt0);
        $t1 = strtotime($dt1);
        return Date::isTimeInRange($t, $t0, $t1);
    }

    public static function isTimeInRange($t, $t0, $t1) {
        return (($t >= $t0) && ($t <= $t1));
    }

    public static function seconds2human($ss) {
        $s = $ss%60;
        $m = floor(($ss%3600)/60);
        $h = floor(($ss%86400)/3600);
        $d = floor(($ss%2592000)/86400);
        $M = floor($ss/2592000);

        $res = "";
        if ($M > 0) { $res = "$M meses, "; }
        if ($d > 0) { $res .= "$d días, "; }
        if ($h > 0) { $res .= "$h horas, "; }
        if ($m > 0) { $res .= "$m minutos, "; }
        if ($s > 0) { $res .= "$s segundos"; }
        return rtrim($res, ', ');
    }

    /** Obtiene un objeto de tipo DateTime para poder usar fecha y hora en un Calendario (Google Calendar, Outlook Calendar…).
     * @param $date Formato: 2023-09-29
     * @param $hour Formato: 10:25:00
     */
    public static function getCalendarDateTime($date, $hour) {
        if (empty($date)) { return false; }
        $h = "00:00:00";
        if (!empty($hour)) {
            $hParts = explode(":", $hour);
            $h = $hParts[0] . ":" . $hParts[1] . ":" . "00";
        }
        $datetime = new DateTime($date . ' ' . $h);
        return $datetime;
    }

    /** Obtiene un fechor en formato para Google Calendar.
     * @param $date Formato: 2023-09-29
     * @param $hour Formato: 10:25:00
     */
    public static function getGoogleCalendarTimestamp($date, $hour) {
        $datetime = \App\Helpers\Date::getCalendarDateTime($date, $hour);
        if (!$datetime) { return false; }
        // necesario poner la zona horaria para que la hora salga correctamente (en caso de no ponerla, pondrá la que tiene el sistema por defecto, que es Europe/Madrid)
        $timezone = new DateTimeZone('UTC');
        $datetime->setTimezone($timezone);
        return $datetime->format('Ymd\THis\Z');
    }

    public static function getOutlookCalendarTimestamp($date, $hour) {
        // 2023-05-08T03:30:00.000Z @see https://github.com/InteractionDesignFoundation/add-event-to-calendar-docs/discussions/47
        $datetime = \App\Helpers\Date::getCalendarDateTime($date, $hour);
        if (!$datetime) { return false; }
        return $datetime->format('Y-m-d\TH:i:s\.000Z');
    }

    public static function getAppleCalendarTimestamp($date, $hour) {
        $datetime = \App\Helpers\Date::getCalendarDateTime($date, $hour);
        if (!$datetime) { return false; }
        return $datetime->format('Ymd\THis');
    }
}
