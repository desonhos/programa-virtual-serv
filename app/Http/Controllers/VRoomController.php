<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\VRoom;
use App\Models\CyxVRoom;

class VRoomController extends Controller {

    public function show($slug) {
        $vroom = VRoom::_getBySlug($slug);
        if ($vroom) {
            $user = \Auth::guard('multi')->user();
            if ($user) { CyxVRoom::_userVisitsRoom($user->getIdInEvent(), $vroom); }
        }
        return view('virtual.room', compact('vroom'));
    }

    public function showNext($slug) {
        $vroom = VRoom::_getNext($slug);
        return redirect()->route('virtual.vroom.view', ['slug' => $vroom->getSlug()]);
    }

    public function showPrev($slug) {
        $vroom = VRoom::_getPrev($slug);
        return redirect()->route('virtual.vroom.view', ['slug' => $vroom->getSlug()]);
    }

    public function showById($id) {
        $vroom = Vroom::findOrFail($id);
        return redirect()->route('virtual.vroom.view', ['slug' => $vroom->getSlug()]);
    }
}
