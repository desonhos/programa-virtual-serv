<?php

namespace App\Helpers;

class Setting {

    /** Modo App si no logueado */
    public static function isApp() {
        $user = \Auth::guard('multi')->user();
        return !$user;
    }

    /** Modo AppPV si logueado de tipo A y estamos en el periodo entre el inicio del congreso virtual
     * y el fin del congreso presencial */
    public static function isAppPV() {
        $user = \Auth::guard('multi')->user();
        if (!$user) { return false; }
        $modo = Setting::getModoCongreso();
        return ($modo == "apppv") && $user->isA();
    }

    /** Modo PV si:
     *  (logueado y ha terminado el periodo de congreso presencial) o
     *  (logueado de tipo V y ha empezado el acceso virtual y no ha terminado el acceso presencial)
     * */
    public static function isPV() {
        $user = \Auth::guard('multi')->user();
        if (!$user) { return false; }
        $modo = Setting::getModoCongreso();
        return (
            (($modo == "apppv") && $user->isV()) ||
            ($modo == "pv")
        );
    }

    public static function isAntesInicioVirtual() {
        $modo = Setting::getModoCongreso();
        return ($modo == "app");
    }

    /** Devuelve verdadero cuando en este congreso hay que comprobar cada X tiempo
     * si el usuario sigue viendo el vídeo */
    public static function isCheckingUserView() {
        $pcvs = Setting::getPlayerCheckViewSecs();
        if (empty($pcvs) || !is_int($pcvs)) { return false; }
        if ($pcvs > 0) { return true; }
        return false;
    }

    /** En una línea de tiempo, sería así: app (inicioAppPV) apppv ($inicioPV) pv */
    public static function getModoCongreso() {
        $inicioAppPV = setting('web.fecha_inicio_apppv');
        $inicioPV = setting('web.fecha_inicio_pv');
        if (!empty($inicioPV) && \DateHelper::isDatetimeInPast($inicioPV)) {
            return "pv";
        }
        if (!empty($inicioAppPV) && \DateHelper::isDatetimeInPast($inicioAppPV)) {
            return "apppv";
        }
        return "app";
    }

    /** Obtiene el fechor de inicio del acceso virtual (el momento en el que se podrán ver vídeos ya) */
    public static function getFechaInicioAccesoVirtual() {
        $inicioAppPV = setting('web.fecha_inicio_apppv');
        $inicioPV = setting('web.fecha_inicio_pv');
        if (empty($inicioPV) || empty($inicioAppPV)) {
            if (empty($inicioPV)) {
                return $inicioAppPV;
            }
            return $inicioPV;
        }
        return ($inicioAppPV < $inicioPV) ? $inicioAppPV : $inicioPV;
    }

    public static function getFechaInicioAccesoVirtualEnTexto($lang = "es") {
        $fecha = Setting::getFechaInicioAccesoVirtual();
        return \DateHelper::formatDayMonth($fecha, $lang);
    }

    public static function getPlayerCheckViewSecs() {
        return setting('web.player_check_view_secs');
    }

    public static function getPlayerCheckViewQuestion() {
        return setting('web.player_check_view_question');
    }

    public static function getPlayerCheckViewConfirm() {
        return setting('web.player_check_view_confirm');
    }

    public static function getContactEmail() {
        return setting('web.contact_email');
    }

    public static function getPrivacyPolicy() {
        return setting('web.privacy_policy');
    }

    /** Obtiene cuánto tiempo (s) pasa entre llamadas al servidor para actualizar
     * el tiempo reproducido del vídeo por parte de un usuario. Como mínimo será 15
     * y si en la base de datos no se pone nada, se saca de los parámetros de configuración */
    public static function getPlayerStoreViewSecs() {
        $secs = setting('web.player_store_view_secs');
        if (!empty($secs) && is_int($secs) && ($secs > 14)) { return $secs; }
        // devuelve el valor por defecto
        return config('web.player.storeViewSecs');
    }

    /** Obtiene la fecha de cierre de la plataforma virtual */
    public static function getClosingDate() {
        $closingDate = setting('web.closing_date');
        if ($closingDate == null) { return "2100-12-31 00:00:00"; }
        return $closingDate;
    }

    public static function getTextAccessFrom($lang = "es") {
        $s = setting('web.text_access_from');
        $varFecha = '$FECHA_FIN_MODO_APP';
        if (str_contains($s, $varFecha)) {
            $s = str_replace($varFecha, \SettingHelper::getFechaInicioAccesoVirtualEnTexto($lang), $s);
        }
        return $s;
    }

    public static function getInfoApp() {
        return setting('web.info_app');
    }

    public static function getInfoAppPV() {
        return setting('web.info_apppv');
    }

    public static function getInfoPV() {
        return setting('web.info_pv');
    }

    public static function getInfo() {
        if (Setting::isApp()) {
            return Setting::getInfoApp();
        }
        elseif (Setting::isAppPV()) {
            return Setting::getInfoAppPV();
        }
        elseif (Setting::isPV()) {
            return Setting::getInfoPV();
        }
    }

    public static function getNoVideoText() {
        return setting('web.no_video_text');
    }

    public static function getWelcomeTitle() {
        return setting('web.welcome_title');
    }

    public static function getWelcomeText() {
        return setting('web.welcome_text');
    }

    public static function getLoginTitle() {
        return setting('web.login_title');
    }

    public static function getWelcomeTitleAfterClosing() {
        return setting('web.welcome_title_after_closing');
    }

    public static function getWelcomeTextAfterClosing() {
        return setting('web.welcome_text_after_closing');
    }

    public static function getSchemaBackgroundColor() {
        $c = setting('web.schema_background_color');
        return Text::isValidHexRGB($c) ? $c : false;
    }

    public static function getSchemaSessionBackgroundColor() {
        $c = setting('web.schema_session_background_color');
        return Text::isValidHexRGB($c) ? $c : false;
    }

    public static function getSchemaHeaderBackgroundColor() {
        $c = setting('web.schema_header_background_color');
        return Text::isValidHexRGB($c) ? $c : false;
    }

    public static function getSchemaHeaderFontColor() {
        $c = setting('web.schema_header_font_color');
        return Text::isValidHexRGB($c) ? $c : false;
    }

    public static function getSchemaDefaultFontColor() {
        $c = setting('web.schema_default_font_color');
        return Text::isValidHexRGB($c) ? $c : false;
    }

    public static function getInstruccionesInicioIos() {
        return setting('web.instrucciones_inicio_ios');
    }

    public static function getInstruccionesInicioAndroid() {
        return setting('web.instrucciones_inicio_android');
    }

    /** Texto que aparece junto al logo, en barra superior (Webapp, virtual…) */
    public static function getNombreCategoriaPlataforma() {
        return setting('web.nombre_' . Setting::getModoCongreso());
    }

    public static function getUrlNoSanitario() {
        return setting('web.url_no_sanitario');
    }

    public static function getHideFieldName() {
        if (Setting::isApp()) {
            return 'pv_hide_app';
        }
        if (Setting::isAppPV()) {
            return 'pv_hide_apppv';
        }
        if (Setting::isPV()) {
            return 'pv_hide_pv';
        }
        // no debería llegar aquí
        return 'pv_hide_pv';
    }

}
