@php
function getId($maparea) {
    if (array_key_exists('id', $maparea)) {
        return $maparea['id'];
    }
    return "";
}
function getNombre($maparea) {
    if (array_key_exists('nombre', $maparea)) {
        return $maparea['nombre'];
    }
    return "";
}
function getTarget($maparea) {
    if (array_key_exists('target', $maparea)) {
        if ($maparea['target'] == "ext") { return "_blank"; }
        elseif ($maparea['target'] == "doc") { return "_blank"; }
        elseif ($maparea['target'] == "video") { return "_blank"; }
        else { return "_self"; }
    }
}
function getHref($maparea) {
    if (array_key_exists('url', $maparea)) {
        if ($maparea['target'] == "ext") { return $maparea['url']; }
        elseif ($maparea['target'] == "doc") { return $maparea['url']; }
        elseif ($maparea['target'] == "video") { return $maparea['url']; }
        else { return $maparea['url']; }
    }
}
function getX0($maparea) {
    if (array_key_exists('inicioX', $maparea)) {
        return $maparea['inicioX'];
    }
    return "";
}
function getX1($maparea) {
    if (array_key_exists('finX', $maparea)) {
        return $maparea['finX'];
    }
    return "";
}
function getY0($maparea) {
    if (array_key_exists('inicioY', $maparea)) {
        return $maparea['inicioY'];
    }
    return "";
}
function getY1($maparea) {
    if (array_key_exists('finY', $maparea)) {
        return $maparea['finY'];
    }
    return "";
}
function getGrupo($maparea) {
    if (array_key_exists('grupo', $maparea)) {
        return $maparea['grupo'];
    }
    return "";
}
function isVideo($maparea) {
    if (array_key_exists('target', $maparea)) {
        if ($maparea['target'] == "video") { return true; }
    }
    return false;
}
function isDoc($maparea) {
    if (array_key_exists('target', $maparea)) {
        if ($maparea['target'] == "doc") { return true; }
    }
    return false;
}

function isYoutubeUrl($videoFile) {
    return (str_contains($videoFile, "https://youtu.be") || str_contains($videoFile, "https://www.youtube.com/"));
}
function getYoutubeIframeFromUrl($string) {
    // https://stackoverflow.com/questions/19050890/find-youtube-link-in-php-string-and-convert-it-into-embed-code
    return preg_replace(
        "/\s*[a-zA-Z\/\/:\.]*youtu(be.com\/watch\?v=|.be\/)([a-zA-Z0-9\-_]+)([a-zA-Z0-9\/\*\-\_\?\&\;\%\=\.]*)/i",
        "<iframe src=\"//www.youtube.com/embed/$2\" allowfullscreen></iframe>",
        $string
    );
}
@endphp
