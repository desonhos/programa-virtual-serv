@if (\Session::has('success'))
<article class="message is-success">
    <div class="message-header">
        <p>{{ __("Completado") }}</p>
    </div>
    <div class="message-body">{!! \Session::get('success') !!}</div>
</article>
@endif

@if ($errors->any())
    <article class="message is-danger">
        <div class="message-header">
            <p>{{ __("Completado") }}</p>
        </div>
        <div class="message-body">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    </article>
@endif
