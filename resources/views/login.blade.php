@extends('layouts/login')

@section('form')
    <form method="POST" action="{{ route('login.virtual') }}">
        @csrf
        <div class="field">
            <div class="control">
                <input name="email" class="input" type="text" value="" placeholder="{{ __('Correo electrónico') }}">
            </div>
        </div>
        <div class="field">
            <div class="control">
                <input name="password" class="input" type="password" value="" placeholder="{{ __('Contraseña') }}">
            </div>
        </div>
        <div class="field">
            <div class="control">
                <a href="{{ route('remember-password.show') }}" class="has-text-14 is-text-link">{{ __('¿Has olvidado tu contraseña?') }}</a>
            </div>
        </div>
        <div class="field pt-2">
            <div class="control has-text-right">
                <input class="button is-primary is-outline" type="submit" value="{{ __('Acceder') }}">
            </div>
        </div>
        @if($errors->any())
            <p class="help is-danger">
                {{$errors->first()}}
            </p>
        @endif
    </form>
@endsection
