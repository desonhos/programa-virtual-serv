{{-- Vista esquema de las sesiones del día $day --}}
<div class="sessions-scheme" id="skeduler-{{$day}}">
</div>

<script type="text/javascript">
    $(document).ready(function(){
        console.log("inicializa el skeduler del día {{$day}}");
        generateSkedulerDia(
            '{{$day}}',
            '{!! json_encode($sessionDataForScheme, JSON_UNESCAPED_SLASHES | JSON_HEX_QUOT | JSON_HEX_APOS | JSON_HEX_TAG | JSON_HEX_AMP) !!}',
            '{!! json_encode($rooms->pluck('id')->toArray()) !!}'
        );
    })
</script>

{{-- Corrección para que se pueda hacer scroll con las barras del navegador --}}
<style>
    @media (min-width: 769px) {
        .skeduler-main {
            overflow: scroll!important;
            /* max-height: 60vh; */
            /* max-height: 100%; */

            /* 58,5px (altura navbar), 46.25px altura selector de días, 72px barra buscador, 1.5rem el padding-top, 3 rem el padding-bottom */
            /* 58.5+46.25+72+24+48 = 248.75px; */
            max-height: calc(100vh - 240.75px); /* se optó por no poner los 48 equivalentes al padding de abajo*/
        }
        .skeduler-main-timeline {
            position: sticky!important;
            left: 0;
        }
        .page-program .sessions-scheme .skeduler-main-body {
            overflow-x: unset;
            overflow-y: unset;
            margin-left: -2px;
        }
        body { min-height: unset!important; }

        /* Para ocultar las barras de scroll pero mantener la funcionalidad */
        .skeduler-main::-webkit-scrollbar {
            display: none;
        }
        .skeduler-main {
            -ms-overflow-style: none;
            scrollbar-width: none;
        }

    }
    .skeduler-task-placeholder > div { z-index: 9980; }
    .page-program .sessions-scheme .skeduler-main-timeline { z-index: 9981; }
</style>

<script>
    document.addEventListener('DOMContentLoaded', function () {
        const ele = document.querySelector('.skeduler-main');
        ele.style.cursor = 'grab';

        let pos = { top: 0, left: 0, x: 0, y: 0 };

        const mouseDownHandler = function (e) {
            ele.style.cursor = 'grabbing';
            ele.style.userSelect = 'none';

            pos = {
                left: ele.scrollLeft,
                top: ele.scrollTop,
                // Get the current mouse position
                x: e.clientX,
                y: e.clientY,
            };

            document.addEventListener('mousemove', mouseMoveHandler);
            document.addEventListener('mouseup', mouseUpHandler);
        };

        const mouseMoveHandler = function (e) {
            // How far the mouse has been moved
            const dx = e.clientX - pos.x;
            const dy = e.clientY - pos.y;

            // Scroll the element
            ele.scrollTop = pos.top - dy;
            ele.scrollLeft = pos.left - dx;
        };

        const mouseUpHandler = function () {
            ele.style.cursor = 'grab';
            ele.style.removeProperty('user-select');

            document.removeEventListener('mousemove', mouseMoveHandler);
            document.removeEventListener('mouseup', mouseUpHandler);
        };

        // Attach the handler
        ele.addEventListener('mousedown', mouseDownHandler);
    });
</script>
{{-- Fin corrección para que se pueda hacer scroll con las barras del navegador --}}

{{-- Raya de los cuartos de hora --}}
<style>
    .skeduler-cell {
        position: relative;
    }
    .skeduler-cell::before {
        content: '';
        position: absolute;
        top: 50%;
        left: 0;
        width: 100%;
        height: 1px;
        /* background-color: rgb(227, 227, 227); */
        background: 
            linear-gradient(90deg, rgba(0, 0, 0, 0) 25%, rgb(227, 227, 227) 25%, rgb(227, 227, 227) 50%, rgba(0, 0, 0, 0) 50%, rgba(0, 0, 0, 0) 75%, rgb(227, 227, 227) 75%, rgb(227, 227, 227)) 0 100%,
            linear-gradient(180deg, rgba(0, 0, 0, 0) 25%, rgb(227, 227, 227) 25%, rgb(227, 227, 227) 50%, rgba(0, 0, 0, 0) 50%, rgba(0, 0, 0, 0) 75%, rgb(227, 227, 227) 75%, rgb(227, 227, 227)) 100% 0;
        background-size: 10px 10px;
        transform: translateY(-50%);
    }
</style>
{{-- Fin raya de los cuartos de hora --}}
