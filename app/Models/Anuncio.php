<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Storage;

class Anuncio extends Model {

    public static function _get($anucioId) {
        return Anuncio::where('id', $anucioId)->first();
    }

    public static function _getAll() {
        return Anuncio::get();
    }

    /** Tiempo (s) que ha de pasar antes de que aparezca el botón que permite cerrar el anuncio */
    public function getTMinCierre() { return $this->t_min_cierre; }

    /** Tiempo (s) tras el cual el anuncio se cerrará automáticamente  */
    public function getTCierreAuto() { return $this->t_cierre_auto; }
    
    /** Tiempo (s) que tienen que haber pasado para que el anuncio se pueda mostrar otra vez */
    public function getProbabilidadMostrar() {
        $p = $this->propabilidad_mostrar;
        if (!$p || !is_int($p)) { $p = 50; } // valor por defecto: 50%
        if ($p > 100) { $p = 100; }
        if ($p < 0) { $p = 0; }
        return $p;
    }
    public function getImagen() { return $this->imagen; }
    public function getImagenVertical() { return $this->imagen_vertical; }
    public function getVideo() { return $this->video; }
    public function getVideoVertical() { return $this->video_vertical; }

    public function getImagenUrl() {
        $img = $this->getImagen();
        if (!$img) { return false; }
        return Storage::disk(config('voyager.storage.disk'))->url($img);
    }
    public function getImagenVerticalUrl() {
        $img = $this->getImagenVertical();
        if (!$img) { return false; }
        return Storage::disk(config('voyager.storage.disk'))->url($img);
    }
    public function getVideoUrl() {
        $video = $this->getVideo();
        if (!$video) { return false; }
        return Storage::disk(config('voyager.storage.disk'))->url($video);
    }
    public function getVideoVerticalUrl() {
        $video = $this->getVideoVertical();
        if (!$video) { return false; }
        return Storage::disk(config('voyager.storage.disk'))->url($video);
    }
    public function hasImageOrVideo() {
        return $this->getImagen() || $this->getVideo();
    }

    /** Indica si el anuncio ya se ha mostrado */
    public function isAlreadyShowed() {
        // Control por sesión
        $value = session('anuncio_mostrado');
        if (empty($value)) {
            session(['anuncio_mostrado' => true]);
            return false;
        }
        return true;
    }
}
