@extends('layouts.app' )

{{-- @section('page-class', $cls) --}}
@section('page-class', 'page-inicio-webapp page-info')

@section('content')
    <?php /*
    <article>
        <section class="section">
            <div class="container content">
                {{-- Menú de info --}}
                @include('partials._menu-info')
                {!! $info !!}
            </div>
        </section>
    </article>
    */ ?>

    <div id="wrap">
        <div id="logos">
            <img class="logo-sociedad" src="/event/inicio-logo-sociedad.png">
        </div>
        <div id="gridMenu">
            @include('partials._menu-info')
        </div>
    </div>
    {{-- Botón de añadir a la página de inicio --}}
    {{-- @if (config('web.hasWebapp'))
        <a class="btnAddHome" href="{{ route('webapp.add-to-homescreen') }}">
            @svg('ico-+inicio')
            <p class="leyenda">
                {{ __('Añadir a pantalla de inicio') }}
            </p>
        </a>
    @endif --}}
    {{-- Botón de iniciar sesión --}}
    {{-- @if (!\Auth::guard('multi')->user() && !\SettingHelper::isAntesInicioVirtual())
        <a href="{{ route('login') }}" id="loginBtnInicio" role="button" class="navbar-burger is-usermenu burger ml-auto is-flex is-align-items-center is-justify-content-center">
            @svg('ico-usuario', 'ico-usuario')
        </a>
    @endif

    @include('partials.anuncio') --}}
@endsection
