<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Schedule;
use App\Models\Session;
use App\Models\Area;
use App\Models\Sessiontype;
use App\Models\Room;

class ScheduleController extends Controller {

    public function view() {
        $allSessions = Session::_getAll();
        $sessions = [];
        foreach ($allSessions as $session) {
            if ($session->isInSchedule()) {
                array_push($sessions, $session);
            }
        }
        $areas = Area::_getAll();
        $sessiontypes = Sessiontype::_getAll();
        $rooms = Room::_getAll();
        $user = session("user");
        return view('schedule', compact(
            'sessions', 'areas', 'sessiontypes', 'rooms', 'user'
        ));
    }
}
