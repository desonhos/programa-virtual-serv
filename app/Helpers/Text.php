<?php

namespace App\Helpers;
// use \Datetime;

class Text {
    // public static function getInitials($string) {
    //     $words = explode(' ', $string);
    //     if (count($words) >= 2) {
    //         return strtoupper(substr($words[0], 0, 1) . substr(end($words), 0, 1));
    //     }
    //     return Text::makeInitialsFromSingleWord($name);
    // }
    //
    // public static function makeInitialsFromSingleWord(string $name) {
    //     preg_match_all('#([A-Z]+)#', $name, $capitals);
    //     if (count($capitals[1]) >= 2) {
    //         return substr(implode('', $capitals[1]), 0, 2);
    //     }
    //     return strtoupper(substr($name, 0, 2));
    // }
    public static function getInitials($string) {
        $res = "";
        $words = explode(' ', $string);
        $i = 0;
        foreach ($words as $word) {
            if ($i >= 2) {
                break;
            }
            $res .= strtoupper(substr($word, 0, 1));
            $i++;
        }
        return $res;
    }
    public static function isUrl($string) {
        if (filter_var($string, FILTER_VALIDATE_URL)) {
            return true;
        }
        return false;
    }
    public static function isJson($string) {
       json_decode($string);
       return json_last_error() === JSON_ERROR_NONE;
    }
    public static function removeAccents($string) {
        return strtolower(trim(preg_replace('~[^0-9a-z]+~i', '-', preg_replace('~&([a-z]{1,2})(acute|cedil|circ|grave|lig|orn|ring|slash|th|tilde|uml);~i', '$1', htmlentities($string, ENT_QUOTES, 'UTF-8'))), ' '));
    }
    /** @see https://stackoverflow.com/questions/18185721/is-rgb-function-in-php */
    public static function isValidHexRGB($hex) {
        return preg_match('/^#?(([a-f0-9]{3}){1,2})$/i', $hex);
    }
    /** @see https://stackoverflow.com/questions/18185721/is-rgb-function-in-php */
    public static function isValidDecimalRgb($rgb) {
        return count($rgb) == 3 && is_numeric(implode($rgb)) && max($rgb) <= 255;
    }
}
