<div class="modal" id="checkViewingModal">
    <div class="modal-background"></div>
    <div class="modal-card">
        <header class="modal-card-head">
            <p class="modal-card-title is-flex is-align-items-center mb-0 is-size-5">
                {{__("Control de asistencia")}} {{ config('web.event.shortName') }}
            </p>
            {{-- <button class="delete" aria-label="close"></button> --}}
        </header>
        <section class="modal-card-body">
            <div class="is-flex is-justify-content-space-between">
                {{ \SettingHelper::getPlayerCheckViewQuestion() }}
            </div>
        </section>
        <footer class="modal-card-foot is-flex is-justify-content-space-between">
            <a class="close button letter-spacing-0">
                {{ \SettingHelper::getPlayerCheckViewConfirm() }}
            </a>
        </footer>
    </div>
</div>
