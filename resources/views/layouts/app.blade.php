<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}"
    @if (!(\Route::current()->getName() == 'inicio-webapp') && !(\Route::current()->getName() == 'webapp.info')) class="has-navbar-fixed-top" @endif
    class="min-safe-h-screen @yield('page-class')"
>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    {{-- <title>{{ config('app.name') }}</title> --}}
    <title>{{ config('web.event.shortName') }}</title>

    <!-- Metas --->
    {{-- <meta name="title" content="{{ config('app.name') }} - @yield('title')"> --}}
    <meta name="description" content="@yield('meta_description')">
    <meta name="keywords" content="@yield('meta_keywords')">
    <meta name="robots" content="@yield('meta_robots')">
    <meta name="revisit-after" content="@yield('meta_revisit-after')">

    <meta property="og:title" content="@yield('title')">
    <meta property="og:image" content="@yield('og_image')">
    <meta property="og:description" content="@yield('meta_description')">
    <meta property="og:url" content="{{ url()->current() }}">

    <!-- Global site tag (gtag.js) - Google Analytics -->
    {{-- <script async src="https://www.googletagmanager.com/gtag/js?id=UA-41607550-13"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-41607550-13');
    </script> --}}

    <!-- Scripts -->
    @include('utils.on-production')
    <script src="https://code.jquery.com/jquery-1.12.4.min.js" integrity="sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ=" crossorigin="anonymous"></script>
    <script src="{{ mix('js/app.js') }}" defer></script>
    @yield('js')

    <!-- Styles -->
    <link href="{{ mix('css/app.css') }}" rel="stylesheet">
    @yield('css')

    <!-- Favicon -->
    <link rel="icon" href="{{ config('app.url') }}/event/favicon.png" type="image/png" />
    <link rel="apple-touch-icon" sizes="1024x1024" href="{{ config('app.url') }}/event/apple-touch-icon.png">
    <meta name="apple-mobile-web-app-title" content="{{ config("web.event.shortName") }}">

    @laravelPWA

    <script>var lang = "{{ App::getLocale() }}"</script>

</head>
<body class="min-safe-h-screen @yield('page-class')">
    @include('partials.event-header')

    @yield('content')

    <a id="back-to-top">@svg('ico-back-to-top')</a>
    
    @include('partials.bottom-bar')
    @include('partials._modal-add-to-homescreen')
    @include('portfolio._modal')
    @include('partials.footer')
</body>
</html>
