import Alert from '@vizuaalog/bulmajs/src/plugins/alert';
import * as Schedule from '../modules/schedule.js';
var Mark = require('mark.js');

$(document).ready(function() {
    // para que no use la caché de Safari al darle al botón atrás (que hace que se vea mal)
    reloadOnBackButton();

    // Inicializa la tabla de sesiones
    initTable();

    // Inicializa los manejadores de enviar el formulario
    setOnSubmit();

    // Inicializa el manejador de borrar los criterios de búsqueda
    setOnClearFilters();

    checkDropdownfilterVisibility();
    $(window).resize(function() {
        checkDropdownfilterVisibility();
    });

    checkClearFilterVisibility();

    backBtnHandler();

    ajustesVisuales();

    checkHash();

     // va al día y a la hora en la que estemos
    checkDay();
    checkHour();

    searchIfPreviousSearched();

    // ajustes a hacer cuando se cambia la orientación de la página
    window.addEventListener('orientationchange', function() {
        ajustesSafariIphone();
    });
    window.addEventListener('resize', function(event) {
        ajustesSafariIphone();
    }, true);

    // asegura no mostrar los tres días seguidos en modo esquema cuando se le ha dado a recargar
    if (location.hash.includes("link-scheme")) {
        if (location.hash.includes("tab-todos") || (location.hash.split("|").length == 1)) {
            showDay($("#tab-todos").next());
        }
    }

    // añade las medias horas. Con jquery recorre todos los elementos hijos de .skeduler-main-timeline
    // y en aquellos cuyo contenido sea vacío, pondrá el contenido que tenga su elemento predecesor, sustituyendo los últimos dos 00 por 30
    jQuery(".skeduler-main-timeline").children().each(function(){
        if (jQuery(this).text() == "") {
            var hora = jQuery(this).prev().text();
            // en hora, reemplaza los últimos 00 por 30
            var yMedia = hora.replace(/00$/, "30");
            jQuery(this).text(yMedia);
            jQuery(this).addClass("mediaHora");
        }
    });
});

conservaScrollEnNavegacion();
function conservaScrollEnNavegacion() {
    // Guardamos la posición de scroll cada vez que se navega (se actualiza la URL)
    window.addEventListener('scroll', function() {
        localStorage.setItem('scrollPosition', window.scrollY); // Guardamos la posición actual
    });
}

/** Inicializa la tabla de sesiones */
function initTable() {
    // Vista lista o esquema
    $('#link-scheme').click(function(){
        console.log("clic link-scheme");
        updateUrlHash("link-scheme", 't');
        showSchemeView();
    });
    $('#link-list').click(function(){
        console.log("clic link-list");
        updateUrlHash("link-list", 't');
        showListView();
    });
    // Vista lista o esquema, al principio se muestra la vista esquema (o si se
    // dio al botón atrás, se muestra la vista que se tenía)
    var t = getTypeFromHash();
    if (t) { $("#"+t).click(); }
    else { $('#link-list').click(); }

    // Tabs (Días), eventos
    $('.tabs li').click(function(ev){
        updateUrlHash($(this).attr("id"), "d");
        ev.preventDefault();

        showDay($(this));
    });
    // Tabs, al inicio se muestra el contenido de la primera pestaña o si se
    // llega con un pestaña predefinida se marca esa
    var tabId = getDayFromHash()
    if (tabId) {
        $("#" + tabId).click();
    } else {
        $('.tabs li:first-child').click();
    }
}

/** Muestra la vista esquema */
function showSchemeView() {
    $('#link-scheme').addClass('is-selected');
    $('#link-scheme').parent().addClass('is-selected');
    $('#link-list').removeClass('is-selected');
    $('#link-list').parent().removeClass('is-selected');
    $('.sessions-scheme').show();
    $('.sessions-table').hide();
    $("#tab-todos").hide();
    // si se estaba viendo todos, se muestra el primer día
    if ($("#tab-todos").hasClass('is-active') || (location.hash.includes("tab-todos"))) {
        showDay($("#tab-todos").next());
    }
    // evita que la URL tenga tab-todos y link-scheme, lo que llevaría a mostrar los tres días en modo esquema si se le da a recargar
    if (location.hash.includes("tab-todos") && location.hash.includes("link-scheme")) {
        let url = window.location.href;
        url = url.replace('|tab-todos', '');
        window.history.replaceState({}, '', url);
    }
    hidePostersTab();
}

/** Muestra la vista lista */
function showListView() {
    $('#link-list').addClass('is-selected');
    $('#link-list').parent().addClass('is-selected');
    $('#link-scheme').removeClass('is-selected');
    $('#link-scheme').parent().removeClass('is-selected');
    $('.sessions-scheme').hide();
    $('.sessions-table').show();
    $("#tab-todos").show();
    showPostersTab();
}

/** Muestra un día determinado */
function showDay($elem) {
    // selector activo
    $elem.parent().find('li').each(function(){
        $(this).removeClass('is-active');
    });
    $elem.addClass('is-active');

    //contenido visible
    var $parent = $('.section-sessions');
    $parent.find('.content-tab').hide();
    var id = $elem.attr("id");
    if (id != "tab-todos") {
        // muestra el tab
        $parent.find('#content-' + $elem.attr("id")).show();
        // asegura que se muestra la cabecera de la tabla
        $(".sessions-table .thead").show();
    }
    else {
        // muestra todos los tabs
        $(".content-tab").show();
        // solo mostrará la cabecera de la primera tabla
        var first = true;
        $(".sessions-table .thead").each(function(){
            if (!first) {
                $(this).hide();
            }
            first = false;
        });
    }
    checkHour();
}

/** En el formulario, selecciona el área cuyo id se pasa por parámetro.
    Devuelve verdadero en caso de que haya implicado un cambio y que se tenga que
    enviar el formulario para actualizar los resultados de búsqueda */
function selectArea(areaId) {
    if ($("#searchSelectArea select").val() == areaId) { return false; }
    $("#searchSelectArea select").val(areaId);
    return true;
}
/** En el formulario, pone en el campo texto el valor pasado por parámetro.
    Devuelve verdadero en caso de que haya implicado un cambio y que se tenga que
    enviar el formulario para actualizar los resultados de búsqueda */
function selectText(text) {
    if ($("#searchTextField input").val() == text) { return false; }
    $("#searchTextField input").val(text);
    return true;
}

/** Inicializa los manejadores de enviar el formulario */
function setOnSubmit() {
    setOnSubmitForm();
    setOnChangeSessiontype();
    setOnChangeArea();
    setOnChangeCongress();
    setOnChangeRoom();
    setOnChangeText();
}
function setOnSubmitForm() {
    $("#programSearch").submit(function(ev) {
        var $daySelected = $("#day-tabs .is-active").attr("id");

        ev.preventDefault();

        var form = $(this);
        var url = form.attr('action');

        $.ajax({
           type: "GET",
           contentType: "application/json",
           url: url,
           data: form.serialize(),
           dataType: 'JSON',
           complete: function(data) {
               var viewLinkId = ($("#link-scheme.is-selected").length) ? "link-scheme" : "link-list";
               console.log("completo");
               $('.section-sessions').replaceWith(data.responseText);
               initTable();
               $("#" + viewLinkId).click(); // para mostrar la vista que se tenía antes
               $("#" + $daySelected).click();//para mostrar la pestaña que estaba seleccionada antes

               $("#dropdownFilter").removeClass('is-selected');

               // recuadro que informa de lo que se ha buscado
               var htmlFilter = "";
               if (($("#searchSelectArea select").val() != null) && $("#searchSelectArea select").val() != "") {
                   htmlFilter = "<span class='has-text-weight-bold'>Categoría</span>: " + $("#searchSelectArea option:selected").text() + "<br>";
               }
               if ($("#searchTextField input").val() !== "") {
                   htmlFilter += "<span class='has-text-weight-bold'>Texto</span>: " + $("#searchTextField input").val();
               }
               if (htmlFilter == "") {
                   $("#action-bar-filter-info").hide();
               } else {
                   $("#action-bar-filter-info-text").html(htmlFilter);
                   $("#action-bar-filter-info").show();
               }
               // Se vuelven a inicializar los eventos de meter y quitar en la agenda
               Schedule.setScheduleHandlers();
               // Se marca el texto buscado
               markSearchedElements();
          }
       });
       checkClearFilterVisibility();
       updateUrlHash("form-text-" + $("#searchTextField input[name=texto]").val(), "form-text");
    })
}
function setOnChangeSessiontype() {
    $("select[name='tipo_sesion'],input[name='tipo_sesion']").change(function(){
        $("#programSearch").submit();
    });
}
function setOnChangeArea() {
    $("select[name='area'],input[name='area']").change(function(){
        console.log("change area");
        updateUrlHash("form-area-" + $(this).val(), "form-area");
        $("#programSearch").submit();
    });
}
function setOnChangeCongress() {
    $("select[name='programa']").change(function(){
        $("#programSearch").submit();
    });
}
function setOnChangeRoom() {
    $("select[name='sala']").change(function(){
        $("#programSearch").submit();
    });
}
function setOnChangeText() {
    // $("input[name='texto']").blur(function(){
    //     $("#programSearch").submit();
    // });
}

function setOnClearFilters() {
    console.log("borra filtros");
    $("#clear-filter").click(function(){
        updateUrlHash('', 'form-area');
        updateUrlHash('', 'form-text');
        $("input[name='texto']").val("");
        $("input[name='tipo_sesion']").removeAttr('checked');
        $("input[name='area']").removeAttr('checked');
        $("input[name='programa']").removeAttr('checked');
        $("input[name='sala']").removeAttr('checked');
        if ($("select[name='area']").length) {
            $("select[name='area'] option:selected").removeAttr("selected");
        }
        submitSearchForm();
    });
}

function submitSearchForm() {
    $("#programSearch").submit();
}

function checkDropdownfilterVisibility() {
    // console.log($( window ).width());
    if ($( window ).width() >= 768) {
        if ($("#dropdownFilter .dropdown-content").html() !== "") {
            $("#dropdownFilter .dropdown-content").contents().insertBefore("#clear-filter");
        }
    }
    else {
        if ($("#dropdownFilter .dropdown-content").html() == "") {
            $("#searchSelectArea").appendTo("#dropdownFilter .dropdown-content");
            $("#searchTextField").appendTo("#dropdownFilter .dropdown-content");
        }
    }
}

function checkClearFilterVisibility() {
    if ($("#searchSelectArea select").val() || $("#searchTextField input").val()) {
        $("#clear-filter").show();
    } else {
        $("#clear-filter").hide();
    }
    //if ($("#searchSelectArea select").val())
}

/** Actualiza el hash de la url añadiendo lo que se pasa por el primer parámetro
* El segundo indica si el hash hace referencia a: día, tipo de vista (esquema o tabla) , área o texto a buscar
* type puede ser "d", "t", "form-area", "form-text" */
function updateUrlHash(hash, type) {
    var h = getWindowLocationHash();
    var newHash = [];
    if (h) {
        h = h.substring(1);
        var partes = h.split("|");
        var entro = false;
        for (var i=0; i<partes.length; i++) {
            if (partes[i].startsWith("tab")) {
                if (type == "d") {
                    entro = true;
                    newHash.push(hash);
                }
                else {
                    newHash.push(partes[i]);
                }
            }
            else if (partes[i].startsWith("link")) {
                if (type == "t") {
                    entro = true;
                    newHash.push(hash);
                }
                else {
                    newHash.push(partes[i]);
                }
            }
            else if (partes[i].startsWith("form-area")) {
                if (type == "form-area") {
                    entro = true;
                    newHash.push(hash);
                }
                else {
                    newHash.push(partes[i]);
                }

            }
            else if (partes[i].startsWith("form-text")) {
                if (type == "form-text") {
                    entro = true;
                    newHash.push(hash);
                }
                else {
                    newHash.push(partes[i]);
                }

            }
        }
        if (!entro) {
            newHash.push(hash);
        }
    }
    else {
        newHash = [hash];
    }
    window.location.hash = newHash.join("|");
}

function getDayFromHash() {
    var windowLocationHash = getWindowLocationHash();
    if (windowLocationHash == '') { return false; }
    var partes = windowLocationHash.substring(1).split("|");
    for (var i = 0; i < partes.length; i++) {
        if (partes[i].startsWith("tab-") ) {
            return partes[i];
        }
    }
    return false;
}

function getTypeFromHash() {
    var windowLocationHash = getWindowLocationHash();
    if (windowLocationHash == '') { return false; }
    var partes = windowLocationHash.substring(1).split("|");
    for (var i = 0; i < partes.length; i++) {
        if (partes[i].startsWith("link-") ) {
            return partes[i];
        }
    }
    return false;
}

function getAreaFromHash() {
    var windowLocationHash = getWindowLocationHash();
    if (windowLocationHash == '') { return false; }
    var partes = windowLocationHash.substring(1).split("|");
    for (var i = 0; i < partes.length; i++) {
        if (partes[i].startsWith("form-area-") ) {
            return partes[i];
        }
    }
    return false;
}

function backBtnHandler() {
    // se entra en el siguiente bloque cada vez que se cambia el hash de la página
    if (window.history && window.history.pushState) {
        // window.history.pushState('forward', null, './#forward');
        $(window).on('popstate', function() {
            checkHash();
        });
    }
}

function ajustesVisuales() {
    $(".is-live").each(function() {
        $(this).appendTo($(this).closest(".tr").find(".isDay"));
    });
    ajustesSafariIphone();
}

function ajustesSafariIphone() {
    // ajustes para el iphone
    if (navigator.userAgent.indexOf('Safari') != -1 && navigator.userAgent.indexOf('Chrome') == -1) {
        var $elems = $(".skeduler-main-timeline > div, .skeduler-headers .scroller > div, .skeduler-task-placeholder");
        if ($(window).width() > 768) {
            // no móvil
            $elems.css("font-size", "100%");
            $(".sessions-scheme .name").removeClass("is-iphone");
        } else {
            // móvil
            $elems.css("font-size", "50%");
            $(".sessions-scheme .name")
            $(".sessions-scheme .name").addClass("is-iphone");
        }
    }
}

/** Muestra la vista que corresponda (con el filtro que corresponda) en función de lo que haya en el hash */
function checkHash() {
    var h = location.hash;
    h = decodeURI(h);
    var submitFormNeeded = false;
    if (h.length) {
        var items = h.substring(1).split("|");
        for (var i = 0; i < items.length; i++) {
            if (items[i].startsWith('tab-')) {
                showDay($("#" + items[i]));
            }
            else if (items[i] == 'link-scheme') {
                showSchemeView();
            }
            else if (items[i] == 'link-list') {
                showListView();
            }
            else if (items[i].startsWith('form-area-')) {
                if (selectArea(items[i].replace('form-area-', ''))) {
                    submitFormNeeded = true;
                }
            }
            else if (items[i].startsWith('form-text-')) {
                if (selectText(items[i].replace('form-text-', ''))) {
                    submitFormNeeded = true;
                }
            }
        }
        if (submitFormNeeded) {
            $("#programSearch").submit();
        }
    }
}

function checkDay() {
    var dayFromHash = getDayFromHash();
    // si en la URL ya viene indicado un día, no se hacen comprobaciones adicionales
    if (dayFromHash == "tab-todos") {
        // excepción al póster
        if (getAreaFromHash() !== "form-area-900") {
            // si el día de hoy tiene tab, lo marca
            if ($("#tab-" + nowDay).length) {
                console.log("cambia al día: " + nowDay);
                $("#tab-" + nowDay).click();
                // TODO: ir a la hora que corresponda (nowHour)
            }
        }
    }
}

function checkHour() {
    var dayFromHash = getDayFromHash();
    scrollToFirstLiveSession(dayFromHash, getTypeFromHash());
}

/** si hay sesiones en directo, hace scroll hasta la primera de ellas */
function scrollToFirstLiveSession(day, type) {
    if (type == "link-list") { type = "sessions-table"; }
    if (type == "link-scheme") { type = "sessions-scheme"; }
    var elem = $("#content-" + day);
    if (!elem.is(":visible")) {
        // No sé porqué, pero empieza con scroll. Esto lo pone arriba del todo
        $('html, body').animate({
            scrollTop: 0
        }, '1');
        return;
    }
    //var elemToScroll = $("#content-" + day + " ." + type + " .is-live");
    var elemToScroll = ($(window). width() > 768) ? elem.find("." + type + " .tr:not(.is-hidden-tablet) .is-live") : elem.find("." + type + " .tr:not(.is-hidden-mobile) .is-live");
    if (!elemToScroll.length) { return; }
    elemToScroll = $(elemToScroll[0]);
    var offset = $("#virtualNavbar").height() + $("#day-tabs-section").height() + $(".skeduler-headers").height();
    if (elemToScroll.length) {
        var $elemToScroll = $(elemToScroll[0]);
        if (type == "sessions-scheme") { $elemToScroll = $($elemToScroll.parents(".sessionLink")[0]); }
        if (type == "sessions-table") { $elemToScroll = $($elemToScroll.parents(".tr")[0]); }
        $('html, body').animate({
            scrollTop: $elemToScroll.offset().top - offset
        }, '300');
    }
}

function markSearchedElements() {
    // Marca en amarillo lo buscado
    var m = new Mark(document.querySelectorAll(".td, .name"));
    m.mark($("#searchTextField input").val(), {});

    // Actualiza el enlace a la sesión para que lleve información sobre lo buscado
    var text = $("#searchTextField input").val();
    if (text !== "") {
        $(".sessionLink").each(function() {
            console.log($(this).attr('href'))
            var url = new URL($(this).attr('href'));
            var search_params = url.searchParams;
            search_params.delete('searched_text');

            // add "searched_text" parameter
            search_params.set('searched_text', text);

            url.search = search_params.toString();

            var new_url = url.toString();

            $(this).attr('href', new_url)
        });
    }
}

/** Obtiene window.location.hash y lo procesa para evitar problemas de Safari */
function getWindowLocationHash() {
    var windowLocationHash = window.location.hash;
    windowLocationHash = windowLocationHash.replace("%7C", "|");
    windowLocationHash = windowLocationHash.replace("%7c", "|");
    return windowLocationHash;
}

/** Devuelve verdadero si se había hecho una búsqueda */
function hasPreviousSearch() {
    return ($("select[name='area']").val() || $("input[name='texto']").val());
}

/** Si hay algo seleccionado en el formulario de búsqueda, filtra los resultados */
function searchIfPreviousSearched() {
    if (hasPreviousSearch()) {
        submitSearchForm();
    }
}

// /** Si había una búsqueda previa, oculta la zona de sesiones */
// function hideSessionsIfPreviousSearched() {
//     if (hasPreviousSearch()) {
//         $(".section-sessions").css('opacity', '0.2');
//     }
// }
//
// /** Deshace el efecto de la función hideSessionsIfPreviousSearched */
// function unhideSessions() {
//     $(".section-sessions").css('opacity', '1');
// }

/**
 * Para que no use la caché de Safari al darle al botón atrás (que hace que se vea mal)
 * Esto hace que se solucione el problema pero se nota que está recargando la página
*/
function reloadOnBackButton() {
    $(window).bind("pageshow", function(event) {
        if (event.originalEvent.persisted) {
            var scrollPos = localStorage.getItem('scrollPosition');
            $("body").hide();
            setTimeout(function () {
                $("body").show();
                window.scrollTo(0, scrollPos);
            }, 500);
        }
    });
}

/** Muestra el tab de póster en caso de que exista y de que esté escondido */
function showPostersTab() {
    $("#tab-").show();
}

/** Oculta el tab de póster en caso de que exista. Si ahora mismo estaba seleccionado, se marca el tab todos */
function hidePostersTab() {
    var postersSelected = $("#tab-").hasClass('is-active');
    if (postersSelected) {
        $("#tab-").closest("ul").find('li:visible:first').click();
    }
    $("#tab-").hide();
}