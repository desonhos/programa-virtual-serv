<script>
    // Deshabilita console.log en producción (https://medium.com/@hfally/deactivate-console-log-on-production-why-and-how-d0345abbe71)
    const env = '<?php echo getenv("APP_ENV"); ?>';
    if (env === 'production') { console.log = function () {}; }
</script>
