<?php

return [
    'eventId' => 246, //235, // GEDOC23: 224, //EPC: 213, //222, // 217
    'filterTable' => 'areas', //programs, areas
    'apiKey' => 'H86foc28fgty1gcw73dsg61txt54h6',
    'cache' => [
        // 'infoevents' => [
        //     'duration' => env('PV_CACHE_INFOEVENTS_DURATION', 60) // número de segundos en los que permanece en caché la  petición de obtener todos los infoevents
        // ],
        'defaultDuration' => env('PV_CACHE_DEFAULT_DURATION', 60)
    ],
    'vimeo' => [
        'urlUser' => 'https://vimeo.com/user115542508/',
        'playerUrl' => 'https://player.vimeo.com/video/',
        'getJsonUrl' => 'https://vimeo.com/api/oembed.json?url=',
        'userId' => 104109593,
        'projectId' => 3228429  //se corresponde con la carpeta donde guarda los vídeos
    ],
    'loginUrl' => '',
    'event' => [
        'shortName' => env('EVENTO_NOMBRE_CORTO'),
        'description' => env('EVENTO_DESCRIPCION'),
        'pdfAttendanceCertificateLink' => 'event/certificado.pdf', // Sin barra al principio. Si hay problemas con el formato, convertirlo aquí: https://docupub.com/pdfconvert/ \ https://www.pdf2go.com/
        // 'pdfAttendanceCertificateLink' => null,
        'defaultImg' => '/event/virtual-platform-ad.jpg'
    ],
    'player' => [
        'storeViewSecs' => env('PV_PLAYER_STORE_VIEW_SECS', 60),
    ],
    'ratings' => [
        'maxScore' => 5
    ],
    'topbarDark' => false,
    'topbarDarkLogin' => false,
    'hasWebapp' => true
];

?>
