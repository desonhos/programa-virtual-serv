<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Session;
use App\Models\Area;
use App\Models\Sessiontype;
use App\Models\Room;
use App\Models\Congress;
use App\Models\Paper;
use App\Models\View;

class ProgramController extends Controller {
    //
    public function view() {
        $sessions = Session::_getAll();
        $days = Session::_getDaysOfSessions($sessions);
        $areas = Area::_getAll();
        $sessiontypes = Sessiontype::_getAll();
        $rooms = Room::_getAll();
        $congresses = Congress::_getAll();
        $user = session("user");
        $currentUserViews = !\SettingHelper::isApp() ? View::_getAllWhereCurrentUser() : [];
        $sessionDataForScheme = Session::_getDataForScheme($sessions, $rooms, $sessiontypes, $areas, $currentUserViews);
        return view('program', compact(
            'sessions', 'days', 'areas', 'sessiontypes', 'rooms', 'congresses',
            'user', 'sessionDataForScheme', 'currentUserViews'
        ));
    }

    public function search(Request $request) {
        $sessions = Session::_search($request->all());
        $days = Session::_getDaysOfSessions($sessions);
        $areas = Area::_getAll();
        $sessiontypes = Sessiontype::_getAll();
        $rooms = Room::_getAll();
        $congresses = Congress::_getAll();
        $oldRequest = $request->all();
        $currentUserViews = !\SettingHelper::isApp() ? View::_getAllWhereCurrentUser() : [];
        $sessionDataForScheme = Session::_getDataForScheme($sessions, $rooms, $sessiontypes, $areas, $currentUserViews);
        return response(
            view('program.sessions', compact(
                'sessions', 'days', 'areas', 'sessiontypes', 'rooms', 'congresses',
                'oldRequest', 'sessionDataForScheme', 'currentUserViews'
            )),
            200,
            ['Content-Type' => 'application/json']
        );
    }

    public function searchVirtual(Request $request) {
        return $this->search($request, true);
    }

    /** Función a la que se tiene que llamar cuando se han subido los vídeos a Vimeo
    * y se desea que automáticamente se guarden las URL que llevan a ellos */
    public function generatePapersVimeoUrls() {
        $papers = Paper::_getAll();
        $nVideosStored = 0;
        $perPage = 100;
        $page = 1;
        $res = \Vimeo::request('/users/'.config('web.vimeo.userId').'/projects/'.config('web.vimeo.projectId').'/videos', ['per_page' => $perPage, 'page' => $page, 'sort' => 'alphabetical', 'direction' => 'asc'], 'GET');
        $data = (isset($res['body']['data'])) ? $res['body']['data'] : null;
        $total = (isset($res['body']['total'])) ? $res['body']['total'] : 0;
        if ($total > 0) {
            while (true) {
                foreach ($papers as $paper) {
                    if ($paper->generateVimeoUrl($data)) {
                        $nVideosStored++;
                    }
                }
                $page++;
                $res = \Vimeo::request('/users/'.config('web.vimeo.userId').'/projects/'.config('web.vimeo.projectId').'/videos', ['per_page' => $perPage, 'page' => $page, 'sort' => 'alphabetical', 'direction' => 'asc'], 'GET');
                $data = (isset($res['body']['data'])) ? $res['body']['data'] : null;
                if (empty($data)) { break; }
            }
        }
        echo "Se han generado " . $nVideosStored . " de un total de " . $total . " que hay en la carpeta de Vimeo<br>";
        if ($nVideosStored < $total) {
            echo "<br><b>Revisa</b>:<ul><li>que los nombres de los vídeos corresponden con el id de las ponencias</li><li>que en la base de datos están insertadas todas las ponencias</li></ul>";
        }
    }
}
