<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Storage;

class Sponsor extends Model {

    public static function _get($sponsorId) {
        return Sponsor::where('id', $sponsorId)->first();
    }

    public static function _getAll() {
        return Sponsor::orderBy('pos')->get();
    }

    public function getName() { return $this->name; }
    public function getLogo() {
        $logoUrl = $this->logo;
        if (str_starts_with($logoUrl, "http")) {
            return $logoUrl;
        } else {
            return Storage::disk(config('voyager.storage.disk'))->url($logoUrl);
        }
    }
    public function getNstand() { return $this->nstand; }
    public function getUrl() { return $this->url_web; }
    public function getDescription() { return $this->description; }
    public function getAdditionalStyle() { return $this->css; }
}
