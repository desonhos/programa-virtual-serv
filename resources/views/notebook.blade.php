@extends('layouts.app' )

@section('css')
    @include('partials.dynamic-styles')
@endsection

@php
$pageClass = 'page-notebook';
if (!\SettingHelper::isApp()) { $pageClass .= ' is-congreso-virtual'; }
@endphp
@section('page-class', $pageClass)

@php $notes = new \App\Models\Notebook(); @endphp

@section('content')
    <article>
        <section class="section section-sessions">
            <div class="container">
                <h1 class="title is-uppercase">@svg('ico-notas', 'ico-notas') {{ __('Mis notas') }}</h1>
                @if (count($sessions) == 0)
                    <div class="mt-2">
                        {{ __('Añade anotaciones sobre las sesiones que más te interesen.') }}
                    </div>
                    <a href="{{ route('program.view') }}" class="button mt-4">{{ __('Ver programa') }}</a>
                @else
                    @foreach ($sessions as $session)
                        <p class="has-border-bottom-1 pb-4 mb-4">
                            <a class="anotar has-text-weight-bold has-text-black is-block"
                                href="{{ route('sessions.modifyNote', ['id' => $session->getId() ]) }}"
                                data-get-note="{{ route('session.getNote', ['id' => $session->getId() ]) }}">
                                {!!FilterHelper::_getFilterNameOfSession($sessiontypes, $areas, $session)!!}: {!! $session->getName() !!}
                            </a>
                            <span class="is-block noteTextToUpdate">
                                {{ $notes->getSessionNote($session->getId()) }}
                            </span>
                        </p>
                    @endforeach
                    @include('notes._modal')
                @endif
            </div>
        </section>
    </article>
@endsection
