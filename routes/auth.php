<?php

use Illuminate\Support\Facades\Route;

Route::get('/acceso-congreso-virtual', 'UserController@showLogin')->name('login');
Route::post('/acceso-congreso-virtual', 'UserController@virtualEventLogin')->name('login.virtual');
Route::get('/recordar-contrasena', 'UserController@showRememberPass')->name('remember-password.show');
Route::post('/recordar-contrasena', 'UserController@rememberPass')->name('remember-password');
// Route::post('/acceso-logievents', 'UserController@login')->name('users.external-login');
