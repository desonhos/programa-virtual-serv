function generateSkedulerDia(idDia, sesiones, idsOrdenadosSalas) {
    var sesiones = JSON.parse(sesiones);
    var sesionesDia = sesiones[idDia];
    var salasDia = [];
    idsOrdenadosSalas = JSON.parse(idsOrdenadosSalas);
    for (var i in idsOrdenadosSalas) {
        var idSala = idsOrdenadosSalas[i];
        if (typeof(sesionesDia[idSala]) !== "undefined") {
            salasDia.push(sesionesDia[idSala][0].room_name);
        }
    }
    var sesionesDia = getSesionsParaEsquema(idDia, salasDia, sesionesDia, idsOrdenadosSalas);
    var horaInicio = getMinHour(sesionesDia);
    var horaFin = getMaxHour(sesionesDia);
    var params = {
        headers: salasDia,
        tasks: sesionesDia,
        // cardTemplate: '\
        //     <a id="esquema-${id}" class="is-${color} session-scheme" href="${url}">\n\
        //         <div class="category">${categoria}</div>\n\
        //         <div class="info">\n\
        //             <div class="name">${nombre_sesion}</div>\n\
        //             <div class="hour">${hora}</div>\n\
        //             <div class="category-secondary">${categoria_secundaria}</div>\n\
        //         </div>\n\
        //     </a>',
        cardTemplate: '\
            <a id="esquema-${id}" class="is-${color} session-scheme sessionLink ${visited}" href="${url}">\n\
                <div class="category">${categoria}</div>\n\
                <div class="info">\n\
                    <div class="name">${nombre_sesion}</div>\n\
                    <div class="hour">${hora}</div>\n\
                </div>\n\
            </a>',
        lineHeight: 100, //por defecto es 30 | ALTO_LINEA_SKEDULER
        horaInicio: horaInicio,
        horaFin: horaFin,  //pintaba las sesiones más abajo y se optó por ocultarlas
        anchoColumna: 120, //ponerlo también en el css
        // onClick: function (e, t) {
        //     console.log(e, t);
        // }
    };
    console.log("#skeduler-"+idDia);
    $("#skeduler-"+idDia).skeduler(params);
}

function getMinHour(sesionesDia) {
    var min = 21;
    for (var i in sesionesDia) {
        if (sesionesDia[i].startTime < min) { min = Math.floor(sesionesDia[i].startTime); }
    }
    return min;
}

function getMaxHour(sesionesDia) {
    var max = 8;
    var fin = 0;
    for (var i in sesionesDia) {
        if (sesionesDia[i].duration < 0) {
            // casos raros como el de la cena, que acaba de madrugada
            fin = 24;
        } else {
            fin = sesionesDia[i].startTime + sesionesDia[i].duration;
        }
        if (fin > max) { max = Math.ceil(fin); }
    }
    return max;
}

function getSesionsParaEsquema(idDia, salasDia, sesionesDia, idsOrdenadosSalas) {
    var res = [];
    var posSala = 0;
    for (var i in idsOrdenadosSalas) {
        var idSala = idsOrdenadosSalas[i];
        if (typeof(sesionesDia[idSala]) == "undefined") { continue; }
        var sesionesDiaSala = sesionesDia[idSala];
        for (var i in sesionesDiaSala) {
            var sesion = sesionesDiaSala[i];
            var horaInicio = (sesion.init_hour !== null) ? sesion.init_hour.substr(0,5) : "";
            var horaFin = (sesion.end_hour !== null) ? sesion.end_hour.substr(0,5) : "";
            var hora = "";
            if ((horaFin !== "") && (horaInicio !== "")) { hora = horaInicio + " - " + horaFin; }
            else if (horaInicio !== "") { hora = horaInicio; }
            else if (horaFin !== "") { hora = " - " + horaFin; }
            res.push({
                startTime: skeduleFormat_startTime(sesion),
                duration: skeduleFormat_getDuration(sesion),
                column: posSala,
                id: sesion.id,
                area_id: sesion.area_1,
                tipo_sesion: sesion.sessiontype_name,
                hora_inicio: horaInicio,
                hora_fin: horaFin,
                nombre_sesion: sesion.name,
                visited: sesion.visited,
                hora: hora,
                moderador: sesion.moderators_string,
                patrocinador: sesion.organization,
                color: sesion.color,
                url: sesion.url,
                categoria: sesion.category,
                categoria_secundaria: sesion.category2
            });
        }
        posSala++;
    }
    return res;
}
function skedule_formatTime(hora){
    if (hora == null) { return ""; }
    hora = hora.substring(0, 5);
    var partes = hora.split(":");
    var minutos = partes[1];
    hora = partes[0];

    hora = parseInt(hora);
    minutos = parseInt(minutos)/60;
    return (hora + minutos);
}

function skeduleFormat_startTime(sesion){
    var hora = sesion.init_hour;
    return skedule_formatTime(hora);
}

/** Devuelve la duración en minutos en formato decimal (0.5 = media hora) de una sesion */
function skeduleFormat_getDuration(sesion){
    var horaInicio = skedule_formatTime(sesion.init_hour);
    var horaFin = skedule_formatTime(sesion.end_hour);
    return (horaFin - horaInicio);
}
