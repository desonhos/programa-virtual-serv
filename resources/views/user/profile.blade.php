@extends('layouts/virtual')

@section('page-class', 'page-virtual is-profile')

@section('content')
    <section class="section">
        <div class="container">
            <div class="content">
                {{-- <h1>Perfil de usuario </h1> --}}
                <form method="POST" action="{{ route('user.update-profile') }}">
                    @csrf
                    @php
                        $user = Auth::guard('multi')->user();
                    @endphp
                    @if (!empty($user))
                        <h2>{{__("Datos personales")}}</h2>
                        <div class="field is-horizontal">
                            <div class="field-label is-normal">
                                <label class="label">{{__("Nombre")}}</label>
                            </div>
                            <div class="field-body">
                                <div class="field">
                                    <input type="text" class="input" name="name" placeholder="{{__("Nombre")}}" value="{{ $user->getName() }}" disabled>
                                </div>
                            </div>
                        </div>
                        <div class="field is-horizontal">
                            <div class="field-label is-normal">
                                <label class="label">{{__("Primer apellido")}}</label>
                            </div>
                            <div class="field-body">
                                <div class="field">
                                    <input type="text" class="input" name="surname" placeholder="{{__("Primer apellido")}}" value="{{ $user->getSurname() }}" disabled>
                                </div>
                            </div>
                        </div>
                        @php $surname2 = $user->getSurname2(); @endphp
                        @if (!empty($surname2))
                            <div class="field is-horizontal">
                                <div class="field-label is-normal">
                                    <label class="label">{{__("Segundo apellido")}}</label>
                                </div>
                                <div class="field-body">
                                    <div class="field">
                                        <input type="text" class="input" name="surname2" placeholder="{{__("Segundo apellido")}}" value="{{ $user->getSurname2() }}" disabled>
                                    </div>
                                </div>
                            </div>
                        @endif
                        <div class="field is-horizontal">
                            <div class="field-label is-normal">
                                <label class="label">{{ __("Correo electrónico") }}</label>
                            </div>
                            <div class="field-body">
                                <div class="field">
                                    <input type="email" class="input" name="email" placeholder="{{__("Correo electrónico")}}" value="{{ $user->getEmail() }}" disabled>
                                </div>
                            </div>
                        </div>
                    @endif
                    <h2>{{__("Cambio de contraseña")}}</h2>
                    <div class="field is-horizontal">
                        <div class="field-label is-normal">
                            <label class="label">{{__("Contraseña actual")}}</label>
                        </div>
                        <div class="field-body">
                            <div class="field">
                                <input type="password" class="input" name="password" placeholder="{{__("Contraseña actual")}}" value="{{ old('password') }}" required>
                            </div>
                        </div>
                    </div>
                    <div class="field is-horizontal">
                        <div class="field-label is-normal">
                            <label class="label">{{__("Nueva contraseña")}}</label>
                        </div>
                        <div class="field-body">
                            <div class="field">
                                <input type="password" class="input" name="password_new" placeholder="{{__("Nueva contraseña")}}" value="{{ old('password_new') }}" required>
                            </div>
                        </div>
                    </div>
                    <div class="field is-horizontal">
                        <div class="field-label is-normal">
                            <label class="label">{{__("Nueva contraseña (repetir)")}}</label>
                        </div>
                        <div class="field-body">
                            <div class="field">
                                <input type="password" class="input" name="password_new2" placeholder="{{__("Nueva contraseña (repetir)")}}" value="{{ old('password_new2') }}" required>
                            </div>
                        </div>
                    </div>

                    <div class="field">
                        <div class="control mt-6 has-text-right	">
                            <input class="button is-primary" type="submit" value="{{__("Guardar")}}">
                        </div>
                        @if ($errors->any())
                            <p class="help is-danger">
                                {{$errors->first()}}
                            </p>
                        @endif
                        @if (\Session::has('success'))
                            <p class="help is-success">
                                {!! \Session::get('success') !!}</li>
                            </p>
                        @endif
                    </div>
                </form>
            </div>
        </div>
    </section>
@endsection
