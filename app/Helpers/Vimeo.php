<?php

namespace App\Helpers;

class Vimeo {
    public static function getVideoThumbnail($videoId) {
        return Vimeo::get_vimeo_data_from_id( $videoId, 'thumbnail_url' );
    }

    /**
    * Grab the specified data like Thumbnail URL of a publicly embeddable video hosted on Vimeo.
    *
    * @param  str $video_id The ID of a Vimeo video.
    * @param  str $data 	  Video data to be fetched
    * @see https://wpdevdesign.com/how-to-get-thumbnail-url-and-other-info-from-a-vimeo-id-in-wordpress/
    * @return str            The specified data
    */
    public static function get_vimeo_data_from_id( $video_id, $data ) {
        $contents = @file_get_contents( 'https://vimeo.com/api/oembed.json?url=https://vimeo.com/' . $video_id );
        $video_array = json_decode( $contents, true );

        return $video_array[$data];
    }
}
