@if (!\SettingHelper::isApp() && !empty($relatedSessions) && count($relatedSessions))
    <section class="mt-6 is-relatedContent">
        <div class="container">
            <h2 class="title is-4">{{ $session->getRelatedSessionsTitle() }}</h2>
            <div class="columns is-multiline">
                @foreach ($relatedSessions as $relatedSession)
                    <div class="column is-one-third">
                        @include('session/_session-in-list', ['session' => $relatedSession])
                    </div>
                @endforeach
            </div>
        </div>
    </section>
@endif
