# Comandos para actualizar en servidor de producción

La primera línea es la carpeta pública y la segunda la parte privada

```
rsync -rv --exclude .DS_Store --exclude index.php --exclude storage public/ virtualcongresoserv@virtual.congresoserv.org:www/
rsync -rv --exclude .DS_Store --exclude .editorconfig --exclude .env --exclude .git --exclude .gitattributes --exclude .gitignore --exclude .styleci.yml --exclude public --exclude storage --exclude vendor/laravel/framework/src/Illuminate/Session/DatabaseSessionHandler.php . virtualcongresoserv@virtual.congresoserv.org:backend/
```

contraseña:
ComoH3mosKmbiau!

## Elementos ignorados al actualizar pero que deberían subirse la primera vez
--exclude .htaccess del primer comando
--exclude storage del segundo comando
--exclude vendor/laravel/framework/src/Illuminate/Session/DatabaseSessionHandler.php

## Cambios en la carpeta vendor. A tener en cuenta al hacer composer install o composer update

TODO: Hacer overrides o mejor aún sería investigar si se pueden quitar de alguna manera cambiando la programación

### VoyagerMediaController

En el panel de administración, si da error la pestaña "medios", ir a: vendor > tcg > voyager > src > Http > Controllers > VoyagerMediaController y comentar estas líneas
    // if (class_exists(\League\Flysystem\Plugin\ListWith::class)) {
        //     $storage = Storage::disk($this->filesystem)->addPlugin(new \League\Flysystem\Plugin\ListWith());
        //     $storageItems = $storage->listWith(['mimetype'], $dir);
        // } else {
            $storage = Storage::disk($this->filesystem);
            $storageItems = $storage->listContents($dir)->sortByPath()->toArray();
        // }

Quizás esto se arregle al actualizar a Laravel Voyager 1.7

### DatabaseSessionHandler

Quizás cuando se actualice a Laravel 10 esto se arregle solo. Para probarlo bastaría con quitar ese código añadido.
El añadido está entrecerrado con // añadido en la función userId() de: vendor/laravel/framework/src/Illuminate/Session/DatabaseSessionHandler.

protected function userId()
    {
        // añadido
        $user = \Auth::guard('multi')->user();
        if (!empty($user)) { return $user->id; }
        // fin añadido

        return $this->container->make(Guard::class)->id();
    }

Esto se hizo porque tras actualizar de Laravel 8 a Laravel 9 empezó a no guardarse el user_id en la tabla de sesiones. Eso es necesario para poder cerrar el resto de sesiones abiertas cuando se hace login y así evitar que con una misma cuenta estén entrando dos personas a la vez desde navegadores/equipos distintos

# Clonado de la plataforma

- Crear el hosting
- Crear un proyecto en Bitbucket
- Ejecutar el comando clone para traerte ese proyecto de Bitbucket
- Dar de alta el proyecto en Billage y crear carpeta en traballo
- Apuntar datos de acceso a: BitBucket, hosting, FTP y Base de Datos
- En la carpeta del proyecto en local, copiar todos los archivos excepto .git y .gitignore (modificar el .gitignore para que no suba cosas innecesarias)
- Hacer el primer comit
- Modificar el archivo README.md con el comando adecuado para actualizar el código
- Configurar el servidor local
- Crear en local el archivo .env y probar que se puede navegar en local (ponerle los parámetros buenos)
- Utilizar los comandos del paso anterior para subir los archivos
- Manualmente, en el servidor de producción:
    - crear el archivo index.php dentro del directorio web y ajustar las rutas relativas
    - crear el archivo .env
        - ojo de poner en producción, sesiones en base de datos, datos del correo, etc
- Crear arriba el enlace simbólico de storage
    - El comando a ejecutar si tuviera permisos sería algo como esto: ln -s ~/cnl/storage/app/public ~/httpdocs/storage
    - Si no se puede ejecutar lo de antes:
        - Crear en local dos carpetas hermanas: backend y www.
        - Dentro de programa: storage/app/public
        - En la línea de comandos:
            cd www
            ln -s ../backend/storage/app/public storage
        - Subirlo con el comando: rsync -rvl /Users/pablo/_enlaces_backend/www/storage . virtualepc2024@virtual.epc2024.eu:www/
- Crear arriba el enlace simbólico de public (https://docs.google.com/document/d/1S_OynoplMOeD67-_9wprwDFKaBb_xLEvmupXq_laRLs/edit#heading=h.3wwurtgil9nj)
- Cambiar los parámetros de configuración:
    - config/web.php
- Copiar el contenido de la base de datos
- En el panel de administración, si da error la pestaña "medios", ir a: vendor > tcg > voyager > src > Http > Controllers > VoyagerMediaController y comentar estas líneas
    // if (class_exists(\League\Flysystem\Plugin\ListWith::class)) {
        //     $storage = Storage::disk($this->filesystem)->addPlugin(new \League\Flysystem\Plugin\ListWith());
        //     $storageItems = $storage->listWith(['mimetype'], $dir);
        // } else {
            $storage = Storage::disk($this->filesystem);
            $storageItems = $storage->listContents($dir)->sortByPath()->toArray();
        // }
- Crear la cuenta de FTP para que suban los adjuntos
    - En www se crea una carpeta llamada archivos, que es a donde apuntará la cuenta FTP secundaria
    - Se crea la cuenta y se apuntan sus accesos para pasárselos a CYEX
    - Se le pasan unas instrucciones como estas:
        Hola, Aldara.

        Estos son los datos para que puedas crear la cuenta FTP para subir los archivos para el programa virtual del congreso SEC 2024:

        host/servidor: programa.congresosec.org
        usuario: archivossec
        contraseña: 1UpMn4@768

        Lo que ahí se suba (por ejemplo, si se sube el archivo a.txt) será accesible con la dirección https://programa.congresosec.org/archivos/a.txt. Si creases una carpeta llamada 2024 y en ella subieses un archivo, la URL a la que se accedería al mismo sería: https://programa.congresosec.org/archivos/2024/a.txt

        Un saludo.

        PD: Otros parámetros que pide Filezilla pero que puedes dejar por defecto, son:

        Protocolo: FTP - Protocolo de Transferencia de Archivos
        Cifrado: Usar FTP explícito sobre TLS si está disponible
        Modo de acceso: normal

- Pedir a Su el material, que será como este: /Volumes/traballo/CLIENTES/AED_Dermatologia/AEDV_ReumaDerm/AED217_Jornada AEDV-SER/AED217_PlataformaVirtual/AED217_3_Desenho pero para el congreso correspondiente

- Desde el panel de administración de la página, configurar https://programa.congresoaedv.net/admin/settings

# Actualización entre años

- Actualizar imagen, colores, etc.
- config/web.php, cambiar los parámetros que correspondan (fundamental eventId)
- Todo lo que pone en config/laravelpwa.php

## Pedir a CYEX

- Certificado de asistencia (y probar que la posición del nombre)

# Comprobaciones antes del congreso

## Servidor

- ¿Cloud?
    - Fechas de levantado
    - Escalado
- .env
    - APP_ENV=production | APP_DEBUG=false
    - BD
        - host=localhost (en ambas a no ser que dataeventservices.net esté alojado en otra máquina)
    - SESSION_DRIVER=database
    - SESSION_LIFETIME=525600 (+ comprobar que no haya limitación por otro sitio. Por ejemplo, Dinahosting para session.gc_maxlifetime tiene como valor máximo: 10000 pero por defecto viene con bastante menos. Ver phpinfo)
    - Cuenta email

## Otros parámetros
- Ver el archivo config/web.php

## Envío de preguntas
- Primero coger una sesión en concreto y poner como mailcontact a pablo.desonhos@gmail.com. Entonces probar a hacer una pregunta
- Comprobar que durante determinadas horas se muestra el livechat y fuera de ellas se muestra el hacer pregunta

## Contador de visualizaciones
Coger una sesión, hacer como que se ve hoy, ponerle otro vídeo (por ejemplo: https://vimeo.com/manage/videos/640641121) y comprobar si va el contador de visualizaciones.

este es el bueno: https://vimeo.com/event/1427939

select *
from pv_views
where event_id=135
and session_id=966

select *
from sessions
where event_id=142
and id=966

Otra cosa a tener en cuenta es que: resources/views/session/video/player-events-js-blade.php tiene el intervalo en el que almacena (en SEC lo subí a 120 segundos para bajar las peticiones al servidor)

# Documentación y comentarios sobre el código

## Visor de vídeos, visibilidad

si webapp
    subvista: partials.virtual-platform-ad
        si pv_no_video == 2 (PV_VIDEO_HIDDEN)
            No muestra nada
        si no
            MOSTRAR PÍLDORA
            si ya pasó la fecha de inicio del congreso
                Mensaje de «Sigue desde la webapp» con enlace
            si no
                Mensaje de «Acceso a partir del»
            fin si
        fin si
si no
    subvista: session._video
        si pv_no_video == 2 (PV_VIDEO_HIDDEN)
            No muestra nada
        si no, si pv_no_video == 1 (PV_WARN_NO_VIDEO)
            subvista: session.video.session-without-video
                Te muestra un mensaje de que no hay vídeo para esa sesión
        si no
            si sesión a la que no tiene acceso el usuario actual
                subvista: session.video.restricted-session
                    Tiene simplemente un mensaje de que no tienes acceso
            si no
                subvista: session.video.player
                    MOSTRAR PÍLDORA
                    Si aún no es la hora
                        Muestra la cuenta atrás
                    si no
                        Muestra el vídeo normal
                    fin si
            fin fin
        fin si
fin si

## ¿En qué modo estamos?
Desde el panel de administración se definen dos fechas. La primera establece cuándo comienza el acceso virtual y la segunda cuándo se ha terminado el acceso presencial.
Cada asistente al congreso tiene un tipo de acceso, que puede ser: A,V y P
- P: solo tiene acceso presencial, por lo que solo podrá ver el modo app
- V: solo tiene acceso virtual, esto significa que en la plataforma:
    - Podrá acceder tras la fecha de comienzo de acceso virtual
    - Cuando accede verá la plataforma en modo PV
- A: tiene acceso presencial y virtual. Para la plataforma esto significa que:
    - Podrá acceder trasla fecha de comienzo de acceso virtual
    - Al acceder, el modo en el que verá la plaforma será:
        - AppPV si ya hay acceso virtual pero aún hay acceso presencial
        - PV si ya hay acceso virtual pero no presencial

Seguramente esté pendiente de cambiar en el código el nombre de esos dos parámetros porque inicialmente le había entendido a Antonio que esos tres estados no podrían convivir en el tiempo, pero resulta que sí (durante el congreso, los que solo tienen acceso virtual verán la plataforma en modo PV y los que tienen acceso virtual y presencial verán la plataforma en modo AppPV)