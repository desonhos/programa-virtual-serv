<ul class="links-list is-horizontal mt-0 mb-0 has-background-grey-light" id="userActions">
    <li class="is-flex is-align-items-center li-notas">
        <a class="is-text-link" id="link-notas" href="{{ route('notas') }}">
            @svg('ico-notas', 'ico-notas')
        </a>
    </li>
    <li class="is-flex is-align-items-center ml-0 li-agenda">
        <a class="is-text-link" id="link-agenda" href="{{ route('agenda') }}">
            @svg('ico-agenda', 'ico-agenda')
        </a>
    </li>
</ul>
