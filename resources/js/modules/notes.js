import Alert from '@vizuaalog/bulmajs/src/plugins/alert';

$(document).ready(function() {
    setNotesHandlers();
});

function setNotesHandlers(getNoteUrl) {
    $(".anotar").click(function(ev) {
        // obtiene la nota actual para mostrarla en el cuadro de diálogo
        ev.preventDefault();
        var $link = $(this);
        var url = $link.attr('href');
        var getNoteUrl = $(this).data('get-note');
        var $noteTextToUpdate = $(this).parent().find('.noteTextToUpdate');
        $.ajax({
            type: "GET",
            url: getNoteUrl,
            complete: function(data) {
                $("#notesModal textarea[name=noteText]").val(data.responseText);
                $("#notesModal .submit").unbind('click').click(function() {
                    saveNote($("#notesModal textarea[name=noteText]").val(), url);
                    if (typeof($noteTextToUpdate) !== "undefined") {
                        $noteTextToUpdate.first().html($("#notesModal textarea[name=noteText]").val());
                    }
                });
                $("#notesModal").addClass('is-active');
            }
        });
    });
    $("#notesModal .modal-background, #notesModal button.delete, #notesModal .cancel").click(function() {
        $(this).closest('.modal').removeClass('is-active');
    });
    $(".deleteNote").click(function() {
        $("#notesModal textarea[name=noteText]").val('');
        $("#notesModal .submit").click();
    })
}
function saveNote(text, url) {
    $.ajax({
        type: "GET",
        url: url,
        data: { 'noteText': text },
        complete: function(data) {
            //alert("guardada la nota");
            Alert().alert({
                type: 'warning',
                title: (lang == 'es') ? 'Éxito' : 'Success',
                body: (lang == 'es') ? 'Cambios realizados correctamente' : 'Changes made successfully',
                confirm: {
                    label: (lang == 'es') ? 'Seguir editando' : 'Continue editing',
                    classes: ['is-link', 'pl-0', 'letter-spacing-0', 'has-border-0'],
                    onClick: function() {

                    }
                },
                cancel: {
                    label: (lang == 'es') ? 'Cerrar nota' : 'Close note',
                    onClick: function() {
                        $("button.delete").click();
                    }
                }
            });
        }
    });
}
