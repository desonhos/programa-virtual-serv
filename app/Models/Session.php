<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\App;


class Session extends Model {

    protected $connection= 'cyex_online';
    public const SURVEY_MONKEY = 'SURVEY_MONKEY';
    public const CYEX = 'CYEX';
    public const PV_SHOW_VIDEO = 0; // el visor y el vídeo se muestran
    public const PV_WARN_NO_VIDEO = 1;  // se muestra un visor pero advirtiendo de que no está prevista la reproducción de ningún vídeo
    public const PV_VIDEO_HIDDEN = 2;   // no se muestra ningún visor

    /** Obtiene sesión por su id, del evento definido en config */
    public static function _get($sessionId) {
        return Cache::remember('session_id_'.$sessionId, config('web.cache.defaultDuration'), function () use ($sessionId) {
            $session = Session::where('id', $sessionId)->where('event_id', config('web.eventId'))->first();
            $user = \Auth::guard('multi')->user();
            if ($user && !$session->isAvailableForCurrentUser()) {
                return false;
            }
            return $session;
        });
    }

    /** Igual que _get pero sin controlar el usuario */
    public static function _getAnonymously($sessionId) {
        return Cache::remember('session_id_'.$sessionId, config('web.cache.defaultDuration'), function () use ($sessionId) {
            $session = Session::where('id', $sessionId)->where('event_id', config('web.eventId'))->first();
            return $session;
        });
    }

    /** Obtiene todas las sesiones del evento actual */
    public static function _getAll() {
        $pvHideField = \SettingHelper::getHideFieldName();
        $sessions = Cache::remember('sessions_' . $pvHideField, config('web.cache.defaultDuration'), function () use ($pvHideField) {
            return Session::select(\DB::connection('cyex_online')->raw('sessions.*, programs.name as program_name'))
            // $sessions = Session::select(\DB::connection('cyex_online')->raw('sessions.*, programs.name as program_name'))
                        ->where('sessions.event_id', config('web.eventId'))
                        ->where(function ($q) use ($pvHideField) {
                            return $q->whereNull($pvHideField)
                                ->orWhere($pvHideField, '!=', 1);
                        })
                        ->join('programs', function($join){
                            $join->on('programs.id', '=', 'sessions.program_id')
                                    ->on('programs.event_id', '=', 'sessions.event_id');
                        })
                        ->join('rooms', function($join){
                            $join->on('rooms.id', '=', 'sessions.room_id')
                                    ->on('rooms.event_id', '=', 'sessions.event_id');
                        })
                        ->orderBy('day_format2')
                        ->orderBy('init_hour')
                        ->orderBy('rooms.orden')
                        ->orderBy('sessions.name')
                        ->get();
        });

        $res = [];

        $user = \Auth::guard('multi')->user();
        foreach ($sessions as $session) {
            $session['objectives'] = str_replace('"', "'", $session['objectives']);
            if ($user && !$session->isAvailableForCurrentUser()) {
                // estos resultados no los queremos
            }
            else {
                array_push($res, $session);
            }
        }

        return $res;
    }

    /** Busca la sesión por su id dentro del listado pasado por parámetro */
    public static function _getFromList($sessionId, $sessions) {
        foreach ($sessions as $session) {
            if (($session->getId() == $sessionId) && ($session->getEventId() == config('web.eventId'))) {
                return $session;
            }
        }
        return null;
    }

    /** Obtiene un listado con los días únicos de las sesiones pasadas */
    public static function _getDaysOfSessions($sessions) {
        $days = [];
        foreach ($sessions as $session) {
            if (!in_array($session->day_format2, $days)) {
                array_push($days, $session->day_format2);
            }
        }
        sort($days);
        $days = array_diff($days, array('')) + array_intersect($days, array(''));
        return $days;
    }

    /** Busca las sesiones del evento actual que cumplan los criterios de búsqueda pasados */
    public static function _search($params) {
        $pvHideField = \SettingHelper::getHideFieldName();
        $searchedWords = (!empty($params['texto'])) ? preg_split('/\s+/', $params['texto'], -1, PREG_SPLIT_NO_EMPTY) : null;
        $sessiontype = (!empty($params['tipo_sesion'])) ? $params['tipo_sesion'] : null;
        $area = (!empty($params['area'])) ? $params['area'] : null;
        $congress = (!empty($params['programa'])) ? $params['programa'] : null;
        $room = (!empty($params['sala'])) ? $params['sala'] : null;
        $sessions = Session::where('event_id', config('web.eventId'))
                    // texto
                    ->when(!empty($searchedWords), function ($query) use ($searchedWords) {
                        $query->where(function ($query) use ($searchedWords) {
                            foreach ($searchedWords as $word) {
                                $query->where(function ($query) use ($word) {
                                    $query->orWhere('name', 'like', "%{$word}%");
                                    $query->orWhere('mod_1', 'like', "%{$word}%");
                                    $query->orWhere('objectives', 'like', "%{$word}%");
                                    $query->orWhere('organization', 'like', "%{$word}%");
                                    $query->orWhere('organization_en', 'like', "%{$word}%");
                                });
                            }
                        });
                    })
                    // tipo de sesión
                    ->when(!empty($sessiontype), function($query) use ($sessiontype){
                        $query->where('program_id', $sessiontype);
                    })
                    // área
                    ->when(!empty($area), function($query) use ($area){
                        $query->where(function ($query) use ($area) {
                            $query->orWhere('area_1', $area);
                            $query->orWhere('area_2', $area);
                        });
                        //$query->where('area_1', $area);
                    })
                    // programa
                    ->when(!empty($congress), function($query) use ($congress){
                        $query->where('congress_id', $congress);
                    })
                    // sala
                    ->when(!empty($room), function($query) use ($room){
                        $query->where('room_id', $room);
                    })
                    // descarta las sesiones ocultas
                    ->where(function ($q) use($pvHideField) {
                        return $q->whereNull($pvHideField)
                            ->orWhere($pvHideField, '!=', 1);
                    })
                    ->get();
                    // echo $sessions->toSql() . "<br>";
                    // print_r( $sessions->getBindings() );
                    // exit;

        // busca en las ponencias
        $sessions2 = null;
        if (!empty($searchedWords)) {
            $sessions2 = Session::where('event_id', config('web.eventId'))
                            // texto
                            ->where(function ($query) use ($searchedWords, $pvHideField) {
                                foreach ($searchedWords as $word) {
                                    $query->where(function ($query) use ($word, $pvHideField) {
                                        $query->whereRaw("id in (
                                            select session_id
                                            from papers
                                            where event_id=".config('web.eventId')."
                                            and ({$pvHideField} is null or {$pvHideField} != 1)
                                            and (title like '%{$word}%'
                                            or author_string like '%{$word}%')
                                        )");
                                    });
                                }
                            })
                            // tipo de sesión
                            ->when(!empty($sessiontype), function($query) use ($sessiontype){
                                $query->where('program_id', $sessiontype);
                            })
                            // área
                            ->when(!empty($area), function($query) use ($area){
                                $query->where(function ($query) use ($area) {
                                    $query->orWhere('area_1', $area);
                                    $query->orWhere('area_2', $area);
                                });
                                //$query->where('area_1', $area);
                            })
                            // programa
                            ->when(!empty($congress), function($query) use ($congress){
                                $query->where('congress_id', $congress);
                            })
                            // sala
                            ->when(!empty($room), function($query) use ($room){
                                $query->where('room_id', $room);
                            })
                            // descarta las sesiones ocultas
                            ->where(function ($q) use($pvHideField) {
                                return $q->whereNull($pvHideField)
                                    ->orWhere($pvHideField, '!=', 1);
                            })
                            // ->toSql();
                            ->get();
                            // echo $sessions2;exit;
        }
        $sessions3 = $sessions;
        if (!empty($sessions2)) {
            foreach ($sessions2 as $s2) {
                $insert = true;
                foreach ($sessions3 as $s) {
                    if ($s->id == $s2->id) {
                        $insert = false;
                        break;
                    }
                }
                if ($insert) {
                    $sessions->push($s2);
                }
            }
        }
        // ordena las sesiones antes de devolver el resultado
        $sessions = $sessions->sortBy([
            ['day_format2', 'asc'],
            ['init_hour', 'asc'],
            ['end_hour', 'asc'],
            ['name', 'asc']
        ]);

        // filtra los resultados de las sesiones a las que el usuario actual no tenga acceso
        $res = [];
        $user = \Auth::guard('multi')->user();
        foreach ($sessions as $session) {
            $session['objectives'] = str_replace('"', "'", $session['objectives']);
            if ($user && !$session->isAvailableForCurrentUser()) {
                // estos resultados no los queremos
            }
            else {
                array_push($res, $session);
            }
        }
        
        return $res;
    }

    /** Convierte el array de sesiones a un formato adecuado para la vista esquema.
    * @return Array indizado por fechas en el que en cada posición hay un array, indizado por salas, con las sesiones de ese día */
    public static function _getDataForScheme($sessions, $rooms, $sessiontypes, $areas, $currentUserViews) {
        if (empty($sessions)) { return null; }
        $currentUserViews = !\SettingHelper::isApp() ? View::_getAllWhereCurrentUser() : [];
        $data = [];
        foreach ($rooms as $room) {
            foreach ($sessions as $session) {
                $date = $session->getDate();
                if (!isset($data[$date])) {
                    $data[$date] = [];
                }
                if ($room->getId() == $session->getRoomId()) {
                    if (!isset($data[$date][$room->getId()])) {
                        $data[$date][$room->getId()] = [];
                    }
                    $elem = $session->attributesToArray();
                    $sessiontype = Sessiontype::_find($session->getSessiontypeId(),$sessiontypes);
                    $elem['room_name'] = $room->getNameAbbr();
                    $elem['name'] = str_replace('"', "'", $session->getNameAndIsLive());
                    $elem['sessiontype_name'] = (!empty($sessiontype)) ? $sessiontype->getName() : "";
                    $elem['color'] = $session->getColor($areas, $sessiontypes, true);
                    $elem['url'] = route('sessions.view', ['session' => $session->getId()]);
                    $elem['category'] = (\FilterHelper::_isArea()) ? $session->getAreaNameAbbr($areas) : $session->getSessiontypeNameAbbr($sessiontypes);
                    $elem['category2'] = (\FilterHelper::_isArea()) ? $session->getSessiontypeNameAbbr($sessiontypes) : $session->getAreaNameAbbr($areas);
                    unset($elem['pv_livechat_code']);
                    unset($elem['pv_survey_code']);
                    unset($elem['pv_vimeo_url']);
                    unset($elem['pv_prev_vimeo_url']);
                    $elem['objectives'] = str_replace(array("\r", "\n"), '', $elem['objectives']);
                    $elem['visited'] = $session->hasAnyView($currentUserViews) ? "visited" : "";
                    unset($elem['html_sponsor_app']);
                    unset($elem['html_sponsor_apppv']);
                    unset($elem['html_sponsor_pv']);
                    unset($elem['televoto_app']);
                    unset($elem['televoto_apppv']);
                    unset($elem['televoto_pv']);
                    array_push($data[$date][$room->getId()], $elem);

                }
            }
        }
        return $data;
    }

    /** Obtiene las sesiones (únicas) relacionadas con los id de ponencias pasados,
     * para el evento actual */
    public static function _getWhereHasPaperIn($paperIds) {
        $sessions = Session::select(\DB::connection('cyex_online')->raw('sessions.*'))
                        ->where('sessions.event_id', config('web.eventId'))
                        ->whereRaw("id in (
                                    select session_id
                                    from papers
                                    where event_id=".config('web.eventId')."
                                    and id in (".implode(",", $paperIds).")
                                )"
                        )
                        ->orderBy('day_format2')
                        ->orderBy('init_hour')
                        ->orderBy('sessions.name')
                        // ->toSql();
                        ->get();
        return $sessions;
    }

    /** Obtiene las ponencias relacionadas con la sesión en el evento actual */
    public function getPapers() {
        $papers = Paper::_getWhereSession($this->id);
        return $papers;
    }

    /** Obtiene las sesiones relacionadas: misma área o tipo de sesión (el que corresponda)
    * en caso de que haya colores, o la siguiente sesión en la sala si se trata de un congreso gris
    */
    public function getRelatedSessions() {
        $sessions = [];
        if (\FilterHelper::_isArea()) {
            return Session::where('event_id', config('web.eventId'))
                            ->where('id', '!=', $this->id)
                            ->where('area_1', $this->getAreaId())
                            ->inRandomOrder()
                            ->limit(3)
                            // ->orderBy('name')
                            ->get();
        }
        elseif (\FilterHelper::_isSessiontype()) {
            return Session::where('event_id', config('web.eventId'))
                            ->where('id', '!=', $this->id)
                            ->where('program_id', $this->getSessiontypeId())
                            ->inRandomOrder()
                            ->limit(3)
                            // ->orderBy('name')
                            ->get();
        }
        return Session::where('event_id', config('web.eventId'))
                        ->where('id', '!=', $this->id)
                        ->where('room_id', $this->room_id)
                        ->inRandomOrder()
                        ->limit(3)
                        // ->orderBy('name')
                        ->get();
    }

    public function getRelatedSessionsTitle() {
        $sessions = [];
        if (\FilterHelper::_isArea()) {
            return __('Más sesiones de') . ' "' . $this->getAreaName() . '"';
        }
        elseif (\FilterHelper::_isSessiontype()) {
            return __('Más sesiones de') . ' "' . $this->getSessiontypeName() . '"';
        }
        return __('Siguientes sesiones en la sala') . ' "' . $this->getRoomName() . '"';
    }

    public function getColor($areas, $sessiontypes, $removeHashtag = false) {
        if (\FilterHelper::_isArea()) {
            $area = $this->getArea($areas);
            if (empty($area)) {
                return "";
            }
            return $area->getColor($removeHashtag);
        }
        elseif (\FilterHelper::_isSessiontype()) {
            $sessiontype = $this->getSessiontype($sessiontypes);
            if (empty($sessiontype)) {
                return "";
            }
            return $sessiontype->getColor($removeHashtag);
        }
        return "";
    }

    /** Obtiene los segundos que faltan para que empiece una sesión.
     * Compara la hora del servidor con la hora de inicio de la sesión */
    public function getSecondsRemainingToStart() {
        $initDatetime = $this->getDate() . " " . $this->getInitHour();
        $currentDatetime = date("Y-m-d H:i:s");
        $difference = \DateHelper::getDifferenceBetweenDatetimes($initDatetime, $currentDatetime);
        return $difference;
        // $res = $initDatetime . "<br>" . $currentDatetime . "<br>" . $difference;
        // return $res;
    }

    /** Obtiene el tiempo en segundos a partir del cual se podrá mostrar el vídeo.
     * Influye la fecha y hora de inicio de la sesión y el parámetro que determina
     * hasta cuándo se verá la carátula en vez del vídeo */
    public function getSecondsRemainingToShowVideo() {
        $secRemainingToStartSession = $this->getSecondsRemainingToStart();
        if ($secRemainingToStartSession <= 0) { return 0; }
        $minBeforeShowVideo = $this->getMinutesBeforeShowVideo();
        if (empty($minBeforeShowVideo)) {
            $minBeforeShowVideo = 0;
        }
        return $secRemainingToStartSession - ($minBeforeShowVideo * 60);
    }

    public function getLastSecondPlayedByCurrentUser() {
        return View::_getLastSecondPlayedByCurrentUser($this->getId());
    }

    /************************** GETTERS ***************************************/
    public function getInitHour() {
        return substr($this->init_hour, 0, 5);
    }

    public function getEndHour() {
        return substr($this->end_hour, 0, 5);
    }

    /** Devuelve los segundos que dura una sesión en función de sus horas de inicio y fin */
    public function getDuration() {
        $initHour = $this->getInitHour();
        $endHour = $this->getEndHour();
        if (empty($initHour) || empty($endHour)) {
            abort(400, 'Es necesario que todas las sesiones tengan establecidas unas horas de inicio y fin y hay una que no lo tiene, que es: ' . $this->getId() );
        }
        return (\DateHelper::HISToSeconds($endHour) - \DateHelper::HISToSeconds($initHour));
    }

    public function getHour() {
        $initHour = $this->getInitHour();
        $endHour = $this->getEndHour();
        if (empty($endHour) && empty($initHour)) { return ""; }
        if (empty($endHour)) { return $initHour; }
        if (empty($initHour)) { return $endHour; }
        return $initHour . "-" . $endHour;
    }

    public function getPrettyDate() {
        $day = ucfirst(\DateHelper::formatLong($this->getDate(), App::currentLocale()));
        if (empty($day)) { return null; }
        return $day;
    }

    public function getDayAbbr() {
        $date = $this->getPrettyDate();
        if (empty($date)) { return null; }
        return substr($date, 0, 3);
    }

    public function getDate() {
        return $this->day_format2;
    }

    public function getDateAndHour() {
        $day = ucfirst(\DateHelper::formatLong($this->getDate(), App::currentLocale()));
        $hour = $this->getHour();
        if (empty($day) && empty($hour)) { return null; }
        if (empty($day)) { return $hour; }
        if (empty($hour)) { return $day; }
        return $day . ". " . $hour;
    }

    public function getAreaName($areas = null) {
        $area = $this->getArea($areas);
        if (!empty($area)) {
            return $area->getName();
        }
        return null;
    }

    public function getAreaNameAbbr($areas = null) {
        $area = $this->getArea($areas);
        if (!empty($area)) {
            return $area->getNameAbbr();
        }
        return null;
    }

    public function getArea($areas = null) {
        if (empty($areas)) {
            $areas = Area::_getAll();
        }
        $area = Area::_find($this->getAreaId(), $areas);
        if ($area) {
            return $area;
        }
        return null;
    }

    public function getSessiontypeName($sessiontypes = null) {
        $sessiontype = $this->getSessiontype($sessiontypes);
        if (!empty($sessiontype)) {
            return $sessiontype->getName();
        }
        return null;
    }

    public function getSessiontypeNameAbbr($sessiontypes = null) {
        $sessiontype = $this->getSessiontype($sessiontypes);
        if (!empty($sessiontype)) {
            return $sessiontype->getNameAbbr();
        }
        return null;
    }

    public function getSessiontype($sessiontypes = null) {
        if (empty($sessiontypes)) {
            $sessiontypes = Sessiontype::_getAll();
        }
        $sessiontype = Sessiontype::_find($this->getSessiontypeId(), $sessiontypes);
        if ($sessiontype) {
            return $sessiontype;
        }
        return null;
    }

    public function getId() {
        return $this->id;
    }

    public function getEventId() {
        return $this->event_id;
    }

    public function getName() {
        $name = trim($this->name);
        if (!empty($this->organization)) {
            $name .= ". <span class='organization'>" . $this->organization . "</span>";
        }
        return $name;
    }

    /** Además del nombre de la sesión, obtiene también si está siendo ahora mismo */
    public function getNameAndIsLive() {
        $name = $this->getName();
        if ($this->isLiveNow()) {
            $text = (!\SettingHelper::isApp()) ? __('¡En directo!') : __('¡Ahora!');
            $name .= "<span class='is-live mt-2'>" . $text . "</span>";
        }
        return $name;
    }

    public function getAuthors() {
        return $this->mod_1;
    }

    public function getRef() {
        return $this->ref;
    }

    public function getRoomName($rooms = null) {
        $room = Room::_find($this->room_id, $rooms);
        if ($room) {
            return $room->getName();
        }
        return null;
    }

    public function getRoomNameAbbr($rooms = null) {
        $room = Room::_find($this->room_id, $rooms);
        if ($room) {
            return $room->getNameAbbr();
        }
        return null;
    }

    public function getRoomId() {
        return $this->room_id;
    }

    public function getSessiontypeCss($sessiontypes) {
        $sessiontype = Sessiontype::_find($this->getSessiontypeId(), $sessiontypes);
        if ($sessiontype) {
            return $sessiontype->getCss();
        }
        return "";
    }

    public function getAreaCss($areas) {
        $area = Area::_find($this->getAreaId(), $areas);
        if ($area) {
            return $area->getCss();
        }
        return "";
    }

    public function getLevelName() {
        $level = Level::_get($this->level_id);
        if ($level) {
            return $level->getName();
        }
        return null;
    }

    public function getType() {
        $types = [];
        if ($this->type_1) {
            array_push($types, $this->type_1);
        }
        if ($this->type_2) {
            array_push($types, $this->type_2);
        }
        if ($this->type_3) {
            array_push($types, $this->type_3);
        }
        if (count($types) == 0) { return null; }
        return implode(', ', $types);
    }

    public function getModerators() {
        return $this->mod_1;
    }

    public function getObjectives() {
        return $this->objectives;
    }

    public function getLivechatCode() {
        $field = "pv_livechat_code";
        if ($this->isTimeToLivechat()) {
            return $this->{$field};
        }
        return null;
    }

    public function getSurveyCode() {
        // if (\App::environment('production')) {
        //     if ($this->isLiveNow()) {
        //         return $this->pv_survey_code;
        //     }
        //     return null;
        // }
        return $this->pv_survey_code;
    }

    public function getSurveyType() { return $this->pv_survey_type; }

    public function getVimeoUrl($embed = true) {
        $vimeoUrl = $this->pv_vimeo_url;
        if (empty($vimeoUrl)) { return null; }
        // para poner el reproductor antiguo, usar resources/views/session/_video_backup y aquí hacer un return $vimeoUrl
        if (substr( $vimeoUrl, 0, 23 ) === "https://vimeo.com/event") {
            if ($embed) {
                return "https://vimeo.com/event/".$this->getVimeoId()."/embed";
            } else {
                return "https://vimeo.com/event/".$this->getVimeoId();
            }
        }
        else {
            return "https://player.vimeo.com/video/" . $this->getVimeoId();
        }
    }

    public function getPrevVimeoUrl() {
        $vimeoUrl = $this->pv_prev_vimeo_url;
        if (empty($vimeoUrl)) { return null; }
        // para poner el reproductor antiguo, usar resources/views/session/_video_backup y aquí hacer un return $vimeoUrl
        if (substr( $vimeoUrl, 0, 23 ) === "https://vimeo.com/event") {
            return "https://vimeo.com/event/".$this->getPrevVimeoId()."/embed";
        }
        else {
            return "https://player.vimeo.com/video/" . $this->getPrevVimeoId();
        }
    }

    public function getSessiontypeId() {
        return $this->program_id;
    }

    public function getAreaId() {
        return $this->area_1;
    }

    public function getVimeoId($field = "pv_vimeo_url") {
        $url = $this->{$field};
        if (empty($url)) { return false; }
        $url = rtrim($url, '/');
        $pos = strrpos($url, '/');
        $id = $pos === false ? $url : substr($url, $pos + 1);
        return $id;
    }

    public function getPrevVimeoId() {
        return $this->getVimeoId('pv_prev_vimeo_url');
    }

    public function getMinutesBeforeShowVideo() {
        return $this->pv_minutes_before_show_video;
    }

    public function getViewerVisibility() {
        return $this->pv_no_video;
    }

    public function getLivechatStartTime() { return $this->pv_livechat_start_time; }
    public function getLivechatEndTime() { return $this->pv_livechat_end_time; }

    public function getCyexSurveyLink() {
        $user = \Auth::guard('multi')->user();
        if (empty($user)) { return null; }
        if ($this->isAlreadyEvaluatedByUser($user)) { return null; }
        return "https://dataeventservices.net/surveys/home/virtual_ses/" . $user->getIdForSurvey() . "/" . $this->getId() . "/" . config('web.eventId');
    }

    public function getRestricted() { return $this->pv_restricted; }

    public function getVimeoThumb() {
        // https://artisansweb.net/get-thumbnail-youtube-vimeo-dailymotion-videos/
        $url = $this->getVimeoUrl(false);
        if (!$url) { return false; }
        $data = json_decode( @file_get_contents( 'https://vimeo.com/api/oembed.json?url=' . $url ) );
        if ( !$data || !property_exists($data, 'thumbnail_url')) return false;
        return $data->thumbnail_url;
    }

    public function getNoVideoImg() {
        if (!empty($this->pv_no_video_img)) {
            return $this->pv_no_video_img;
        }
        else {
            return setting('web.no_video_img');
        }
    }
    public function getNoVideoText() {
        if (!empty($this->pv_no_video_text)) {
            return $this->pv_no_video_text;
        }
        else {
            return \SettingHelper::getNoVideoText();
        }
    }
    public function getNoVideoTextColor() {
        $color = $this->pv_no_video_text_color_rgbhex;
        if (empty($color)) { return null; }
        if (!preg_match("/^#(?:[0-9a-fA-F]{3}){1,2}$/", $color)) { return null; }
        return $color;
    }
    public function getPromoVideoVimeoUrl() {
        $field = 'pv_promo_vimeo_url';
        $vimeoUrl = $this->{$field};
        if (empty($vimeoUrl)) { return null; }
        // para poner el reproductor antiguo, usar resources/views/session/_video_backup y aquí hacer un return $vimeoUrl
        if (substr( $vimeoUrl, 0, 23 ) === "https://vimeo.com/event") {
            return "https://vimeo.com/event/".$this->getVimeoId($field)."/embed";
        }
        else {
            return "https://player.vimeo.com/video/" . $this->getVimeoId($field);
        }
    }
    public function getSponsorHtml() {
        $field = 'html_sponsor_app';
        if (\SettingHelper::isAppPV()) {
            $field = 'html_sponsor_apppv';
        } elseif (\SettingHelper::isPV()) {
            $field = 'html_sponsor_pv';
        }
        return $this->{$field};
    }
    public function getTelevoto() {
        $field = 'televoto_app';
        if (\SettingHelper::isAppPV()) {
            $field = 'televoto_apppv';
        } elseif (\SettingHelper::isPV()) {
            $field = 'televoto_pv';
        }
        return $this->{$field};
    }
    public function getGoogleCalendarLink() {
        $startDateHour = \DateHelper::getGoogleCalendarTimestamp($this->getDate(), $this->getInitHour());
        if (!$startDateHour) { return false; }
        $endDateHour = \DateHelper::getGoogleCalendarTimestamp($this->getDate(), $this->getEndHour());
        $name = strip_tags($this->getName());
        $location = $this->getRoomName();
        $details = [];
        $sessionTypeName = $this->getSessiontypeName();
        if (!empty($sessionTypeName)) { array_push($details, $sessionTypeName . "."); }
        $moderators = $this->getModerators();
        if (!empty($moderators)) { array_push($details, "Modera: " . $moderators . "."); }
        $details = implode("<br>", $details);
        return "https://calendar.google.com/calendar/u/0/r/eventedit?action=TEMPLATE&dates={$startDateHour}/{$endDateHour}&details={$details}&location={$location}&text={$name}";
    }

    public function getOutlookCalendarLink() {
        $startDateHour = \DateHelper::getOutlookCalendarTimestamp($this->getDate(), $this->getInitHour());
        if (!$startDateHour) { return false; }
        $endDateHour = \DateHelper::getOutlookCalendarTimestamp($this->getDate(), $this->getEndHour());
        $name = strip_tags($this->getName());
        $location = $this->getRoomName();
        $details = [];
        $sessionTypeName = $this->getSessiontypeName();
        if (!empty($sessionTypeName)) { array_push($details, $sessionTypeName . "."); }
        $moderators = $this->getModerators();
        if (!empty($moderators)) { array_push($details, "Modera: " . $moderators . "."); }
        $details = implode("\n", $details);
        return "https://outlook.live.com/calendar/0/deeplink/compose?path=/calendar/action/compose&rru=addevent&startdt={$startDateHour}&enddt={$endDateHour}&subject={$name}&body={$details}&location={$location}";
    }

    public function getAppleCalendarLink() {
        $startDateHour = \DateHelper::getAppleCalendarTimestamp($this->getDate(), $this->getInitHour());
        if (!$startDateHour) { return false; }
        $endDateHour = \DateHelper::getAppleCalendarTimestamp($this->getDate(), $this->getEndHour());
        $name = strip_tags($this->getName());
        $location = $this->getRoomName();
        $details = [];
        $sessionTypeName = $this->getSessiontypeName();
        if (!empty($sessionTypeName)) { array_push($details, $sessionTypeName . "."); }
        $moderators = $this->getModerators();
        if (!empty($moderators)) { array_push($details, "Modera: " . $moderators . "."); }
        // se pone la doble barra para que en el documento resultante ponga \n en lugar de agregar un salto de línea. De esta manera, cuando se importa en Calendar lo interpretará como un salto de línea
        $details = implode("\\r\\n", $details);
        
        // @see https://devguide.calconnect.org/Data-Model/Simple-Event/
        $icsContent = "BEGIN:VCALENDAR\r\nVERSION:2.0\r\nBEGIN:VEVENT\r\nUID:" . uniqid() . "\r\nDTSTAMP:" . gmdate('Ymd\THis\Z') . "\r\nDTSTART;TZID=".config('app.timezone').":{$startDateHour}\r\nDTEND;TZID=".config('app.timezone').":{$endDateHour}\r\nSUMMARY:{$name}\r\nDESCRIPTION:{$details}\r\nLOCATION:{$location}\r\nEND:VEVENT\r\nEND:VCALENDAR";
        $icsFile = "data:text/calendar;charset=utf8," . rawurlencode($icsContent);
        
        return $icsFile;
    }

    /************************** HASERS ***************************************/
    public function hasDay($day) {
        return ($this->day_format2 === $day);
    }

    public function hasSurveyToShow() {
        $surveyType = $this->getSurveyType();
        if ($surveyType == self::CYEX) {
            return true;
        }
        elseif ($surveyType == self::SURVEY_MONKEY) {
            $surveyCode = $this->getSurveyCode();
            if (!empty($surveyCode)) {
                return true;
            }
        }
        return false;
    }

    /** Devuelve verdadero en el caso de que la sesión actual esté en el listado
     * de visualizaciones pasado
     * @param View $views Array de modelos de tipo View */
    public function hasAnyView($views) {
        foreach ($views as $view) {
            if ($view->getSessionId() == $this->getId()) {
                return true;
            }
        }
        return false;
    }

    public function hasNote() {
        $notes = new \App\Models\Notebook();
        $nota = $notes->getSessionNote($this->getId());
        return !empty($nota);
    }

    /************************** ISERS ***************************************/
    /** Devuelve verdadero si la sesión está teniendo lugar ahora mismo */
    public function isLiveNow() {
        $endHour = $this->getEndHour();
        if (!empty($endHour)) {
            $errorHourRange = 0;    //se considera que está en directo hasta pasadas $errorHourRange horas del tiempo límite
            $endHour = \DateHelper::modifyHISHour($endHour, $errorHourRange);
        }
        return (
            \DateHelper::isToday($this->getDate()) &&
            \DateHelper::isHourInRange($this->getInitHour(), $endHour)
        );
    }
    /** Devuelve si la sesión está cerca de empezar */
    public function isAboutToStart() {
        if (!\DateHelper::isToday($this->getDate())) { return false; }
        $initHour = $this->getInitHour();
        if (empty($initHour)) { return false; }
        $now = date("H:i:s");
        $nowSec = \DateHelper::HISToSeconds($now);
        $initSec = \DateHelper::HISToSeconds($initHour);
        $nSecBefore = 30 * 60; // 15min
        if (($initSec - $nSecBefore) < $nowSec) {
            return true;
        }
        return false;
    }
    /** Devuelve verdadero si la sesión no ha tenido lugar */
    public function isStartingInFuture() {
        $day = $this->getDate();
        if (empty($day)) {
            return false;
        }
        $initHour = $this->getInitHour();
        if (empty($initHour)) {
            $initHour = "23:59:59";
            // $initHour = "00:00:00"; // TODO: Si no se indica hora, ¿se considera futuro o no si estamos en ese día?
        }
        return !\DateHelper::isDatetimeInPast($day . " " . $initHour);
    }
    /** Devuelve verdadero si la sesión ya ha tenido lugar */
    public function isPast() {
        $day = $this->getDate();
        if (empty($day)) {
            return false;
        }
        $endHour = $this->getEndHour();
        if (empty($endHour)) {
            $endHour = "23:59:59";
        }
        return \DateHelper::isDatetimeInPast($day . " " . $endHour);
    }

    /* Devuelve si la sesión actual está incluida en la agenda */
    public function isInSchedule() {
        $agenda = new Schedule();
        return $agenda->isSessionInSchedule($this->getId());
    }

    /** Devuelve si la sesión cuyo id se pasa por parámetro se solapa con la actual en horario */
    public function isOverlapping($sessionId) {
        if ($sessionId == $this->getId()) { return false; } // no se puede comparar consigo misma
        $session = $this->_get($sessionId);
        if (empty($session)) { return false; }
        if ($session->getDate() !== $this->getDate()) { return false; }
        if (empty($session->getInitHour()) || empty($session->getEndHour())
            || empty($this->getInitHour()) || empty($this->getEndHour())) {
                return false;
        }
        $dt0This = $this->getDate() . " " . $this->getInitHour();
        $dt1This = $this->getDate() . " " . $this->getEndHour();
        $dt0Session = $session->getDate() . " " . $session->getInitHour();
        $dt1Session = $session->getDate() . " " . $session->getEndHour();

        // Si coincide la fecha de inicio con la de fin de otra --> no son coincidentes
        if (($dt0This == $dt1Session) || ($dt1This == $dt0Session)) { return false; }

        if (\DateHelper::isDatetimeInRange($dt0This, $dt0Session, $dt1Session)) { return true; }
        if (\DateHelper::isDatetimeInRange($dt1This, $dt0Session, $dt1Session)) { return true; }
        if (\DateHelper::isDatetimeInRange($dt0Session, $dt0This, $dt1This)) { return true; }
        if (\DateHelper::isDatetimeInRange($dt1Session, $dt0This, $dt1This)) { return true; }
        return false;
    }

    /* Devuelve si la sesión actual tiene alguna nota */
    public function isInNotebook() {
        $notes = new Notebook();
        return $notes->isSessionInNotebook($this->getId());
    }

    /** Devuelve si se muestra el vídeo (si hay vídeo y si no está en un momento de mostrar la carátula previa) */
    public function isVideoReadyToShow() {
        return ($this->getSecondsRemainingToShowVideo() <= 0);
    }

    public function isTimeToLivechat() {
        return \DateHelper::isDatetimeInRange(date("Y-m-d H:i:s"), $this->getLivechatStartTime(), $this->getLivechatEndTime());
    }

    public function isRestricted() {
        $restricted = $this->getRestricted();
        return ($restricted == 1);
    }

    /** Devuelve verdadero en caso de que esta sesión esté restringida
     * y además el usuario logueado no la tenga añadida como sesión restringida
     * a la que tiene acceso */
    public function isRestrictedForCurrentUser() {
        if (!$this->isRestricted()) { return false; }

        $user = \Auth::guard('multi')->user();
        if (empty($user)) { return true; }

        $allowedSessionsIds = $user->getRestrictedAllowedSessionsIds();
        return !in_array($this->getId(), $allowedSessionsIds);
    }

    public function isAlreadyEvaluatedByUser($user) {
        return !empty(
            \DB::connection('cyex_online')
                ->table('sur_session_registered')
                ->where('user_id', $user->getIdForSurvey())
                ->where('session_id', $this->getId())
                ->where('event_id', config('web.eventId'))
                ->where('survey_done', 1)
                ->first()
        );
    }

    /** Devuelve si la sesión está disponible para el usuario actual. Está disponible en el caso de que:
     * el usuario no tiene nada en restricted_program_letter
     * restricted_program_letter del usuario coincida con alguno de pv_restricted_program_1..5
    */
    public function isAvailableForCurrentUser() {
        $user = \Auth::guard('multi')->user();
        if (empty($user)) { return true; }
        
        $userRestrictedProgramLetter = $user->getRestrictedProgramLetter();
        if (empty($userRestrictedProgramLetter)) {
            return true;
        }

        for ($i = 1; $i<=5; $i++) {
            if ($this->{'pv_restricted_program_' . $i} == $userRestrictedProgramLetter) {
                return true;
            }
        }
        return false;
    }

    public function isPoster() {
        return $this->poster;
    }

}
