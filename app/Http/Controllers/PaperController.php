<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Paper;
use App\Models\PosterRating;
use App\Mail\AskPaper;

class PaperController extends Controller {

    /** Llamada a través de AJAX */
    public function ask($paperId, Request $request) {
        $paper = Paper::_get($paperId);
        if (empty($paper)) { echo "false"; return; }
        // Validación
        $rules = [
            'email' => 'required',
            'email' => 'email',
            'acceptance' => 'required',
            'message' => 'required'
        ];
        $messages = [
            'email.required' => 'Escriba su mensaje antes de enviar',
            'email.email' => 'Dirección de correo electrónico incorrecta',
            'acceptance.required' => 'Debe aceptar la política de privacidad',
            'message.required' => 'Introduzca su pregunta antes de enviar'
        ];
        $validatedData = $request->validate($rules, $messages);

        // Envía la pregunta por correo
        \Mail::to($paper->getMailcontact())->send(new AskPaper($request->all(), $paper));

        echo "true"; //esta llamada se hace por AJAX

        // return redirect()->route('supportmessages.view', ['course' => $course->id])
        //                 ->with('message', __('support_message_sent'));
    }

    /** Procesa la votación de una ponencia (que es póster) */
    public function rate(Request $request) {
        // Validación de datos
        $rules = [
            'paper_id' => 'required',
            'session_id' => 'required',
            'rating' => 'required'
        ];
        $messages = [
            'paper_id.required' => 'Falta el id de la ponencia',
            'session_id.required' => 'Falta el id de sesión',
            'rating.required' => 'Haga su valoración antes de enviar'
        ];
        $validatedData = $request->validate($rules, $messages);

        // Guarda la puntuación
        PosterRating::_setUserRatingOfPaper($request->paper_id, $request->rating);

        // Redirige a donde se estaba, informando de que se guardó la valoración
        return redirect()->route('sessions.view', ['session' => $request->session_id])
                        ->with('success', 'Votación registrada');
    }
}
