<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class VRoom extends Model {
    protected $table = 'vrooms';

    public static function _getBySlug($slug) {
        return VRoom::where('slug', $slug)->first();
    }
    /** Obtiene la siguiente sala (siguiendo el campo pos) a la que se pasa por parámetro */
    public static function _getNext($slug) {
        $vroom = VRoom::_getBySlug($slug);
        $vrooms = VRoom::where('is_exhibitor', 1)->orderBy('pos', 'ASC')->get();
        $returnNext = false;
        foreach ($vrooms as $vr) {
            if ($returnNext) {
                return $vr;
            }
            if ($vr->getId() == $vroom->getId()) { $returnNext = true; }
        }
        // se trata de la última --> devuelve la primera
        if ($returnNext) { return $vrooms[0]; }
    }
    /** Obtiene la sala previa (siguiendo el campo pos) a la que se pasa por parámetro */
    public static function _getPrev($slug) {
        $vroom = VRoom::_getBySlug($slug);
        $vrooms = VRoom::where('is_exhibitor', 1)->orderBy('pos', 'DESC')->get();
        $returnNext = false;
        foreach ($vrooms as $vr) {
            if ($returnNext) {
                return $vr;
            }
            if ($vr->getId() == $vroom->getId()) { $returnNext = true; }
        }
        // se trata de la primera --> devuelve la última
        if ($returnNext) { return $vrooms[0]; }
    }

    public function getId() { return $this->id; }
    public function getName() { return $this->name; }
    public function getSlug() { return $this->slug; }
    public function getImgSrc() { return $this->background; }
    public function getMapareas() {
        if (empty($this->mapareas)) { return []; }
        return json_decode($this->mapareas, true);
    }
    public function getLegal() { return $this->legal; }
    public function getLegalYes() { return $this->legal_yes; }
    public function getLegalNo() { return $this->legal_no; }

    public function isExhibitor() { return $this->is_exhibitor; }
    public function isExhibition() { return ($this->getSlug() == "exposicion"); }
    public function isExhibitorOrExhibition() {
        return $this->isExhibitor() || $this->isExhibition();
    }
}
