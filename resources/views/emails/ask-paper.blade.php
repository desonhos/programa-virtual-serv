@component('mail::message')

@php
$session = App\Models\Session::_get($paper->getSessionid());
@endphp

# {{ __("Has recibido una consulta desde") }} {{ config('app.name') }}.

@if (!empty($session))
* {{ __("Sesión:")}} {!! $session->getName() !!}.
@endif
* {{ __("Ponencia:")}} {!! $paper->getName() !!}.

## {{ __("Remitente") }}
* {{ __("Nombre") }}: {{ $senderName }}.
* {{ __("Correo electrónico") }}: {{ $senderEmail }}.

## {{ __("Consulta") }}
* {{ __("Mensaje") }}: {{ $message }}.

@endcomponent
