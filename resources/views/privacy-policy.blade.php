@extends('layouts.app' )

@section('page-class', 'page-info')

@section('content')
    <article>
        <section class="section">
            <div class="container content">
            <h1>{{ __('Política de privacidad') }}</h1>
            <p>{!! \SettingHelper::getPrivacyPolicy() !!}</p>
    </article>
@endsection
