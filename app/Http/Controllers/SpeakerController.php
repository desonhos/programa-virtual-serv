<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Paper;
use App\Models\Session;
use App\Models\Speaker;
use App\Models\Area;
use App\Models\Sessiontype;
use App\Models\Room;

class SpeakerController extends Controller {

    public function index() {
        $speakers = Speaker::_getAll();
        return view('speakers', compact('speakers'));
    }

    public function search(Request $request) {
        $speakers = Speaker::search($request->searchText);
        return view('speakers', [
            'speakers' => Speaker::search($request->searchText),
            'searchText' => $request->searchText
        ]);
    }

    public function view($speakerId) {
        $speaker = Speaker::_get($speakerId);
        $areas = Area::_getAll();
        $sessiontypes = Sessiontype::_getAll();
        $rooms = Room::_getAll();
        return view('speakers.view', compact('speaker', 'areas', 'sessiontypes', 'rooms'));
    }
}
