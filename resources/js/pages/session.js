var Mark = require('mark.js');

$(document).ready(function() {
    // Marca en amarillo lo buscado
    var searched_text = findGetParameter("searched_text");
    if (searched_text) {
        searched_text = searched_text.replaceAll('+', ' ');
        if (searched_text) {
            var m = new Mark(document.querySelectorAll(".subtitle, .title, .paperTitle, .author span, .objectives, .mod_1"));
            m.mark(searched_text, {});
        }
    }
    // Modal de valorar póster
    $(".ratePoster").click(function() {
        showRateModal($(this).data('paper-id'));
    });
    $(".modal.is-rate .close, .modal.is-rate .modal-background").click(function() {
        $(this).parents(".modal").removeClass("is-active");
    })
    setOnSubmitRating();
    $(".ratings-average-stars svg").click(function(){
        $($(this).closest(".paper").find(".ratePoster")[0]).click();
    });

    // Manejadores para ordenar los pósteres
    sortPapersHandlers();

    // Conflictos de interés
    $(".conflictosTitulo").click(function(){
        $(this).next().toggle();
        var elem = $(this).find(".aspa")[0];
        if (typeof(elem) !== "undefined") {
            if ($(this).next().is(":visible")) {
                elem.style.transform = "rotate(180deg)";
            } else {
                elem.style.transform = "rotate(0deg)";
            }

        }
    });
});

function findGetParameter(parameterName) {
    var result = null,
        tmp = [];
    location.search
        .substr(1)
        .split("&")
        .forEach(function (item) {
          tmp = item.split("=");
          if (tmp[0] === parameterName) result = decodeURIComponent(tmp[1]);
        });
    return result;
}


/** Muestra el modal de valorar poster */
function showRateModal(paperId) {
    // console.log(paperId);
    $("#rateModal_" + paperId).addClass("is-active");
}

/** Manejadores del submit del formulario de valorar póster */
function setOnSubmitRating() {
    $("form.ratePoster").submit(function(ev) {
        var id = $(this).attr('id');
        if (!$("#" + id + " input:radio[name=rating]:checked").length) {
            $("#" + id + " .noSelectionError").removeClass('is-hidden');
            ev.preventDefault();
        }
        else {
            $("#" + id + " .messageAfterSend").removeClass('is-hidden');
            $("#" + id + " .modal-card-body > *:not(.messageAfterSend)").hide();
        }
    });
}

function sortPapersHandlers() {
    // POR AQUI
    setAverageOfPosters();
    $('#orden_mas_alta').change(function() {
        $(".paperList").html(getSorted(".paper", "average", 0));
        setAverageOfPosters();
        //if ($(this).is(':checked')) { alert("ver primero las más altas"); }
    });
    $('#orden_mas_baja').change(function() {
        $(".paperList").html(getSorted(".paper", "average", 1));
        setAverageOfPosters();
        //if ($(this).is(':checked')) { alert("ver primero las más bajas"); }
    });

}

function setAverageOfPosters() {
    $(".paper").each(function() {
        var average = $(this).find(".averageHidden");
        if (average.length) {
            $(this).data('average', $(average[0]).html());
        }
    })
}

function getSorted(selector, dataName, asc) {
    if (asc) {
        return $($(selector).toArray().sort(function(a, b){
            var aVal = parseFloat($(a).data(dataName)),
                bVal = parseFloat($(b).data(dataName));
            return aVal - bVal;
        }));
    } else {
        return $($(selector).toArray().sort(function(a, b){
            var aVal = parseFloat($(a).data(dataName)),
                bVal = parseFloat($(b).data(dataName));
            return bVal - aVal;
        }));
    }
}
