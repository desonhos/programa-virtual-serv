<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSponsorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sponsors', function (Blueprint $table) {
            $table->increments('id');

            $table->string('name', 128);
            $table->string('logo')->nullable();
            $table->string('nstand', 64)->nullable();
            $table->string('url_web', 1024)->nullable();
            $table->longText('description')->nullable();
            $table->longText('logo_css', 64)->nullable();
            $table->integer('pos')->default(99);
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sponsors');
    }
}
