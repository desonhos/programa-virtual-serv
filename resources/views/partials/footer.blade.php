@php
$privacyPolicyRoute = 'privacy-policy';
@endphp

<footer class="footer">
    <div class="content is-flex is-align-items-center">
        <a href="https://www.logievents.com/" target="_blank" class="is-flex" id="logoLogieventsWrapper">
            <img id="logoLogievents" src="/event/logo-logievents.png">
        </a>
        <a href="{{ route($privacyPolicyRoute) }}">{{__("Política de privacidad") }}</a>
        <a href="mailto:{{ \SettingHelper::getContactEmail() }}?subject={{ config('web.event.shortName') }}">{{__("Soporte técnico")}}</a>
    </div>
</footer>
