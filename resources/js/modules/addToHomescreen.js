$(document).ready(function() {
    // Si se tiene el parámetro GET addToHomescreen, se muestran las instrucciones
    const urlParams = new URLSearchParams(window.location.search);
    var addToHomescreen = urlParams.get('addToHomescreen');
    if (addToHomescreen == "1") {
        $("#addToHomescreenModal").addClass('is-active');
    }

    // Cierra al pulsar en cancelar
    $("#addToHomescreenModal .close.button").click(function() {
        $("#addToHomescreenModal").removeClass('is-active');
    });
});
