@extends('layouts.app' )

{{-- @section('title', $page->title)
@section('meta_description', $page->meta_description)
@section('meta_keywords', $page->meta_keywords)
@section('og_image', Storage::disk(config('voyager.storage.disk'))->url($page->image) )
@section('meta_robots', 'index, follow')
@section('meta_revisit-after', 'content="3 days') --}}

@section('css')
    @include('partials.dynamic-styles')
@endsection

@section('js')
    {{-- <script type="text/javascript" src="{{ asset('vendor/ultimate-video-player/java/FWDUVPlayer.js') }}"></script>--}}
    {{-- <script src="https://code.jquery.com/jquery-1.12.4.min.js" integrity="sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ=" crossorigin="anonymous"></script> --}}
    <script
      src="https://code.jquery.com/jquery-2.2.4.min.js"
      integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44="
      crossorigin="anonymous"></script>
    <script src="{{ asset('vendor/jquery-skeduler/jquery.skeduler.js') }}" type="text/javascript" defer></script>
    <script src="{{ mix('js/pages/program.js') }}" defer></script>
    <script src="{{ asset('js/pages/programSchemeView.js') }}" type="text/javascript" defer></script>
    <script type="text/javascript">
        var nowDay = '{{ date("Y-m-d") }}';
        var nowHour = '{{ date("H:i") }}';
    </script>
@endsection

@section('page-class', 'page-program')

@section('content')
    <article id="programZoomableArea">
        @include('program.day-selector')
        @include('program.action-bar')
        @include('program.sessions')
    </article>
    @include('program.zoom')
@endsection
