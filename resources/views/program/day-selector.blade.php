@if (!empty($days))
    <section class="section pt-0 pb-0" id="day-tabs-section">
        <div class="container__">
            <div class="tabs is-boxed" id="day-tabs">
                <ul>
                    <li class="is-active" id="tab-todos">
                        <a class="is-uppercase is-size-7">{{ __("TODO") }}</a>
                    </li>
                    @php $first = false; @endphp
                    @foreach ($days as $day)
                        <li @php if ($first) { echo 'class="is-active"'; } @endphp id="tab-{{ $day }}">
                            <a class="is-uppercase is-size-7">{{ DateHelper::formatShort($day, Lang::locale()) }}</a>
                        </li>
                        @php ($first = false)
                    @endforeach
                </ul>
            </div>
        </div>
    </section>
@endif
