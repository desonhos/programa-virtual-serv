@php
    $colorModifier = $color;
    if (empty($color)) {
        $color = "777777";
        $colorModifier = "";
    }
@endphp
<style>
    .page-program #session-filter input[type=radio].is-{{ $colorModifier }}:after { border-color: #{{ $color }}; }
    .page-program #session-filter input[type=radio].is-{{ $colorModifier }}:checked:after { background-color: #{{ $color }}; }
    .has-text-{{ $colorModifier }} { color: #{{$color }}; }
    /* .page-session.is-{{ $colorModifier }} section.is-header { background-color: #{{ $color }}; } */
    /* .page-session.is-{{ $colorModifier }} section.is-video-and-summary .details { color: #{{ $color }}; }
    .page-session.is-{{ $colorModifier }} section.is-video-and-summary .details .label { color: #{{ $color }}; } */
    .page-session.is-{{ $colorModifier }} .paper .paperTitle { color: #{{ $color }}; }
    .page-session.is-{{ $colorModifier }} a:not(.button,.noDynamicStyles) { color: #{{ $color }}; }
    /* .page-session.is-{{ $colorModifier }} a:not(.button) svg.ico-back polyline { stroke: #{{ $color }}; } */
    .page-session.is-{{ $colorModifier }} .button.is-info { background-color: #{{ $color }}; }
    .page-session.is-{{ $colorModifier }} #guestVideoAccessForm .button.is-primary { background-color: #{{$colorModifier}}; }
    /* @media screen and (min-width: 1024px) {
        .page-session.is-{{ $colorModifier }} section.is-video-and-summary .details {
            border-left: 2px solid #{{ $color }};
        }
    } */
    /* @media screen and (max-width: 1023px) {
        .page-session.is-{{ $colorModifier }} section.is-video-and-summary .details {
            border-top: 2px solid #{{ $color }};
            border-bottom: 2px solid #{{ $color }};
        }
    } */
    .page-session.is-{{ $colorModifier }} section.is-header > .container { border-left-color: #{{ $color }}; border-bottom-color: #{{ $color }}; }
    .page-session.is-{{ $colorModifier }} section.is-header .subtitle { color: #{{ $color }}; }
    .page-session.is-{{ $colorModifier }} section.is-header .title { color: #{{ $color }}; }

    .session-scheme.is-{{ $colorModifier }} .category { background-color: #{{ $color }}; }
    .session-scheme.is-{{ $colorModifier }} .info .name { color: #{{ $color }}; }
    .session-scheme.is-{{ $colorModifier }} .info { border: 1px solid #{{ $color }}; }

    @if ($color == "E1DD23")
        @php $darkText = "5a5a5a"; @endphp
        .session-scheme.is-{{ $colorModifier }} .category { color: #{{ $darkText }}; }
        .page-session.is-{{ $colorModifier }} section.is-header .subtitle,
        .page-session.is-{{ $colorModifier }} section.is-header .title { color: #{{ $darkText }}; }
        .page-session.is-{{ $colorModifier }} .button.is-info { color: #{{ $darkText }}; }
    @endif

    /* #circulitoDelante
    .page-program .sessions-table .tbody .td.isType.has-text-{{ $colorModifier }}:before {
        background-color: #{{ $color }};
    }
    .page-program .sessions-table .tbody .td.isType.has-text-{{ $colorModifier }} span {
        background-color: #{{ $color }};
        color: white;
        display: block;
    }*/
    .page-program .sessions-table .tbody .td.isType.has-text-{{ $colorModifier }} span {
        color: #{{ $color }};
        background-color: white;
        font-weight: bold;
    }
</style>
